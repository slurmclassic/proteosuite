package uk.ac.liv.proteoidviewer.listener;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import uk.ac.ebi.pride.tools.jmzreader.model.Spectrum;

import com.compomics.util.gui.interfaces.SpectrumAnnotation;
import com.compomics.util.gui.spectrum.DefaultSpectrumAnnotation;
import com.compomics.util.gui.spectrum.SpectrumPanel;
import java.awt.event.MouseAdapter;
import uk.ac.liv.proteoidviewer.ProteoIDViewerNew;
import uk.ac.liv.proteoidviewer.tabs.SpectrumSummaryNew;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.CvParam;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.FragmentArray;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.Fragmentation;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.IonType;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.PeptideEvidence;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.PeptideEvidenceRef;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.SpectrumIdentificationItem;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.SpectrumIdentificationResult;

public final class SpectrumIdentificationItemTableMouseClickedNew extends
        MouseAdapter {

    private static final List<String> FILTER_LIST_ION = new ArrayList<>();
    private static final List<String> FILTER_LIST_CHARGE = new ArrayList<>();

    private final SpectrumSummaryNew spectrumSummary;
    private final ProteoIDViewerNew proteoIDViewer;

    public SpectrumIdentificationItemTableMouseClickedNew(
            final ProteoIDViewerNew proteoIDViewer, final SpectrumSummaryNew spectrumSummary) {
        this.proteoIDViewer = proteoIDViewer;
        this.spectrumSummary = spectrumSummary;
    }

    @Override
    public void mouseClicked(final MouseEvent e) {
        JTable spectrumSummaryIdentificationItem = (JTable) e.getSource();
        int row = spectrumSummaryIdentificationItem.getSelectedRow();
        if (row == -1) {
            return;
        }

        proteoIDViewer.setCursor(new Cursor(Cursor.WAIT_CURSOR));
        spectrumSummary.removeAllFragmentation();
        spectrumSummary.removeAllEvidence();

        // TODO: Disabled - Andrew
        // fragmentationTable.scrollRowToVisible(0);
        SpectrumIdentificationItem spectrumIdentificationItem
                = spectrumSummary.getSpectrumIdentificationItemListForSpecificResult().
                get(row);

        Fragmentation fragmentation = null;
        if (spectrumIdentificationItem != null) {
            List<PeptideEvidenceRef> peptideEvidenceRefList
                    = spectrumIdentificationItem
                    .getPeptideEvidenceRef();
            if (peptideEvidenceRefList != null) {
                for (int i = 0; i < peptideEvidenceRefList.size(); i++) {
                    PeptideEvidenceRef peptideEvidenceRef
                            = peptideEvidenceRefList
                            .get(i);
                    PeptideEvidence peptideEvidence = proteoIDViewer.
                            getSingleStorage().findPeptideEvidenceByRef(
                                    peptideEvidenceRef);
                    spectrumSummary.addEvidence(new Object[]{
                        peptideEvidence.getStart(),
                        peptideEvidence.getEnd(),
                        peptideEvidence.getPre(),
                        peptideEvidence.getPost(),
                        peptideEvidence.isDecoy(),
                        peptideEvidence.getPeptideRef(),
                        peptideEvidence.getDBSequenceRef()
                    });
                }
            }

            fragmentation = spectrumIdentificationItem
                    .getFragmentation();
        }

        if (fragmentation != null) {
            for (IonType ionType : fragmentation) {
                CvParam cvParam = ionType.getCvParam();
                if (!FILTER_LIST_ION.contains(cvParam.getName())) {
                    FILTER_LIST_ION.add(cvParam.getName());
                }
                if (!FILTER_LIST_CHARGE.contains(String.valueOf(ionType
                        .getCharge()))) {
                    FILTER_LIST_CHARGE
                            .add(String.valueOf(ionType.getCharge()));
                }

                List<FragmentArray> fragArrays = ionType.getFragmentArray();
                List<Float> m_mz = fragArrays.get(0)
                        .getValues();
                List<Float> m_intensity = fragArrays.get(1)
                        .getValues();
                List<Float> m_error = fragArrays.get(2)
                        .getValues();
                String type = cvParam.getName();
                type = type.replaceAll("param: ", "");
                type = type.replaceAll(" ion", "");

                if (m_mz != null && !m_mz.isEmpty()) {
                    for (int j = 0; j < m_mz.size(); j++) {
                        ((DefaultTableModel) spectrumSummary
                                .getFragmentationTable().getModel())
                                .addRow(new Object[]{m_mz.get(j),
                            m_intensity.get(j), m_error.get(j),
                            type + ionType.getIndex().get(j),
                            ionType.getCharge()});

                    }
                }
            }

            double[] mzValuesAsDouble = new double[spectrumSummary
                    .getFragmentationTable().getModel().getRowCount()];
            double[] intensityValuesAsDouble = new double[spectrumSummary
                    .getFragmentationTable().getModel().getRowCount()];
            double[] m_errorValuesAsDouble = new double[spectrumSummary
                    .getFragmentationTable().getModel().getRowCount()];
            final List<SpectrumAnnotation> peakAnnotation = new ArrayList<>();
            for (int k = 0; k < spectrumSummary.getFragmentationTable()
                    .getModel().getRowCount(); k++) {
                mzValuesAsDouble[k] = (double) (spectrumSummary
                        .getFragmentationTable().getModel().getValueAt(k, 0));

                intensityValuesAsDouble[k] = (double) (spectrumSummary
                        .getFragmentationTable().getModel().getValueAt(k, 1));
                m_errorValuesAsDouble[k] = (double) (spectrumSummary
                        .getFragmentationTable().getModel().getValueAt(k, 2));

                String type = (String) spectrumSummary.getFragmentationTable()
                        .getModel().getValueAt(k, 3);
                type = type.replaceFirst("frag:", "");
                type = type.replaceFirst("ion", "");
                type = type.replaceFirst("internal", "");

                peakAnnotation.add(new DefaultSpectrumAnnotation(
                        mzValuesAsDouble[k], m_errorValuesAsDouble[k],
                        Color.blue, type));
            }
            spectrumSummary.getGraph().removeAll();

            spectrumSummary.getGraph().validate();
            spectrumSummary.getGraph().repaint();
            if (proteoIDViewer.isRawAvailable()) {
                try {
                    int row1 = spectrumSummary.getIdentificationResultTable()
                            .getSelectedRow();
                    String sir_id = (String) spectrumSummary
                            .getIdentificationResultTable().getModel()
                            .getValueAt(row1, 0);
                    // System.out.println(sir_id);
                    SpectrumIdentificationResult spectrumIdentificationResult
                            = proteoIDViewer.getSingleStorage().
                            findSpectrumIdentificationResultByRef(sir_id);
                    Spectrum spectrum = proteoIDViewer.getRawSpectrum(
                            spectrumIdentificationResult
                            .getSpectrumID());

                    List<Double> mzValues;
                    if (spectrum.getPeakList() != null) {
                        mzValues = new ArrayList<>(spectrum.getPeakList()
                                .keySet());
                    } else {
                        mzValues = Collections.emptyList();
                    }

                    double[] mz = new double[mzValues.size()];
                    double[] intensities = new double[mzValues.size()];

                    int index = 0;
                    for (double mzValue : mzValues) {
                        mz[index] = mzValue;
                        intensities[index] = spectrum.getPeakList()
                                .get(mzValue);
                        index++;
                    }
                    SpectrumPanel spectrumPanel = new SpectrumPanel(mz,
                            intensities,
                            spectrumIdentificationItem
                            .getExperimentalMassToCharge(),
                            String.valueOf(spectrumIdentificationItem
                                    .getChargeState()),
                            spectrumIdentificationItem.getName());
                    spectrumPanel.setAnnotations(peakAnnotation);
                    spectrumSummary.getGraph().setLayout(new BorderLayout());
                    spectrumSummary.getGraph().setLayout(
                            new BoxLayout(spectrumSummary.getGraph(),
                                    BoxLayout.LINE_AXIS));
                    spectrumSummary.getGraph().add(spectrumPanel);
                    spectrumSummary.getGraph().validate();
                    spectrumSummary.getGraph().repaint();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            } else if (mzValuesAsDouble.length > 0) {
                SpectrumPanel spectrumPanel = new SpectrumPanel(
                        mzValuesAsDouble, intensityValuesAsDouble,
                        spectrumIdentificationItem
                        .getExperimentalMassToCharge(),
                        String.valueOf(spectrumIdentificationItem
                                .getChargeState()),
                        spectrumIdentificationItem.getName());

                spectrumPanel.setAnnotations(peakAnnotation);

                spectrumSummary.getGraph().setLayout(new BorderLayout());
                spectrumSummary.getGraph().setLayout(
                        new BoxLayout(spectrumSummary.getGraph(),
                                BoxLayout.LINE_AXIS));
                spectrumSummary.getGraph().add(spectrumPanel);
                spectrumSummary.getGraph().validate();
                spectrumSummary.getGraph().repaint();
                proteoIDViewer.repaint();
            }
        }

        proteoIDViewer.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    }
}
