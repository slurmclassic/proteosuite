package uk.ac.liv.proteoidviewer.listener;

import java.awt.Cursor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Iterator;

import javax.swing.JTable;

import uk.ac.liv.proteoidviewer.ProteoIDViewerNew;
import uk.ac.liv.proteoidviewer.tabs.SpectrumSummaryNew;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.SpectrumIdentificationItem;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.SpectrumIdentificationResult;

public final class SpectrumIdentificationItemProteinTableMouseClickedNew extends
        MouseAdapter {

    private final ProteoIDViewerNew proteoIDViewer;
    private final SpectrumSummaryNew spectrumSummary;

    public SpectrumIdentificationItemProteinTableMouseClickedNew(
            final ProteoIDViewerNew proteoIDViewer, final SpectrumSummaryNew spectrumSummary) {
        this.proteoIDViewer = proteoIDViewer;
        this.spectrumSummary = spectrumSummary;
    }

    @Override
    public void mouseClicked(final MouseEvent evt) {
        proteoIDViewer.setCursor(new Cursor(Cursor.WAIT_CURSOR));
        JTable source = (JTable) evt.getSource();
        String sii_ref = (String) source.getValueAt(source.getSelectedRow(), 1);

        for (int i = 0; i < spectrumSummary.getIdentificationResultTable()
                .getRowCount(); i++) {
            String sir_id = (String) spectrumSummary
                    .getIdentificationResultTable().getValueAt(i, 0);
            SpectrumIdentificationResult sir
                    = proteoIDViewer.getSingleStorage().
                    findSpectrumIdentificationResultByRef(sir_id);
            Iterator<SpectrumIdentificationItem> itemIterator = sir.iterator();
            for (int j = 0; itemIterator.hasNext(); j++) {
                SpectrumIdentificationItem spectrumIdentificationItem
                        = itemIterator.next();
                if (sii_ref.equals(spectrumIdentificationItem.getId())) {

                    spectrumSummary.getIdentificationResultTable()
                            .setRowSelectionInterval(i, i);
                    new SpectrumIdentificationResultTableMouseClickedNew(
                            proteoIDViewer, spectrumSummary)
                            .mouseClicked(evt);

                    spectrumSummary.getIdentificationItemTable()
                            .setRowSelectionInterval(j, j);
                    new SpectrumIdentificationItemTableMouseClickedNew(
                            proteoIDViewer, spectrumSummary)
                            .mouseClicked(evt);
                    break;
                }
            }

        }

        proteoIDViewer.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        // TODO: Argh why! - AC
        //if (!proteoIDViewer.isSecondTabLoaded) {
        //	spectrumSummary
        //			.loadSpectrumIdentificationResultTable(proteoIDViewer
        ////					.getMzIdentMLUnmarshaller());
        //	proteoIDViewer.isSecondTabLoaded = true;
        //}
        proteoIDViewer.setSelectedIndex(1);
    }
}
