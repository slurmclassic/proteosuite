package uk.ac.liv.proteoidviewer.listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import uk.ac.liv.proteoidviewer.tabs.PeptideSummaryNew;

public final class PsmRankValueActionPerformedNew implements ActionListener {

    private final PeptideSummaryNew peptideSummary;

    public PsmRankValueActionPerformedNew(final PeptideSummaryNew peptideSummary) {
        this.peptideSummary = peptideSummary;
    }

    @Override
    public void actionPerformed(final ActionEvent e) {
        peptideSummary.load();
    }

}
