package uk.ac.liv.proteoidviewer.listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;

import uk.ac.liv.proteoidviewer.tabs.GlobalStatisticsPanelNew;

public final class SiiComboBoxActionPerformedNew implements ActionListener {

    private final GlobalStatisticsPanelNew globalStatisticsPanel;

    public SiiComboBoxActionPerformedNew(
            final GlobalStatisticsPanelNew globalStatisticsPanel) {
        this.globalStatisticsPanel = globalStatisticsPanel;
    }

    @Override
    public void actionPerformed(final ActionEvent e) {
        JComboBox<String> siiComboBox = (JComboBox<String>) e.getSource();

        if (siiComboBox.getSelectedIndex() == -1) {
            return;
        }

        globalStatisticsPanel.load();

    }
}
