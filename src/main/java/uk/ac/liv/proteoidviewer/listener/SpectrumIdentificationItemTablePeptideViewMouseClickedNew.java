package uk.ac.liv.proteoidviewer.listener;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import uk.ac.ebi.pride.tools.jmzreader.model.Spectrum;

import com.compomics.util.gui.interfaces.SpectrumAnnotation;
import com.compomics.util.gui.spectrum.DefaultSpectrumAnnotation;
import com.compomics.util.gui.spectrum.SpectrumPanel;
import java.awt.event.MouseAdapter;
import uk.ac.liv.proteoidviewer.ProteoIDViewerNew;
import uk.ac.liv.proteoidviewer.tabs.PeptideSummaryNew;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.CvParam;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.Fragmentation;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.IonType;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.PeptideEvidence;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.PeptideEvidenceRef;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.SpectrumIdentificationItem;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.SpectrumIdentificationResult;

public final class SpectrumIdentificationItemTablePeptideViewMouseClickedNew extends
        MouseAdapter {

    private final ProteoIDViewerNew proteoIDViewer;
    private final PeptideSummaryNew peptideSummary;
    private final List<String> filterListIon1;
    private final List<String> filterListCharge1;
    private final JPanel jGraph1;
    private final Map<String, String> siiSirMap;

    public SpectrumIdentificationItemTablePeptideViewMouseClickedNew(
            final ProteoIDViewerNew proteoIDViewer, final PeptideSummaryNew peptideSummary,
            final List<String> filterListIon1, final List<String> filterListCharge1,
            final JPanel jGraph1, final Map<String, String> siiSirMap) {
        this.proteoIDViewer = proteoIDViewer;
        this.filterListIon1 = filterListIon1;
        this.filterListCharge1 = filterListCharge1;
        this.jGraph1 = jGraph1;
        this.siiSirMap = siiSirMap;
        this.peptideSummary = peptideSummary;
    }

    @Override
    public void mouseClicked(final MouseEvent e) {
        JTable peptideSummaryIdentification = (JTable) e.getSource();
        int row = peptideSummaryIdentification.getSelectedRow();
        if (row == -1) {
            return;
        }

        proteoIDViewer.setCursor(new Cursor(Cursor.WAIT_CURSOR));
        peptideSummary.removeAllFragmentation();

        ((DefaultTableModel) peptideSummary.getEvidenceTable().getModel())
                .setNumRows(0);
        SpectrumIdentificationItem spectrumIdentificationItem = proteoIDViewer.
                getSingleStorage().findSpectrumIdentificationItemByRef(
                        (String) peptideSummaryIdentification
                        .getValueAt(row, 0));

        Fragmentation fragmentation = null;
        if (spectrumIdentificationItem != null) {
            List<PeptideEvidenceRef> peptideEvidenceRefList
                    = spectrumIdentificationItem
                    .getPeptideEvidenceRef();
            if (peptideEvidenceRefList != null) {
                for (PeptideEvidenceRef peptideEvidenceRef
                        : peptideEvidenceRefList) {
                    PeptideEvidence peptideEvidence = proteoIDViewer.
                            getSingleStorage().findPeptideEvidenceByRef(
                                    peptideEvidenceRef);
                    ((DefaultTableModel) peptideSummary
                            .getEvidenceTable().getModel())
                            .addRow(new Object[]{
                        peptideEvidence.getStart(),
                        peptideEvidence.getEnd(),
                        peptideEvidence.getPre(),
                        peptideEvidence.getPost(),
                        peptideEvidence.isDecoy(),
                        peptideEvidence.getPeptideRef(),
                        peptideEvidence.getDBSequenceRef()
                    // "<html><a href=>"
                    // +peptideEvidence.getDBSequenceRef()+"</a>"
                    });
                }
            }

            fragmentation = spectrumIdentificationItem
                    .getFragmentation();
        }

        if (fragmentation != null) {

            for (IonType ionType : fragmentation) {
                CvParam cvParam = ionType.getCvParam();
                if (!filterListIon1.contains(cvParam.getName())) {
                    filterListIon1.add(cvParam.getName());
                }
                if (!filterListCharge1.contains(String.valueOf(ionType
                        .getCharge()))) {
                    filterListCharge1.add(String.valueOf(ionType
                            .getCharge()));
                }
                List<Float> m_mz = ionType.getFragmentArray().get(0)
                        .getValues();
                List<Float> m_intensity = ionType.getFragmentArray()
                        .get(1).getValues();
                List<Float> m_error = ionType.getFragmentArray().get(2)
                        .getValues();
                String type = cvParam.getName();
                type = type.replaceAll(" ion", "");
                type = type.replaceAll("param: ", "");

                if (m_mz != null && !m_mz.isEmpty()) {
                    for (int j = 0; j < m_mz.size(); j++) {
                        ((DefaultTableModel) peptideSummary
                                .getFragmentationTable().getModel())
                                .addRow(new Object[]{
                            m_mz.get(j),
                            m_intensity.get(j),
                            m_error.get(j),
                            type
                            + ionType.getIndex()
                            .get(j),
                            ionType.getCharge()});

                    }
                }
            }

            double[] mzValuesAsDouble = new double[peptideSummary
                    .getFragmentationTable().getModel().getRowCount()];
            double[] intensityValuesAsDouble = new double[peptideSummary
                    .getFragmentationTable().getModel().getRowCount()];
            double[] m_errorValuesAsDouble = new double[peptideSummary
                    .getFragmentationTable().getModel().getRowCount()];
            List<SpectrumAnnotation> peakAnnotation1 = new ArrayList<>();
            for (int k = 0; k < peptideSummary.getFragmentationTable()
                    .getModel().getRowCount(); k++) {
                mzValuesAsDouble[k] = (double) (peptideSummary
                        .getFragmentationTable().getModel()
                        .getValueAt(k, 0));

                intensityValuesAsDouble[k] = (double) (peptideSummary
                        .getFragmentationTable().getModel()
                        .getValueAt(k, 1));
                m_errorValuesAsDouble[k] = (double) (peptideSummary
                        .getFragmentationTable().getModel()
                        .getValueAt(k, 2));

                String type = (String) peptideSummary
                        .getFragmentationTable().getModel()
                        .getValueAt(k, 3);
                type = type.replaceFirst("frag:", "");
                type = type.replaceFirst("ion", "");
                type = type.replaceFirst("internal", "");

                peakAnnotation1.add(new DefaultSpectrumAnnotation(
                        mzValuesAsDouble[k], m_errorValuesAsDouble[k],
                        Color.blue, type));
            }

            jGraph1.removeAll();
            if (proteoIDViewer.isRawAvailable()) {
                try {

                    String sir_id = siiSirMap.get((String) peptideSummary
                            .getIdentificationTable().getValueAt(row, 0));
                    SpectrumIdentificationResult spectrumIdentificationResult
                            = proteoIDViewer.getSingleStorage().
                            findSpectrumIdentificationResultByRef(sir_id);

                    Spectrum spectrum = proteoIDViewer
                            .getRawSpectrum(spectrumIdentificationResult
                                    .getSpectrumID());

                    List<Double> mzValues;
                    if (spectrum.getPeakList() != null) {
                        mzValues = new ArrayList<>(spectrum
                                .getPeakList().keySet());
                    } else {
                        mzValues = Collections.emptyList();
                    }

                    double[] mz = new double[mzValues.size()];
                    double[] intensities = new double[mzValues.size()];

                    int index = 0;
                    for (double mzValue : mzValues) {
                        mz[index] = mzValue;
                        intensities[index] = spectrum.getPeakList().get(
                                mzValue);
                        index++;
                    }
                    SpectrumPanel spectrumPanel1 = new SpectrumPanel(mz,
                            intensities,
                            spectrumIdentificationItem
                            .getExperimentalMassToCharge(),
                            String.valueOf(spectrumIdentificationItem
                                    .getChargeState()),
                            spectrumIdentificationItem.getName());
                    spectrumPanel1.setAnnotations(peakAnnotation1);
                    jGraph1.setLayout(new BorderLayout());
                    jGraph1.setLayout(new BoxLayout(jGraph1,
                            BoxLayout.LINE_AXIS));
                    jGraph1.add(spectrumPanel1);
                    jGraph1.validate();
                    jGraph1.repaint();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            } else if (mzValuesAsDouble.length > 0) {
                SpectrumPanel spectrumPanel1 = new SpectrumPanel(
                        mzValuesAsDouble, intensityValuesAsDouble,
                        spectrumIdentificationItem
                        .getExperimentalMassToCharge(),
                        String.valueOf(spectrumIdentificationItem
                                .getChargeState()),
                        spectrumIdentificationItem.getName());

                spectrumPanel1.setAnnotations(peakAnnotation1);

                jGraph1.setLayout(new BorderLayout());
                jGraph1.setLayout(new BoxLayout(jGraph1,
                        BoxLayout.LINE_AXIS));
                jGraph1.add(spectrumPanel1);
                jGraph1.validate();
                jGraph1.repaint();
                proteoIDViewer.repaint();
            }
        }

        proteoIDViewer.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    }
}
