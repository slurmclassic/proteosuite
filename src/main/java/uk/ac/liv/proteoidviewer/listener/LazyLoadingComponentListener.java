package uk.ac.liv.proteoidviewer.listener;

import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

import uk.ac.liv.proteoidviewer.interfaces.LazyLoading;

public final class LazyLoadingComponentListener extends
        ComponentAdapter {

    @Override
    public void componentShown(final ComponentEvent e) {
        final LazyLoading lazyLoadable = (LazyLoading) e.getSource();
        lazyLoadable.removeComponentListener(this);
        new Thread(lazyLoadable.getClass().getName() + " LazyLoader") {
            @Override
            public void run() {
                lazyLoadable.load();
            }
        }.start();
    }
}
