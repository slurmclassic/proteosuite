package uk.ac.liv.proteoidviewer.listener;

import java.awt.Cursor;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import uk.ac.ebi.pride.tools.jmzreader.model.Spectrum;
import uk.ac.liv.proteoidviewer.util.IdViewerUtils;

import com.compomics.util.gui.interfaces.SpectrumAnnotation;
import com.compomics.util.gui.spectrum.SpectrumPanel;
import java.awt.event.MouseAdapter;
import java.util.Iterator;

import uk.ac.liv.proteoidviewer.ProteoIDViewerNew;
import uk.ac.liv.proteoidviewer.tabs.SpectrumSummaryNew;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.CvParam;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.Modification;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.Peptide;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.SpectrumIdentificationItem;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.SpectrumIdentificationResult;
import uk.ac.liv.pgb.psi.utils.ident.standard.utils.CollectionUtils;

public final class SpectrumIdentificationResultTableMouseClickedNew extends
        MouseAdapter {

    private final SpectrumSummaryNew spectrumSummary;
    private final ProteoIDViewerNew proteoIDViewer;

    public SpectrumIdentificationResultTableMouseClickedNew(
            final ProteoIDViewerNew proteoIDViewer, final SpectrumSummaryNew spectrumSummary) {
        this.proteoIDViewer = proteoIDViewer;
        this.spectrumSummary = spectrumSummary;
    }

    @Override
    public void mouseClicked(final MouseEvent e) {
        JTable spectrumSummaryIdentificationResult = (JTable) e.getSource();
        int row = spectrumSummaryIdentificationResult
                .getSelectedRow();
        if (row == -1) {
            return;
        }

        proteoIDViewer.setCursor(new Cursor(Cursor.WAIT_CURSOR));
        // row =
        // spectrumIdentificationResultTable.convertRowIndexToModel(row);
        spectrumSummary.removeAllIdentificationItems();
        spectrumSummary.removeAllEvidence();
        spectrumSummary.removeAllFragmentation();
        spectrumSummary.getGraph().removeAll();
        spectrumSummary.getGraph().validate();
        spectrumSummary.getGraph().repaint();

        try {
            // TODO: Disabled - Andrew
            // spectrumIdentificationItemTable.scrollRowToVisible(0);
            String sirID = (String) spectrumSummaryIdentificationResult.
                    getModel()
                    .getValueAt(row, 0);
            SpectrumIdentificationResult spectrumIdentificationResult
                    = proteoIDViewer.getSingleStorage().
                    findSpectrumIdentificationResultByRef(sirID);

            if (proteoIDViewer.isRawAvailable()) {

                Spectrum spectrum = proteoIDViewer
                        .getRawSpectrum(spectrumIdentificationResult
                                .getSpectrumID());

                List<Double> mzValues;
                if (spectrum.getPeakList() != null) {
                    mzValues = new ArrayList<Double>(spectrum.getPeakList()
                            .keySet());
                } else {
                    mzValues = Collections.emptyList();
                }

                double[] mz = new double[mzValues.size()];
                double[] intensities = new double[mzValues.size()];

                int index = 0;
                final List<SpectrumAnnotation> peakAnnotation
                        = new ArrayList<>();
                for (double mzValue : mzValues) {
                    mz[index] = mzValue;
                    intensities[index] = spectrum.getPeakList().get(mzValue);

                    index++;
                }

                SpectrumPanel spectrumPanel = new SpectrumPanel(mz,
                        intensities, spectrum.getPrecursorMZ(), spectrum.
                        getPrecursorCharge().toString(), "");
                spectrumPanel.setAnnotations(peakAnnotation);
                spectrumSummary.getGraph().add(spectrumPanel);
                spectrumSummary.getGraph().validate();
                spectrumSummary.getGraph().repaint();
            }

            // Perhaps change to listFromIterableFullCopy. List is only used for getting a specific element, so perhaps full copy is quicker.
            spectrumSummary.setSpectrumIdentificationItemListForSpecificResult(CollectionUtils.listFromIterableNoCopyImmutable(
                            spectrumIdentificationResult));

            for (SpectrumIdentificationItem item : spectrumIdentificationResult) {
                try {

                    boolean isDecoy = proteoIDViewer
                            .checkIfSpectrumIdentificationItemIsDecoy(
                                    item);

                    Peptide peptide = proteoIDViewer.getSingleStorage().
                            findPeptideByRef(item.getPeptideRef());

                    if (peptide != null) {
                        Iterator<Modification> modIterator = peptide.iterator();
                        String residues = null;
                        String modificationName = null;
                        int location = -1;
                        if (modIterator.hasNext()) {
                            Modification mod = modIterator.next();
                            location = mod.getLocation();

                            List<String> residueList = mod.getResidues();
                            if (!residueList.isEmpty()) {
                                residues = residueList.get(0);
                            }

                            List<CvParam> cvParamList = mod.getCvParam();
                            if (!cvParamList.isEmpty()) {
                                modificationName = cvParamList.get(0).getName();
                            }
                        }

                        StringBuilder builder = new StringBuilder();
                        if (modificationName != null) {
                            builder.append(modificationName);
                        }

                        if (residues != null) {
                            builder.append(" on residues: ");
                            builder.append(residues);
                        }

                        if (location > -1) {
                            builder.append(" at location: ");
                            builder.append(location);

                        }
                        double calculatedMassToCharge = item
                                .getCalculatedMassToCharge();

                        ((DefaultTableModel) spectrumSummary
                                .getIdentificationItemTable().getModel())
                                .addRow(new Object[]{
                            item.getId(),
                            peptide.getPeptideSequence(),
                            builder.toString(),
                            IdViewerUtils
                            .roundTwoDecimals(calculatedMassToCharge),
                            IdViewerUtils
                            .roundTwoDecimals(item
                            .getExperimentalMassToCharge()),
                            item.getRank(),
                            isDecoy,
                            item.isPassThreshold()});

                        List<CvParam> cvParamListspectrumIdentificationItem
                                = item.getCvParam();

                        for (int s = 0; s
                                < cvParamListspectrumIdentificationItem
                                .size(); s++) {
                            CvParam cvParam
                                    = cvParamListspectrumIdentificationItem
                                    .get(s);

                            if (cvParam.getName().equals(
                                    "peptide unique to one protein")) {
                                ((DefaultTableModel) spectrumSummary
                                        .getIdentificationItemTable()
                                        .getModel())
                                        .setValueAt(
                                                1,
                                                ((DefaultTableModel) spectrumSummary.
                                                getIdentificationItemTable()
                                                .getModel())
                                                .getRowCount() - 1,
                                                8 + s);
                            } else {
                                ((DefaultTableModel) spectrumSummary
                                        .getIdentificationItemTable()
                                        .getModel())
                                        .setValueAt(
                                                cvParam.getValue(),
                                                ((DefaultTableModel) spectrumSummary.
                                                getIdentificationItemTable()
                                                .getModel())
                                                .getRowCount() - 1,
                                                8 + s);
                            }
                        }

                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        proteoIDViewer.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    }
}
