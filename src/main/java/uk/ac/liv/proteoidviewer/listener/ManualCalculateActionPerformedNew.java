package uk.ac.liv.proteoidviewer.listener;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import uk.ac.liv.mzidlib.fdr.FalseDiscoveryRate;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.DBSequence;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.PeptideEvidence;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.PeptideEvidenceRef;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.SpectrumIdentificationItem;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.SpectrumIdentificationList;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.SpectrumIdentificationResult;
import uk.ac.liv.proteoidviewer.ProteoIDViewerNew;
import uk.ac.liv.proteoidviewer.tabs.GlobalStatisticsPanelNew;
import uk.ac.liv.proteoidviewer.util.IdViewerUtils;

public final class ManualCalculateActionPerformedNew implements ActionListener {

    private final ProteoIDViewerNew proteoIDViewer;
    private final JComboBox<String> siiComboBox;
    private final JTextField manualDecoyPrefixValue;
    private final JCheckBox manualDecoy;
    private final JLabel isDecoySiiValue;
    private final JLabel isDecoySiiFalseValue;
    private final JTextField manualDecoyRatioValue;
    private final JLabel fpSiiValue;
    private final List<SpectrumIdentificationItem> sIIListPassThreshold;
    private final JLabel tpSiiValue;
    private final JLabel fdrSiiValue;
    private final JPanel fdrPanel;
    private final JComboBox<String> jComboBox2;
    private final Map<String, String> cvTermMap;
    private final JComboBox<String> jComboBox1;
    private final JPanel tpEvaluePanel;
    private final JPanel tpQvaluePanel;
    private final GlobalStatisticsPanelNew globalStatisticsPanel;

    private ManualCalculateActionPerformedNew(Builder builder) {
        this.proteoIDViewer = builder.proteoIDViewer;
        this.siiComboBox = builder.siiComboBox;
        this.manualDecoy = builder.manualDecoy;
        this.manualDecoyPrefixValue = builder.manualDecoyPrefixValue;
        this.isDecoySiiValue = builder.isDecoySiiValue;
        this.isDecoySiiFalseValue = builder.isDecoySiiFalseValue;
        this.manualDecoyRatioValue = builder.manualDecoyRatioValue;
        this.fpSiiValue = builder.fpSiiValue;
        this.sIIListPassThreshold = builder.sIIListPassThreshold;
        this.tpSiiValue = builder.tpSiiValue;
        this.fdrSiiValue = builder.fdrSiiValue;
        this.fdrPanel = builder.fdrPanel;
        this.jComboBox2 = builder.jComboBox2;
        this.cvTermMap = builder.cvTermMap;
        this.jComboBox1 = builder.jComboBox1;
        this.tpEvaluePanel = builder.tpEvaluePanel;
        this.tpQvaluePanel = builder.tpQvaluePanel;
        this.globalStatisticsPanel = builder.globalStatisticsPanel;
    }

    private ManualCalculateActionPerformedNew(final ProteoIDViewerNew proteoIDViewer,
            final JComboBox<String> siiComboBox, final JCheckBox manualDecoy,
            final JTextField manualDecoyPrefixValue, final JLabel isDecoySiiValue,
            final JLabel isDecoySiiFalseValue, final JTextField manualDecoyRatioValue,
            final JLabel fpSiiValue,
            final List<SpectrumIdentificationItem> sIIListPassThreshold,
            final JLabel tpSiiValue, final JLabel fdrSiiValue, final JPanel fdrPanel,
            final JComboBox<String> jComboBox2, final Map<String, String> cvTermMap,
            final JComboBox<String> jComboBox1, final JPanel tpEvaluePanel,
            final JPanel tpQvaluePanel, final GlobalStatisticsPanelNew globalStatisticsPanel) {
        this.proteoIDViewer = proteoIDViewer;
        this.siiComboBox = siiComboBox;
        this.manualDecoy = manualDecoy;
        this.manualDecoyPrefixValue = manualDecoyPrefixValue;
        this.isDecoySiiValue = isDecoySiiValue;
        this.isDecoySiiFalseValue = isDecoySiiFalseValue;
        this.manualDecoyRatioValue = manualDecoyRatioValue;
        this.fpSiiValue = fpSiiValue;
        this.sIIListPassThreshold = sIIListPassThreshold;
        this.tpSiiValue = tpSiiValue;
        this.fdrSiiValue = fdrSiiValue;
        this.fdrPanel = fdrPanel;
        this.jComboBox2 = jComboBox2;
        this.cvTermMap = cvTermMap;
        this.jComboBox1 = jComboBox1;
        this.tpEvaluePanel = tpEvaluePanel;
        this.tpQvaluePanel = tpQvaluePanel;
        this.globalStatisticsPanel = globalStatisticsPanel;
    }

    @Override
    public void actionPerformed(final ActionEvent e) {
        Thread t = new Thread("LoadingThread") {

            @Override
            public void run() {
                makeFDRGraphs();
            }
        };
        t.start();
    }

    private void makeFDRGraphs() {
        List<SpectrumIdentificationItem> sIIListIsDecoyTrue = new ArrayList<>();
        List<SpectrumIdentificationItem> sIIListIsDecoyFalse = new ArrayList<>();
        Optional<SpectrumIdentificationList> optionalList = proteoIDViewer.
                getSingleStorage().getSpectrumIdentificationLists().stream().
                filter(p -> p.getId().equalsIgnoreCase(siiComboBox.
                        getSelectedItem().toString())).findAny();
        List<SpectrumIdentificationItem> siiListTemp = new ArrayList<>();
        if (optionalList.isPresent()) {
            for (SpectrumIdentificationResult spectrumIdentificationResult
                    : optionalList.get()) {
                for (SpectrumIdentificationItem item
                        : spectrumIdentificationResult) {
                    siiListTemp.add(item);
                }
            }
        }
        boolean isdecoy = true;
        for (SpectrumIdentificationItem spectrumIdentificationItem : siiListTemp) {
            List<PeptideEvidenceRef> peptideEvidenceRefList
                    = spectrumIdentificationItem
                    .getPeptideEvidenceRef();

            for (PeptideEvidenceRef peptideEvidenceRef : peptideEvidenceRefList) {
                PeptideEvidence peptideEvidence = proteoIDViewer.
                        getSingleStorage().findPeptideEvidenceByRef(
                                peptideEvidenceRef);
                DBSequence dbSeq = proteoIDViewer.getSingleStorage().
                        findDBSequenceByRef(peptideEvidence.getDBSequenceRef());

                if (manualDecoy.isSelected()) { // Added by ARJ to use value
                    // in file if manual decoy
                    // is not selected
                    if (!dbSeq.getAccession().startsWith(
                            manualDecoyPrefixValue.getText())) {
                        sIIListIsDecoyFalse.add(spectrumIdentificationItem);
                        isdecoy = false;
                        break;
                    }
                } else if (!peptideEvidence.isDecoy()) {
                    sIIListIsDecoyFalse.add(spectrumIdentificationItem);
                    isdecoy = false;
                    break;
                }
            }
            if (isdecoy) {
                sIIListIsDecoyTrue.add(spectrumIdentificationItem);
            }
        }

        isDecoySiiValue.setText(String.valueOf(sIIListIsDecoyTrue
                .size()));

        isDecoySiiFalseValue.setText(String.valueOf(sIIListIsDecoyFalse
                .size()));

        globalStatisticsPanel.falsePositiveSii = IdViewerUtils
                .roundThreeDecimals(sIIListIsDecoyTrue.size()
                        / Double.valueOf(manualDecoyRatioValue
                                .getText().trim()));
        fpSiiValue.setText(String
                .valueOf(globalStatisticsPanel.falsePositiveSii));

        globalStatisticsPanel.truePositiveSii = IdViewerUtils
                .roundThreeDecimals(sIIListPassThreshold.size()
                        - sIIListIsDecoyTrue.size());
        tpSiiValue.setText(String
                .valueOf(globalStatisticsPanel.truePositiveSii));

        globalStatisticsPanel.fdrSii = IdViewerUtils
                .roundThreeDecimals(globalStatisticsPanel.falsePositiveSii
                        / (globalStatisticsPanel.falsePositiveSii
                        + globalStatisticsPanel.truePositiveSii));
        fdrSiiValue.setText(String
                .valueOf(globalStatisticsPanel.fdrSii));

        fdrPanel.removeAll();
        if (manualDecoy.isSelected()) {
            String cvTerm = "";
            boolean order = false;
            if (jComboBox2.getSelectedItem().equals(
                    "Better scores are lower")) {
                order = true;
            }
            cvTerm = cvTermMap.get((String) jComboBox1.getSelectedItem());
            FalseDiscoveryRate falseDiscoveryRate = null;
            falseDiscoveryRate = new FalseDiscoveryRate(
                    new File(proteoIDViewer.getFileName().getAbsolutePath()),
                    Integer.parseInt(manualDecoyRatioValue.getText()),
                    manualDecoyPrefixValue.getText(), cvTerm, order);

            falseDiscoveryRate.computeFDRusingJonesMethod();

            // FDR Graph
            XYSeriesCollection datasetFDR = new XYSeriesCollection();
            final XYSeries dataFDR = new XYSeries("FDR", false);

            for (int i = 0; i < falseDiscoveryRate.getSorted_evalues()
                    .size(); i++) {
                double eValue = falseDiscoveryRate.getSorted_evalues().get(i);
                double eFDR = falseDiscoveryRate.getSorted_estimatedFDR()
                        .get(i);
                if (eValue != 0) {
                    dataFDR.add(Math.log10(eValue),
                            eFDR);
                }

            }

            final XYSeries dataFDRQvalue = new XYSeries("Q-value", false);

            for (int i = 0; i < falseDiscoveryRate.getSorted_evalues()
                    .size(); i++) {
                if (falseDiscoveryRate.getSorted_evalues().get(i) != 0) {
                    dataFDRQvalue.add(Math.log10(falseDiscoveryRate
                            .getSorted_evalues().get(i)),
                            falseDiscoveryRate.getSorted_qValues().get(i));
                }
            }

            final XYSeries dataFDRSimple = new XYSeries("Simple FDR", false);

            for (int i = 0; i < falseDiscoveryRate.getSorted_evalues()
                    .size(); i++) {
                if (falseDiscoveryRate.getSorted_evalues().get(i) != 0) {
                    dataFDRSimple
                            .add(Math.log10(falseDiscoveryRate
                                    .getSorted_evalues().get(i)),
                                    falseDiscoveryRate
                                    .getSorted_simpleFDR().get(i));
                }

            }

            datasetFDR.addSeries(dataFDR);
            datasetFDR.addSeries(dataFDRQvalue);
            datasetFDR.addSeries(dataFDRSimple);
            final JFreeChart chartFDR = createFDRChart(datasetFDR);
            final ChartPanel chartPanelFDR = new ChartPanel(chartFDR);

            chartPanelFDR.setPreferredSize(new Dimension(257, 255));
            fdrPanel.add(chartPanelFDR);
            JScrollPane jFDRPane = new JScrollPane(chartPanelFDR);
            fdrPanel.setLayout(new BorderLayout());
            fdrPanel.add(jFDRPane);

            // TP vs Evalue Graph
            XYSeriesCollection datasetTpQvalue = new XYSeriesCollection();
            final XYSeries dataTpQvalue = new XYSeries("TP", false);

            for (int i = 0; i < falseDiscoveryRate.getSorted_evalues()
                    .size(); i++) {
                if (falseDiscoveryRate.getSorted_evalues().get(i) != 0) {
                    dataTpQvalue.add(Math.log10(falseDiscoveryRate
                            .getSorted_evalues().get(i)),
                            falseDiscoveryRate.getTP().get(i));
                }
            }

            final XYSeries dataFpQvalue = new XYSeries("FP", false);

            for (int i = 0; i < falseDiscoveryRate.getSorted_evalues()
                    .size(); i++) {
                if (falseDiscoveryRate.getSorted_evalues().get(i) != 0) {
                    dataFpQvalue.add(Math.log10(falseDiscoveryRate
                            .getSorted_evalues().get(i)),
                            falseDiscoveryRate.getFP().get(i));
                }
            }

            datasetTpQvalue.addSeries(dataTpQvalue);
            datasetTpQvalue.addSeries(dataFpQvalue);
            final JFreeChart chartTpQvalue
                    = createTpQvalueChart(datasetTpQvalue);
            final ChartPanel chartPanelTpQvalue = new ChartPanel(
                    chartTpQvalue);

            chartPanelTpQvalue.setPreferredSize(new Dimension(257, 255));
            tpEvaluePanel.add(chartPanelTpQvalue);
            JScrollPane jTpQvaluePane = new JScrollPane(chartPanelTpQvalue);
            tpEvaluePanel.setLayout(new BorderLayout());
            tpEvaluePanel.add(jTpQvaluePane);

            // TP vs Qvalue
            XYSeriesCollection datasetTpQvalueCollection
                    = new XYSeriesCollection();
            final XYSeries dataTpQValueSeries = new XYSeries("data", false);

            for (int i = 0; i < falseDiscoveryRate.getSorted_evalues()
                    .size(); i++) {
                dataTpQValueSeries.add(falseDiscoveryRate
                        .getSorted_qValues().get(i), falseDiscoveryRate
                        .getTP().get(i));
                // System.out.println(falseDiscoveryRate.getSorted_qValues().get(i)
                // + " " + falseDiscoveryRate.getTP().get(i) );
            }

            datasetTpQvalueCollection.addSeries(dataTpQValueSeries);
            final JFreeChart chartTpQvalueChart = createTpQvalue(
                    datasetTpQvalueCollection);
            final ChartPanel chartPanelTpSimpleFDR = new ChartPanel(
                    chartTpQvalueChart);

            chartPanelTpSimpleFDR.setPreferredSize(new Dimension(257, 255));
            tpQvaluePanel.add(chartPanelTpSimpleFDR);
            JScrollPane jTpSimpleFDRPane = new JScrollPane(
                    chartPanelTpSimpleFDR);
            tpQvaluePanel.setLayout(new BorderLayout());
            tpQvaluePanel.add(jTpSimpleFDRPane);

            proteoIDViewer.repaint();
        }
    } // GEN-LAST:event_manualCalculateActionPerformed

    private JFreeChart createFDRChart(final XYDataset dataset) {
        final JFreeChart chart = ChartFactory.createScatterPlot("FDR", // chart
                // title
                "log10(e-value)", // x axis label
                "", // y axis label
                dataset, // data
                PlotOrientation.VERTICAL, true, // include legend
                true, // tooltips
                false // urls
        );
        XYPlot plot = (XYPlot) chart.getPlot();
        XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
        renderer.setSeriesLinesVisible(0, true);
        plot.setRenderer(renderer);
        return chart;
    }

    private JFreeChart createTpQvalueChart(final XYDataset dataset) {
        final JFreeChart chart = ChartFactory.createScatterPlot(
                "TP vs FP vs E-value", // chart title
                "log10(e-value)", // x axis label
                "", // y axis label
                dataset, // data
                PlotOrientation.VERTICAL, true, // include legend
                true, // tooltips
                false // urls
        );
        XYPlot plot = (XYPlot) chart.getPlot();
        XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
        renderer.setSeriesLinesVisible(0, true);
        plot.setRenderer(renderer);
        return chart;
    }

    private JFreeChart createTpQvalue(final XYDataset dataset) {
        final JFreeChart chart = ChartFactory.createScatterPlot(
                "TP vs Q-value", // chart title
                "Q-value", // x axis label
                "TP value", // y axis label
                dataset, // data
                PlotOrientation.VERTICAL, true, // include legend
                true, // tooltips
                false // urls
        );
        XYPlot plot = (XYPlot) chart.getPlot();
        XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
        renderer.setSeriesLinesVisible(0, true);
        plot.setRenderer(renderer);
        return chart;
    }

    public static class Builder {

        private ProteoIDViewerNew proteoIDViewer;
        private JComboBox<String> siiComboBox;
        private JCheckBox manualDecoy;
        private JTextField manualDecoyPrefixValue;
        private JLabel isDecoySiiValue;
        private JLabel isDecoySiiFalseValue;
        private JTextField manualDecoyRatioValue;
        private JLabel fpSiiValue;
        private List<SpectrumIdentificationItem> sIIListPassThreshold;
        private JLabel tpSiiValue;
        private JLabel fdrSiiValue;
        private JPanel fdrPanel;
        private JComboBox<String> jComboBox2;
        private Map<String, String> cvTermMap;
        private JComboBox<String> jComboBox1;
        private JPanel tpEvaluePanel;
        private JPanel tpQvaluePanel;
        private GlobalStatisticsPanelNew globalStatisticsPanel;

        public Builder() { }

        public Builder proteoIDViewer(ProteoIDViewerNew proteoIDViewer) {
            this.proteoIDViewer = proteoIDViewer;
            return this;
        }

        public Builder siiComboBox(JComboBox<String> siiComboBox) {
            this.siiComboBox = siiComboBox;
            return this;
        }

        public Builder manualDecoy(JCheckBox manualDecoy) {
            this.manualDecoy = manualDecoy;
            return this;
        }

        public Builder manualDecoyPrefixValue(JTextField manualDecoyPrefixValue) {
            this.manualDecoyPrefixValue = manualDecoyPrefixValue;
            return this;
        }

        public Builder isDecoySiiValue(JLabel isDecoySiiValue) {
            this.isDecoySiiValue = isDecoySiiValue;
            return this;
        }

        public Builder isDecoySiiFalseValue(JLabel isDecoySiiFalseValue) {
            this.isDecoySiiFalseValue = isDecoySiiFalseValue;
            return this;
        }

        public Builder manualDecoyRatioValue(JTextField manualDecoyRatioValue) {
            this.manualDecoyRatioValue = manualDecoyRatioValue;
            return this;
        }

        public Builder fpSiiValue(JLabel fpSiiValue) {
            this.fpSiiValue = fpSiiValue;
            return this;
        }

        public Builder siiListPassThreshold(List<SpectrumIdentificationItem> sIIListPassThreshold) {
            this.sIIListPassThreshold = sIIListPassThreshold;
            return this;
        }

        public Builder tpSiiValue(JLabel tpSiiValue) {
            this.tpSiiValue = tpSiiValue;
            return this;
        }

        public Builder fdrSiiValue(JLabel fdrSiiValue) {
            this.fdrSiiValue = fdrSiiValue;
            return this;
        }

        public Builder fdrPanel(JPanel fdrPanel) {
            this.fdrPanel = fdrPanel;
            return this;
        }

        public Builder jComboBox2(JComboBox<String> jComboBox2) {
            this.jComboBox2 = jComboBox2;
            return this;
        }

        public Builder cvTermMap(Map<String, String> cvTermMap) {
            this.cvTermMap = cvTermMap;
            return this;
        }

        public Builder jComboBox1(JComboBox<String> jComboBox1) {
            this.jComboBox1 = jComboBox1;
            return this;
        }

        public Builder tpEvaluePanel(JPanel tpEvaluePanel) {
            this.tpEvaluePanel = tpEvaluePanel;
            return this;
        }

        public Builder tpQvaluePanel(JPanel tpQvaluePanel) {
            this.tpQvaluePanel = tpQvaluePanel;
            return this;
        }

        public Builder globalStatisticsPanel(GlobalStatisticsPanelNew globalStatisticsPanel) {
            this.globalStatisticsPanel = globalStatisticsPanel;
            return this;
        }

        public ManualCalculateActionPerformedNew build() {
            return new ManualCalculateActionPerformedNew(this);
        }

    }
}
