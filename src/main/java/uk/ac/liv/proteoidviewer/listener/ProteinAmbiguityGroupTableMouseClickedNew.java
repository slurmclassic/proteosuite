package uk.ac.liv.proteoidviewer.listener;

import java.awt.Cursor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.table.DefaultTableModel;

import uk.ac.liv.proteoidviewer.ProteoIDViewerNew;
import uk.ac.liv.proteoidviewer.tabs.ProteinViewNew;
import uk.ac.liv.proteoidviewer.util.IdViewerUtils;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.CvParam;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.DBSequence;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.ProteinAmbiguityGroup;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.ProteinDetectionHypothesis;

public final class ProteinAmbiguityGroupTableMouseClickedNew extends MouseAdapter {

    private final ProteoIDViewerNew proteoIDViewer;
    private final JTextArea jProteinSequenceTextPane;
    private final JLabel jScientificNameValueLabel;

    private final ProteinViewNew proteinView;

    public ProteinAmbiguityGroupTableMouseClickedNew(
            final ProteoIDViewerNew proteoIDViewer, final JTextArea jProteinSequenceTextPane,
            final JLabel jScientificNameValueLabel,
            final ProteinViewNew proteinView) {
        this.proteoIDViewer = proteoIDViewer;
        this.jProteinSequenceTextPane = jProteinSequenceTextPane;
        this.jScientificNameValueLabel = jScientificNameValueLabel;
        this.proteinView = proteinView;
    }

    @Override
    public void mouseClicked(final MouseEvent e) {
        JTable proteinAmbiguityGroupTable = (JTable) e.getSource();
        jProteinSequenceTextPane.setText("");
        jScientificNameValueLabel.setText("");
        proteoIDViewer.setCursor(new Cursor(Cursor.WAIT_CURSOR));
        int row = proteinAmbiguityGroupTable.getSelectedRow();

        if (row != -1) {
            row = proteinAmbiguityGroupTable.convertRowIndexToModel(row);
            ((DefaultTableModel) proteinView.getDetectionHypothesisTable()
                    .getModel()).setRowCount(0);
            ((DefaultTableModel) proteinView.getIdentificationItemTable()
                    .getModel()).setRowCount(0);
            String pag_id = (String) proteinAmbiguityGroupTable.getModel()
                    .getValueAt(row, 0);
            ProteinAmbiguityGroup proteinAmbiguityGroup = proteoIDViewer.
                    getSingleStorage().findProteinAmbiguityGroupByRef(pag_id);
            List<ProteinDetectionHypothesis> proteinDetectionHypothesisList
                    = proteinAmbiguityGroup
                    .getProteinDetectionHypothesis();
            if (!proteinDetectionHypothesisList.isEmpty()) {
                for (ProteinDetectionHypothesis proteinDetectionHypothesis
                        : proteinDetectionHypothesisList) {
                    DBSequence dBSequence = proteoIDViewer.getSingleStorage().
                            findDBSequenceByRef(proteinDetectionHypothesis.
                                    getDBSequenceRef());
                    boolean isDecoy = proteinView
                            .checkIfProteinDetectionHypothesisIsDecoy(
                                    proteinDetectionHypothesis);
                    List<CvParam> cvParamList = proteinDetectionHypothesis
                            .getCvParam();
                    String score = " ";
                    String number_peptide = " ";
                    for (CvParam cvParam : cvParamList) {
                        if (cvParam.getName().contains("score")) {
                            score = cvParam.getValue();
                        }
                    }

                    String dBSequenceAccession = "";
                    if (dBSequence != null) {
                        dBSequenceAccession = dBSequence.getAccession();
                    }
                    if (proteinDetectionHypothesis.getPeptideHypothesis()
                            != null) {
                        number_peptide = String
                                .valueOf(proteinDetectionHypothesis
                                        .getPeptideHypothesis().size());
                    }

                    ((DefaultTableModel) proteinView
                            .getDetectionHypothesisTable().getModel())
                            .addRow(new Object[]{
                        proteinDetectionHypothesis.getId(),
                        dBSequenceAccession,
                        IdViewerUtils.
                        roundTwoDecimals(Double.parseDouble(score)),
                        "",
                        Integer.valueOf(number_peptide),
                        isDecoy,
                        proteinDetectionHypothesis
                        .isPassThreshold()});
                }
            }
        }

        proteoIDViewer.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    }
}
