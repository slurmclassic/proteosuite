package uk.ac.liv.proteoidviewer.listener;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JTable;

import uk.ac.liv.proteoidviewer.ProteoIDViewerNew;
import uk.ac.liv.proteoidviewer.tabs.ProteinDBViewNew;

public final class PeptideEvidenceTableMouseClickedNew extends MouseAdapter {

    private final ProteoIDViewerNew proteoIDViewer;
    private final ProteinDBViewNew proteinDBView;

    public PeptideEvidenceTableMouseClickedNew(final ProteoIDViewerNew proteoIDViewer,
            final ProteinDBViewNew proteinDBView) {
        this.proteoIDViewer = proteoIDViewer;
        this.proteinDBView = proteinDBView;
    }

    @Override
    public void mouseClicked(final MouseEvent e) {
        JTable spectrumSummaryEvidence = (JTable) e.getSource();
        int row = spectrumSummaryEvidence.getSelectedRow();
        if (row != -1) {
            // row = peptideEvidenceTable.convertRowIndexToModel(row);
            String db_ref = (String) spectrumSummaryEvidence.getValueAt(row, 6);

            int rowCount = proteinDBView.getRowCount();
            for (int i = 0; i < rowCount; i++) {
                if (db_ref.equals((String) proteinDBView.getTable().
                        getValueAt(i, 0))) {

                    proteinDBView.getTable().setRowSelectionInterval(i, i);
                }
            }
        }
        proteoIDViewer.setSelectedIndex(3);
    }
}
