package uk.ac.liv.proteoidviewer.interfaces;

import java.awt.event.ComponentListener;

public interface LazyLoading {

    void load();

    boolean isLoaded();

    void removeComponentListener(ComponentListener componentListener);

}
