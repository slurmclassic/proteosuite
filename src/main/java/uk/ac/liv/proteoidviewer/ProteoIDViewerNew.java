/*
 * ProteoIDViewer.java
 *
 * Created on 15-Dec-2010, 10:34:37
 *
 * Fawaz Ghali
 *
 */
package uk.ac.liv.proteoidviewer;

import java.io.File;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import org.proteosuite.model.MzIdentMLFile;

import uk.ac.ebi.pride.tools.jmzreader.JMzReader;
import uk.ac.ebi.pride.tools.jmzreader.JMzReaderException;
import uk.ac.ebi.pride.tools.jmzreader.model.Spectrum;
import uk.ac.ebi.pride.tools.mgf_parser.MgfFile;
import uk.ac.ebi.pride.tools.mzml_wrapper.MzMlWrapper;
import uk.ac.liv.proteoidviewer.tabs.GlobalStatisticsPanelNew;
import uk.ac.liv.proteoidviewer.tabs.PeptideSummaryNew;
import uk.ac.liv.proteoidviewer.tabs.ProteinDBViewNew;
import uk.ac.liv.proteoidviewer.tabs.ProteinViewNew;
import uk.ac.liv.proteoidviewer.tabs.ProtocolPanelNew;
import uk.ac.liv.proteoidviewer.tabs.SpectrumSummaryNew;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.MzIdentMLSingleStorage;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.PeptideEvidence;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.PeptideEvidenceRef;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.SpectrumIdentificationItem;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.SpectrumIdentificationList;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.SpectrumIdentificationResult;

/**
 *
 * @author Fawaz Ghali
 */
public final class ProteoIDViewerNew extends JTabbedPane {

    private static final long serialVersionUID = 1L;

    private transient JMzReader jMzReader = null;
    private File fileName = null;
    private String rawType = "";

    private transient MzIdentMLSingleStorage singleStorage = null;

    private final GlobalStatisticsPanelNew globalStatisticsPanel
            = new GlobalStatisticsPanelNew(
                    this);
    private final PeptideSummaryNew peptideSummary = new PeptideSummaryNew(this);
    private final ProteinDBViewNew proteinDBView = new ProteinDBViewNew(this);
    {
        proteinDBView.initialise();
    }

    private final ProtocolPanelNew protocolPanel = new ProtocolPanelNew(this);
    {
        protocolPanel.initialise();
    }

    private final SpectrumSummaryNew spectrumSummary = new SpectrumSummaryNew(
            this, proteinDBView);
    private final ProteinViewNew proteinView = new ProteinViewNew(this,
            spectrumSummary, globalStatisticsPanel);
    {
        proteinView.initialise();
    }

    public void initialise() {
        setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
        addTab("Protein View", null, proteinView, "Protein View");
        addTab("Spectrum Summary", null, spectrumSummary, "Spectrum Summary");
        addTab("Peptide Summary", peptideSummary);
        addTab("Protein DB View", proteinDBView);
        addTab("Global Statistics", null, globalStatisticsPanel,
                "Global Statistics");
        addTab("Protocols", protocolPanel);
        repaint();
    }

    public MzIdentMLSingleStorage getSingleStorage() {
        return this.singleStorage;
    }

    public boolean checkIfSpectrumIdentificationItemIsDecoy(
            final SpectrumIdentificationItem spectrumIdentificationItem) {
        boolean result = false;

        List<PeptideEvidenceRef> peptideEvidenceRefList
                = spectrumIdentificationItem.getPeptideEvidenceRef();

        for (PeptideEvidenceRef peptideEvidenceRef : peptideEvidenceRefList) {
            PeptideEvidence peptiedEvidence = singleStorage.
                    findPeptideEvidenceByRef(peptideEvidenceRef);
            if (peptiedEvidence != null && peptiedEvidence.isDecoy()) {
                result = true;
                break;
            }
        }

        return result;
    }

    public void createTables() {
        globalStatisticsPanel.reset();
        proteinDBView.reset();
        proteinView.reset();

        List<SpectrumIdentificationResult> spectrumIdentificationResultList
                = new ArrayList<>();
        List<SpectrumIdentificationItem> spectrumIdentificationItemList
                = new ArrayList<>();

        for (SpectrumIdentificationList list : singleStorage.
                getSpectrumIdentificationLists()) {
            for (SpectrumIdentificationResult result : list) {
                spectrumIdentificationResultList.add(result);
            }
        }

        for (SpectrumIdentificationResult result
                : spectrumIdentificationResultList) {
            for (SpectrumIdentificationItem item : result) {
                spectrumIdentificationItemList.add(item);
            }
        }

        SpectrumIdentificationResult sirWithMostCvParams
                = spectrumIdentificationResultList.stream().max(
                        new Comparator<SpectrumIdentificationResult>() {
                    @Override
                    public int compare(final SpectrumIdentificationResult s1,
                            final SpectrumIdentificationResult s2) {
                        return s1.getCvParam().size() > s2.getCvParam().size()
                                ? 1 : s1.getCvParam().size() < s2.getCvParam().
                                size() ? -1 : 0;
                    }
                }).get();

        List<String> sirParamNameList = sirWithMostCvParams.getCvParam().
                stream().map(p -> p.getName()).collect(Collectors.toList());

        SpectrumIdentificationItem siiWithMostCvParams
                = spectrumIdentificationItemList.stream().max(
                        new Comparator<SpectrumIdentificationItem>() {

                    @Override
                    public int compare(final SpectrumIdentificationItem s1,
                            final SpectrumIdentificationItem s2) {
                        return s1.getCvParam().size() > s2.getCvParam().size()
                                ? 1 : s1.getCvParam().size() < s2.getCvParam().
                                size() ? -1 : 0;
                    }
                }).get();

        List<String> siiParamNameList = siiWithMostCvParams.getCvParam().
                stream().map(p -> p.getName()).collect(Collectors.toList());
        if (siiParamNameList == null) {
            return;
        }

        // TODO: Disabled - Andrew
        /*
	 * / if
	 * (spectrumIdentificationItemTable.getColumnExt("X!Tandem:expect")
	 * != null) {
	 * spectrumIdentificationItemTable.getColumnExt("X!Tandem:expect"
	 * ).setComparator(new DoubleComparator()); } if
	 * (spectrumIdentificationItemTable.getColumnExt("OMSSA:evalue") !=
	 * null) { spectrumIdentificationItemTable.getColumnExt("OMSSA:evalue").
	 * setComparator(new DoubleComparator()); } if
	 * (spectrumIdentificationItemTable
	 * .getColumnExt("mascot:expectation value") != null) {
	 * spectrumIdentificationItemTable
	 * .getColumnExt("mascot:expectation value").setComparator(new
	 * DoubleComparator()); } if
	 * (spectrumIdentificationItemTable.getColumnExt
	 * ("SEQUEST:expectation value") != null) {
	 * spectrumIdentificationItemTable
	 * .getColumnExt("SEQUEST:expectation value").setComparator(new
	 * DoubleComparator()); } /
         */
        String[] spectrumIdentificationItemTableHeaders
                = new String[siiParamNameList.size() + 8];
        spectrumIdentificationItemTableHeaders[0] = "ID";
        spectrumIdentificationItemTableHeaders[1] = "Peptide Sequence";
        spectrumIdentificationItemTableHeaders[2] = "Modification";
        spectrumIdentificationItemTableHeaders[3] = "Calculated MassToCharge";
        spectrumIdentificationItemTableHeaders[4] = "Experimental MassToCharge";
        spectrumIdentificationItemTableHeaders[5] = "Rank";
        spectrumIdentificationItemTableHeaders[6] = "Is Decoy";
        spectrumIdentificationItemTableHeaders[7] = "PassThreshold";

        for (int i = 0; i < siiParamNameList.size(); i++) {
            String string = siiParamNameList.get(i);

            string = string.replaceAll("\\\\", "");

            spectrumIdentificationItemTableHeaders[8 + i] = string;
        }

        spectrumSummary.reset(sirParamNameList, sirParamNameList.size(),
                spectrumIdentificationItemTableHeaders);

        spectrumIdentificationItemTableHeaders = new String[siiParamNameList.
                size() + 7];
        spectrumIdentificationItemTableHeaders[0] = "ID";
        spectrumIdentificationItemTableHeaders[1] = "Peptide Sequence";
        spectrumIdentificationItemTableHeaders[2] = "Theoretical m/z";
        spectrumIdentificationItemTableHeaders[3] = "Experimental m/z";
        spectrumIdentificationItemTableHeaders[4] = "Rank";
        spectrumIdentificationItemTableHeaders[5] = "Is Decoy";
        spectrumIdentificationItemTableHeaders[6] = "PassThreshold";

        for (int i = 0; i < siiParamNameList.size(); i++) {
            String string = siiParamNameList.get(i);

            string = string.replaceAll("\\\\", "");

            spectrumIdentificationItemTableHeaders[7 + i] = string;
        }

        peptideSummary.reset(spectrumIdentificationItemTableHeaders);
        // peptideEvidenceTablePeptideView.setAutoCreateRowSorter(true);
    }

    public GlobalStatisticsPanelNew globalStatisticsPanel() {
        return globalStatisticsPanel;
    }

    public ProteinViewNew getProteinView() {
        return proteinView;
    }

    public void setMzIdentMLFile(final MzIdentMLFile identDataFile) {
        singleStorage = identDataFile.getSingleStorage();
        fileName = identDataFile.getFile();

        String versionNumber = singleStorage.getVersion();
        if (!versionNumber.startsWith("1.1.")
                && !versionNumber.startsWith(
                        "1.2.")) {
            JOptionPane
                    .showMessageDialog(
                            this,
                            "The file is not compatible with the Viewer: different mzIdentMl version",
                            "mzIdentMl version",
                            JOptionPane.INFORMATION_MESSAGE);
            return;
        }
        jMzReader = null;
        rawType = "";

        if (identDataFile.getParent() != null) {
            try {
                File file = identDataFile.getParent().getFile();
                if (file.getAbsolutePath().toLowerCase().endsWith("mgf")) {
                    jMzReader = new MgfFile(file);
                    rawType = "mgf";
                } else if (file.getAbsolutePath().toLowerCase().endsWith("mzml")) {
                    jMzReader = new MzMlWrapper(file);
                    rawType = "mzML";
                } else {
                    JOptionPane.showMessageDialog(null, file.getName()
                            + " is not supported", "Spectrum file",
                            JOptionPane.INFORMATION_MESSAGE);
                }
            } catch (JMzReaderException ex) {
                ex.printStackTrace();
            }
        }

        createTables();
        setSelectedIndex(0);
    }

    public boolean isRawAvailable() {
        if (jMzReader != null) {
            return true;
        }

        return false;
    }

    public Spectrum getRawSpectrum(final String id) throws JMzReaderException {
        if (rawType.equals("mgf")) {
            String spectrumIndex = id.substring(6);
            int index1 = Integer.parseInt(spectrumIndex) + 1;
            return jMzReader.getSpectrumById(String.valueOf(index1));
        }

        return jMzReader.getSpectrumById(id);
    }

    public File getFileName() {
        return fileName;
    }
}
