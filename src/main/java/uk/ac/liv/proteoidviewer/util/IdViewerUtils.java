package uk.ac.liv.proteoidviewer.util;

public final class IdViewerUtils {
    /**
     * Private constructor to prevent instantiation.
     */
    private IdViewerUtils() { }

    public static double roundThreeDecimals(final double d) {
        double multipicationFactor = Math.pow(10, 3);
        return Math.round(d * multipicationFactor) / multipicationFactor;
    }

    public static double roundTwoDecimals(final double d) {
        double multipicationFactor = Math.pow(10, 2);
        return Math.round(d * multipicationFactor) / multipicationFactor;
    }
}
