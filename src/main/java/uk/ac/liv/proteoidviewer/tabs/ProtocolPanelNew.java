package uk.ac.liv.proteoidviewer.tabs;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;

import uk.ac.liv.proteoidviewer.ProteoIDViewerNew;
import uk.ac.liv.proteoidviewer.interfaces.LazyLoading;
import uk.ac.liv.proteoidviewer.listener.LazyLoadingComponentListener;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.AnalysisProtocolCollection;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.CvParam;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.Enzyme;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.Enzymes;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.ModificationParams;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.Param;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.ProteinDetectionProtocol;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.SearchModification;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.SpectrumIdentificationProtocol;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.Tolerance;

public final class ProtocolPanelNew extends JPanel implements LazyLoading {

    private static final long serialVersionUID = 1L;
    private final JTextPane protocolText = new JTextPane();
    private final ProteoIDViewerNew proteoIDViewer;

    private boolean isLoaded = false;

    public ProtocolPanelNew(final ProteoIDViewerNew proteoIDViewer) {
        this.proteoIDViewer = proteoIDViewer;        
        protocolText.setContentType("text/html");
        protocolText.setEditable(false);
    }
    
    public void initialise() {
        addComponentListener(new LazyLoadingComponentListener());
        setLayout(new BorderLayout());
        setBorder(BorderFactory.createTitledBorder("Summary"));
        add(new JScrollPane(protocolText), BorderLayout.CENTER);
    }

    @Override
    public boolean isLoaded() {
        return isLoaded;
    }

    @Override
    public void load() {
        setProtocolText("");
        setCursor(new Cursor(Cursor.WAIT_CURSOR));

        AnalysisProtocolCollection analysisProtocolCollection = proteoIDViewer.
                getSingleStorage().getAnalysisProcolCollection();

        List<SpectrumIdentificationProtocol> spectrumIdentificationProtocol
                = analysisProtocolCollection
                .getSpectrumIdentificationProtocol();
        ProteinDetectionProtocol proteinDetectionProtocol
                = analysisProtocolCollection
                .getProteinDetectionProtocol();

        StringBuilder textBuilder = new StringBuilder();

        if (spectrumIdentificationProtocol != null) {
            textBuilder.append(
                    "<html><font color=red>Spectrum Identification Protocol</font><BR>");
            for (SpectrumIdentificationProtocol sip
                    : spectrumIdentificationProtocol) {
                Param param = sip.getSearchType();
                if (param != null) {
                    CvParam cvParam = param.getCvParam();
                    if (cvParam != null) {
                        textBuilder.append("<B>Type of search:</B> ");
                        textBuilder.append(cvParam.getName());
                        textBuilder.append("<BR>");
                    }
                }

                Enzymes enzymes = sip.getEnzymes();
                if (enzymes != null) {
                    List<Enzyme> enzymesList = enzymes.getEnzyme();
                    for (Enzyme enzyme : enzymesList) {
                        if (enzyme != null && enzyme.getEnzymeName() != null) {
                            List<CvParam> cvParamList = enzyme
                                    .getEnzymeName().getCvParam();
                            for (CvParam cvParam : cvParamList) {
                                textBuilder.append("<B>Enzyme:</B> ");
                                textBuilder.append(cvParam.getName());
                                textBuilder.append("<BR>");
                            }
                        }
                    }
                }

                ModificationParams modificationParams = sip.
                        getModificationParams();
                if (modificationParams != null) {
                    List<SearchModification> searchModificationList
                            = modificationParams
                            .getSearchModification();
                    for (SearchModification searchModification
                            : searchModificationList) {
                        String mod = "";
                        if (searchModification.isFixedMod()) {
                            mod = "(fixed)";
                        } else {
                            mod = "(variable)";
                        }

                        textBuilder.append("<B>Search Modification:</B> Type ");
                        textBuilder.append(mod);
                        textBuilder.append(" Residues: ");
                        textBuilder.append(searchModification.getResidues());
                        textBuilder.append(" Mass Delta: ");
                        textBuilder.append(searchModification.getMassDelta());
                        textBuilder.append("<BR>");
                    }
                }

                Tolerance tolerance = sip.getFragmentTolerance();
                if (tolerance != null) {
                    String t1 = "";
                    String t2 = "";
                    for (CvParam cvParam : tolerance.getCvParam()) {
                        if (cvParam.getName().equals(
                                "search tolerance plus value")) {
                            t1 = "+" + cvParam.getValue() + " Da ";
                        }
                        if (cvParam.getName().equals(
                                "search tolerance minus value")) {
                            t2 = "-" + cvParam.getValue() + " Da ";
                        }
                    }

                    textBuilder.append("<B>Fragment Tolerance: </B>");
                    textBuilder.append(t1);
                    textBuilder.append(" / ");
                    textBuilder.append(t2);
                    textBuilder.append("<BR>");
                }

                Tolerance toleranceParent = sip.getParentTolerance();
                if (toleranceParent != null) {
                    String t1 = "";
                    String t2 = "";
                    List<CvParam> cvParamList = toleranceParent.getCvParam();
                    for (CvParam cvParam : cvParamList) {
                        if (cvParam.getName().equals(
                                "search tolerance plus value")) {
                            t1 = "+" + cvParam.getValue() + " Da ";
                        }
                        if (cvParam.getName().equals(
                                "search tolerance minus value")) {
                            t2 = "-" + cvParam.getValue() + " Da ";
                        }
                    }

                    textBuilder.append("<B>Parent Fragment Tolerance: </B>");
                    textBuilder.append(t1);
                    textBuilder.append(" / ");
                    textBuilder.append(t2);
                    textBuilder.append("<BR>");
                }

                String threshold = sip.getThreshold().getCvParam().get(0).
                        getValue();
                if (threshold != null) {
                    textBuilder.append("<B>Threshold: </B>");
                    textBuilder.append(threshold);
                    textBuilder.append("<BR>");
                }
            }

        }
        if (proteinDetectionProtocol != null) {
            textBuilder.append(
                    "<html><font color=red>Protein Detection Protocol</font><BR>");
            String threshold = proteinDetectionProtocol.getThreshold()
                    .getCvParam().get(0).getValue();
            if (threshold != null) {
                textBuilder.append("<B>Threshold: </B>");
                textBuilder.append(threshold);
                textBuilder.append("<BR>");
            }

        }
        setProtocolText(textBuilder.toString());
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        isLoaded = true;
    }

    public void setProtocolText(final String string) {
        protocolText.setText(string);
    }
}
