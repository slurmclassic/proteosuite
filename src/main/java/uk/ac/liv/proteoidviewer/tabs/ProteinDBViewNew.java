package uk.ac.liv.proteoidviewer.tabs;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.util.Iterator;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

import uk.ac.liv.proteoidviewer.ProteoIDViewerNew;
import uk.ac.liv.proteoidviewer.interfaces.LazyLoading;
import uk.ac.liv.proteoidviewer.listener.LazyLoadingComponentListener;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.DBSequence;

public final class ProteinDBViewNew extends JPanel implements LazyLoading {

    private static final long serialVersionUID = 1L;
    private static final String[] DB_SEQUENCE_TABLE_HEADERS = new String[]{"ID",
        "Accession", "Seq", "Protein Description"};

    private final JTable dBSequenceTable = new JTable();
    private final ProteoIDViewerNew proteoIDViewer;

    private boolean isLoaded = false;

    public ProteinDBViewNew(final ProteoIDViewerNew proteoIDViewer) {
        this.proteoIDViewer = proteoIDViewer;

        // dBSequenceTable Table
        dBSequenceTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        dBSequenceTable.getTableHeader().setReorderingAllowed(false);
    }

    public void initialise() {
        addComponentListener(new LazyLoadingComponentListener());
        setLayout(new BorderLayout());
        add(new JScrollPane(dBSequenceTable));
        setBorder(BorderFactory.createTitledBorder("DB Sequence"));
    }

    public int getRowCount() {
        return dBSequenceTable.getModel().getRowCount();
    }

    public JTable getTable() {
        return dBSequenceTable;
    }

    @Override
    public boolean isLoaded() {
        return isLoaded;
    }

    @Override
    public void load() {

        this.setCursor(new Cursor(Cursor.WAIT_CURSOR));
        Iterator<DBSequence> dbseqIterator = proteoIDViewer.getSingleStorage().
                getSequenceCollection().getDBSequence();

        while (dbseqIterator.hasNext()) {
            DBSequence dBSequence = dbseqIterator.next();

            String cv = "";
            if (dBSequence.getCvParam() != null && dBSequence.getCvParam().
                    size() > 0) {
                cv = dBSequence.getCvParam().get(0).getValue();
            }

            ((DefaultTableModel) dBSequenceTable.getModel())
                    .addRow(new String[]{dBSequence.getId(),
                dBSequence.getAccession(), dBSequence.getSeq(), cv});

        }
        this.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        isLoaded = true;
    }

    public void reset() {
        // dBSequence view dBSequenceTable
        // dBSequenceTable.setAutoCreateRowSorter(true);
        dBSequenceTable.setModel(new DefaultTableModel(new Object[][]{},
                DB_SEQUENCE_TABLE_HEADERS) {
            private static final long serialVersionUID = 1L;

            @Override
            public boolean isCellEditable(final int row, final int col) {
                return false;
            }
        });
        ((DefaultTableModel) dBSequenceTable.getModel()).setRowCount(0);
    }
}
