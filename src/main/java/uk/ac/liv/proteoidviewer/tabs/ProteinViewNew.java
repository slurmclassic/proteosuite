package uk.ac.liv.proteoidviewer.tabs;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

import uk.ac.liv.proteoidviewer.ProteoIDViewerNew;
import uk.ac.liv.proteoidviewer.interfaces.LazyLoading;
import uk.ac.liv.proteoidviewer.listener.LazyLoadingComponentListener;
import uk.ac.liv.proteoidviewer.listener.ProteinAmbiguityGroupTableMouseClickedNew;
import uk.ac.liv.proteoidviewer.listener.ProteinDetectionHypothesisTableMouseClickedNew;
import uk.ac.liv.proteoidviewer.listener.SpectrumIdentificationItemProteinTableMouseClickedNew;
import uk.ac.liv.proteoidviewer.util.IdViewerUtils;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.CvParam;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.DBSequence;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.PeptideEvidence;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.PeptideHypothesis;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.ProteinAmbiguityGroup;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.ProteinDetectionHypothesis;
import uk.ac.liv.pgb.psi.utils.ident.standard.utils.CollectionUtils;

public final class ProteinViewNew extends JPanel implements LazyLoading {

    private static final String[] SPECTRUM_IDENTIFICATION_ITEM_PROTEIN_VIEW_TABLE_HEADERS
            = new String[]{
                "Peptide Sequence", "SII", "Name", "Score", "Expectation value",
                "passThreshold"};
    private static final String[] PROTEIN_DETECTION_HYPOTHESIS_TABLE_HEADERS
            = new String[]{
                "ID", "Accession", "Scores", "P-values", "Number of peptides",
                "Is Decoy", "passThreshold"};
    private static final String[] PROTEIN_AMBIGUITY_GROUP_TABLE_HEADERS
            = new String[]{
                "ID", "Name", "Protein Accessions", "Representative Protein",
                "Scores", "P-values", "Number of peptides", "Is Decoy",
                "passThreshold"};

    private static final long serialVersionUID = 1L;

    private final JTable spectrumIdentificationItemProteinViewTable
            = new JTable();
    private final JTable proteinDetectionHypothesisTable = new JTable();
    private final JTable proteinAmbiguityGroupTable = new JTable();

    private final JTextArea proteinDescription = new JTextArea();
    private final JTextArea proteinSequence = new JTextArea();
    private final JLabel scientificNameValue = new JLabel();

    private final ProteoIDViewerNew proteoIDViewer;
    private final GlobalStatisticsPanelNew globalStatisticsPanel;
    private final JPanel leftPanel;
    private final JPanel jProteinInfoPanel;

    private boolean isLoaded = false;    

    public ProteinViewNew(final ProteoIDViewerNew proteoIDViewer,
            final SpectrumSummaryNew spectrumSummary,
            final GlobalStatisticsPanelNew globalStatisticsPanel) {
        this.proteoIDViewer = proteoIDViewer;
        this.globalStatisticsPanel = globalStatisticsPanel;

        createTables(proteoIDViewer, spectrumSummary);
        proteinDescription.setEditable(false);
        proteinSequence.setEditable(false);

        final JPanel jProteinDescriptionPanel = new JPanel(new BorderLayout());
        jProteinDescriptionPanel.setBorder(BorderFactory
                .createTitledBorder("Protein Description"));
        jProteinDescriptionPanel.add(new JScrollPane(proteinDescription),
                BorderLayout.CENTER);

        final JPanel jProteinSequencePanel = new JPanel(new BorderLayout());
        jProteinSequencePanel.setBorder(BorderFactory
                .createTitledBorder("Protein Sequence"));
        jProteinSequencePanel.add(new JScrollPane(proteinSequence),
                BorderLayout.CENTER);

        jProteinInfoPanel = new JPanel(new GridLayout(5, 1));
        jProteinInfoPanel.setBorder(BorderFactory
                .createTitledBorder("Protein Info"));
        jProteinInfoPanel.setInheritsPopupMenu(true);
        JPanel scientificNamePanel = new JPanel(new FlowLayout());
        scientificNamePanel.add(new JLabel("Scientific name:"));
        scientificNamePanel.add(scientificNameValue);
        jProteinInfoPanel.add(scientificNamePanel);
        jProteinInfoPanel.add(jProteinDescriptionPanel);
        jProteinInfoPanel.add(jProteinSequencePanel);

        final JPanel jSpectrumIdentificationItemProteinPanel = new JPanel();
        jSpectrumIdentificationItemProteinPanel.setBorder(BorderFactory
                .createTitledBorder("Peptide-Spectrum matches"));
        jSpectrumIdentificationItemProteinPanel
                .setToolTipText("Protein Detection Hypothesis");
        jSpectrumIdentificationItemProteinPanel.setPreferredSize(new Dimension(
                772, 150));
        jSpectrumIdentificationItemProteinPanel.getAccessibleContext()
                .setAccessibleDescription("Spectrum Identification Item");
        jSpectrumIdentificationItemProteinPanel.setLayout(new BorderLayout());
        jSpectrumIdentificationItemProteinPanel.add(new JScrollPane(
                spectrumIdentificationItemProteinViewTable));
        jProteinInfoPanel.add(jSpectrumIdentificationItemProteinPanel);

        leftPanel = new JPanel(new GridLayout(2, 1));

        final JPanel jProteinAmbiguityGroupPanel = new JPanel();
        jProteinAmbiguityGroupPanel.setBorder(BorderFactory
                .createTitledBorder("Protein Group"));
        jProteinAmbiguityGroupPanel
                .setToolTipText(
                        "groups of proteins sharing some or all of the same peptides");
        jProteinAmbiguityGroupPanel.setPreferredSize(new Dimension(772, 150));
        jProteinAmbiguityGroupPanel.setLayout(new BorderLayout());
        jProteinAmbiguityGroupPanel.add(new JScrollPane(
                proteinAmbiguityGroupTable));

        leftPanel.add(jProteinAmbiguityGroupPanel);

        final JPanel jProteinDetectionHypothesisPanel = new JPanel();
        jProteinDetectionHypothesisPanel.setBorder(BorderFactory
                .createTitledBorder("Protein"));
        jProteinDetectionHypothesisPanel
                .setToolTipText(
                        "proteins inferred based on a set of peptide spectrum matches");
        jProteinDetectionHypothesisPanel.setPreferredSize(new Dimension(772,
                150));
        jProteinDetectionHypothesisPanel.setLayout(new BorderLayout());
        jProteinDetectionHypothesisPanel.add(new JScrollPane(
                proteinDetectionHypothesisTable));
        leftPanel.add(jProteinDetectionHypothesisPanel);
    }
    
    public void initialise() {
        addComponentListener(new LazyLoadingComponentListener());
        setLayout(new GridLayout(1, 2));
        setToolTipText("Protein View");
        setName("Protein View"); // NOI18N
        setPreferredSize(new Dimension(889, 939));
        add(leftPanel);
        add(jProteinInfoPanel);
        getAccessibleContext().setAccessibleName("Protein View");
    }

    public boolean checkIfProteinDetectionHypothesisIsDecoy(
            final ProteinDetectionHypothesis proteinDetectionHypothesis) {
        boolean result = false;
        List<PeptideHypothesis> PeptideHyposthesisList
                = proteinDetectionHypothesis
                .getPeptideHypothesis();
        for (PeptideHypothesis PeptideHyposthesisList1 : PeptideHyposthesisList) {
            try {
                PeptideHypothesis peptideHypothesis = PeptideHyposthesisList1;
                String peptideRef = peptideHypothesis.getPeptideEvidenceRef();
                PeptideEvidence peptideEvidence = proteoIDViewer.
                        getSingleStorage().findPeptideEvidenceByRef(peptideRef);
                if (peptideEvidence.isDecoy()) {
                    result = true;
                    break;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return result;
    }

    private void createTables(final ProteoIDViewerNew proteoIDViewer,
            final SpectrumSummaryNew spectrumSummary) {
        spectrumIdentificationItemProteinViewTable.setAutoCreateRowSorter(true);
        proteinDetectionHypothesisTable.setAutoCreateRowSorter(true);
        proteinAmbiguityGroupTable.setAutoCreateRowSorter(true);

        spectrumIdentificationItemProteinViewTable
                .setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        proteinDetectionHypothesisTable
                .setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        proteinAmbiguityGroupTable
                .setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        spectrumIdentificationItemProteinViewTable.getTableHeader()
                .setReorderingAllowed(false);
        proteinDetectionHypothesisTable.getTableHeader().setReorderingAllowed(
                false);
        proteinAmbiguityGroupTable.getTableHeader().setReorderingAllowed(false);

        spectrumIdentificationItemProteinViewTable
                .addMouseListener(
                        new SpectrumIdentificationItemProteinTableMouseClickedNew(
                                proteoIDViewer, spectrumSummary));
        proteinDetectionHypothesisTable
                .addMouseListener(
                        new ProteinDetectionHypothesisTableMouseClickedNew(
                                proteoIDViewer, this,
                                scientificNameValue, proteinDescription,
                                proteinSequence));
        proteinAmbiguityGroupTable
                .addMouseListener(new ProteinAmbiguityGroupTableMouseClickedNew(
                        proteoIDViewer, proteinSequence, scientificNameValue,
                        this));

        proteinDetectionHypothesisTable.setCursor(Cursor.getPredefinedCursor(
                Cursor.HAND_CURSOR));
        proteinAmbiguityGroupTable.setCursor(Cursor.getPredefinedCursor(
                Cursor.HAND_CURSOR));
        spectrumIdentificationItemProteinViewTable.setCursor(Cursor.
                getPredefinedCursor(Cursor.HAND_CURSOR));
    }

    public JTable getAmbiguityGroupTable() {
        return proteinAmbiguityGroupTable;
    }

    public JTable getDetectionHypothesisTable() {
        return proteinDetectionHypothesisTable;
    }

    public JTable getIdentificationItemTable() {
        return spectrumIdentificationItemProteinViewTable;
    }

    @Override
    public boolean isLoaded() {
        return isLoaded;
    }

    @Override
    public void load() {

        this.setCursor(new Cursor(Cursor.WAIT_CURSOR));
        String protein_accessions = "";
        globalStatisticsPanel.getPDHListPassThreshold().clear();

        Iterable<ProteinAmbiguityGroup> groupIterable = CollectionUtils.concat(
                proteoIDViewer.getSingleStorage().getProteinDetectionLists());

        List<ProteinDetectionHypothesis> proteinDetectionHypothesisList;
        for (ProteinAmbiguityGroup group : groupIterable) {

            protein_accessions = "";
            proteinDetectionHypothesisList = group
                    .getProteinDetectionHypothesis();
            boolean anchorProtein = false;
            String anchorProteinAccession = "";
            // mzid 1.2
            boolean leadProtein = false;
            String leadProteinAccession = "";
            boolean groupRepresentativeProtein = false;
            String groupRepresentativeProteinAccession = "";

            boolean isDecoy = false;
            String score = " ";
            String number_peptide = " ";
            boolean isPassThreshold = false;
            if (!proteinDetectionHypothesisList.isEmpty()) {
                for (int j = 0; j < proteinDetectionHypothesisList.size(); j++) {
                    try {
                        ProteinDetectionHypothesis proteinDetectionHypothesis
                                = proteinDetectionHypothesisList
                                .get(j);

                        DBSequence dBSequence = proteoIDViewer.
                                getSingleStorage().findDBSequenceByRef(
                                        proteinDetectionHypothesis.
                                        getDBSequenceRef());

                        if (dBSequence.getAccession() != null) {
                            protein_accessions = protein_accessions
                                    + dBSequence.getAccession() + ";";
                        }

                        if (proteinDetectionHypothesis.isPassThreshold()) {
                            globalStatisticsPanel.getPDHListPassThreshold()
                                    .add(proteinDetectionHypothesis);
                        }

                        List<CvParam> cvParamList = proteinDetectionHypothesis
                                .getCvParam();
                        for (int i = 0; i < cvParamList.size(); i++) {
                            CvParam cvParam = cvParamList.get(i);
                            if (proteoIDViewer.getSingleStorage().getVersion()
                                    .startsWith("1.1.")
                                    && cvParam.getName().equals(
                                            "anchor protein")) {
                                anchorProtein = true;
                                anchorProteinAccession = dBSequence
                                        .getAccession();
                                isDecoy
                                        = checkIfProteinDetectionHypothesisIsDecoy(
                                                proteinDetectionHypothesis);
                                if (proteinDetectionHypothesis
                                        .getPeptideHypothesis() != null) {
                                    number_peptide = String
                                            .valueOf(proteinDetectionHypothesis
                                                    .getPeptideHypothesis()
                                                    .size());
                                }
                                isPassThreshold = proteinDetectionHypothesis
                                        .isPassThreshold();
                            } else if (proteoIDViewer.getSingleStorage().
                                    getVersion().startsWith("1.2.")
                                    && cvParam.getName().equals(
                                            "group representative")) {
                                groupRepresentativeProtein = true;
                                groupRepresentativeProteinAccession = dBSequence
                                        .getAccession();
                                isDecoy
                                        = checkIfProteinDetectionHypothesisIsDecoy(
                                                proteinDetectionHypothesis);
                                if (proteinDetectionHypothesis
                                        .getPeptideHypothesis() != null) {
                                    number_peptide = String
                                            .valueOf(proteinDetectionHypothesis
                                                    .getPeptideHypothesis()
                                                    .size());
                                }
                                isPassThreshold = proteinDetectionHypothesis
                                        .isPassThreshold();

                            } else if (proteoIDViewer.getSingleStorage().
                                    getVersion().startsWith("1.2.")
                                    && cvParam.getName().equals(
                                            "leading protein")) {
                                leadProtein = true;
                                leadProteinAccession = dBSequence
                                        .getAccession();
                                isDecoy
                                        = checkIfProteinDetectionHypothesisIsDecoy(
                                                proteinDetectionHypothesis);
                                if (proteinDetectionHypothesis
                                        .getPeptideHypothesis() != null) {
                                    number_peptide = String
                                            .valueOf(proteinDetectionHypothesis
                                                    .getPeptideHypothesis()
                                                    .size());
                                }
                                isPassThreshold = proteinDetectionHypothesis
                                        .isPassThreshold();

                            }
                            if (cvParam.getName().contains("score")) {
                                score = cvParam.getValue();
                            }

                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                }
            }
            protein_accessions = protein_accessions.substring(0,
                    protein_accessions.length() - 1);
            if (proteoIDViewer.getSingleStorage().getVersion().
                    startsWith("1.1.")
                    && anchorProtein) {
                ((DefaultTableModel) getAmbiguityGroupTable().getModel())
                        .addRow(new Object[]{
                    group.getId(),
                    group.getName(),
                    protein_accessions,
                    anchorProteinAccession,
                    IdViewerUtils.roundTwoDecimals(Double.parseDouble(score)),
                    " ",
                    Integer.valueOf(number_peptide),
                    String.valueOf(isDecoy),
                    String.valueOf(isPassThreshold)});
            } else if (proteoIDViewer.getSingleStorage().getVersion().
                    startsWith(
                            "1.2.")
                    && groupRepresentativeProtein) {
                ((DefaultTableModel) getAmbiguityGroupTable().getModel())
                        .addRow(new Object[]{
                    group.getId(),
                    group.getName(),
                    protein_accessions,
                    groupRepresentativeProteinAccession,
                    IdViewerUtils.roundTwoDecimals(Double.parseDouble(score)),
                    " ",
                    Integer.valueOf(number_peptide),
                    String.valueOf(isDecoy),
                    String.valueOf(isPassThreshold)});

            } else if (proteoIDViewer.getSingleStorage().getVersion().
                    startsWith(
                            "1.2.")
                    && leadProtein) {
                ((DefaultTableModel) getAmbiguityGroupTable().getModel())
                        .addRow(new Object[]{
                    group.getId(),
                    group.getName(),
                    protein_accessions,
                    leadProteinAccession,
                    IdViewerUtils.roundTwoDecimals(Double.parseDouble(score)),
                    " ",
                    Integer.valueOf(number_peptide),
                    String.valueOf(isDecoy),
                    String.valueOf(isPassThreshold)});

            } else {
                ((DefaultTableModel) getAmbiguityGroupTable().getModel())
                        .addRow(new Object[]{group.getId(),
                    group.getName(),
                    protein_accessions, " ", " ", " ", " ", " ",
                    " "});
            }
        }

        this.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        isLoaded = true;
    }

    public void reset() {
        spectrumIdentificationItemProteinViewTable
                .setModel(new DefaultTableModel(new Object[][]{},
                SPECTRUM_IDENTIFICATION_ITEM_PROTEIN_VIEW_TABLE_HEADERS) {
                    private static final long serialVersionUID = 1L;
                });
        ((DefaultTableModel) spectrumIdentificationItemProteinViewTable
                .getModel()).setRowCount(0);

        // spectrumIdentificationItemProteinViewTable.setAutoCreateRowSorter(true);
        proteinDetectionHypothesisTable.setModel(new DefaultTableModel(
                new Object[][]{}, PROTEIN_DETECTION_HYPOTHESIS_TABLE_HEADERS) {
            private static final long serialVersionUID = 1L;
        });
        ((DefaultTableModel) proteinDetectionHypothesisTable.getModel())
                .setRowCount(0);
        // proteinDetectionHypothesisTable.setAutoCreateRowSorter(true);

        proteinAmbiguityGroupTable.setModel(new DefaultTableModel(
                new Object[][]{}, PROTEIN_AMBIGUITY_GROUP_TABLE_HEADERS) {
            private static final long serialVersionUID = 1L;
        });
        ((DefaultTableModel) proteinAmbiguityGroupTable.getModel())
                .setRowCount(0);
        // proteinAmbiguityGroupTable.setAutoCreateRowSorter(true);

        proteinDescription.setText("");
        proteinSequence.setText("");
    }
}
