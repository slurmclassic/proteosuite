package uk.ac.liv.proteoidviewer.tabs;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import org.apache.commons.lang.StringEscapeUtils;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.Peptide;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.PeptideEvidence;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.PeptideEvidenceRef;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.ProteinDetectionHypothesis;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.SpectrumIdentificationItem;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.SpectrumIdentificationList;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.SpectrumIdentificationResult;
import uk.ac.liv.pgb.psi.utils.ident.standard.utils.CollectionUtils;
import uk.ac.liv.proteoidviewer.ProteoIDViewerNew;
import uk.ac.liv.proteoidviewer.interfaces.LazyLoading;
import uk.ac.liv.proteoidviewer.listener.LazyLoadingComponentListener;
import uk.ac.liv.proteoidviewer.listener.ManualCalculateActionPerformedNew;
import uk.ac.liv.proteoidviewer.listener.ManualDecoyActionPerformedNew;
import uk.ac.liv.proteoidviewer.listener.SiiComboBoxActionPerformedNew;
import uk.ac.liv.proteoidviewer.util.IdViewerUtils;

public final class GlobalStatisticsPanelNew extends JPanel implements LazyLoading {

    private static final long serialVersionUID = 1L;
    private final ProteoIDViewerNew proteoIDViewer;

    private final JLabel tpSiiValue = new JLabel("0");
    private final JLabel fdrSiiValue = new JLabel("0");
    private final JLabel percentIdentifiedSpectraLabelValue = new JLabel("0");
    private final JLabel fpSiiValue = new JLabel("0");
    private final JLabel isDecoySiiFalseValue = new JLabel("0");
    private final JLabel isDecoySiiValue = new JLabel("0");
    private final JLabel totalPAGsLabelValue = new JLabel("0");
    private final JLabel totalPDHsaboveThresholdLabelValue = new JLabel("0");
    private final JLabel totalPeptidesaboveThresholdLabelValue = new JLabel("0");
    private final JLabel totalSIIaboveThresholdLabelValue = new JLabel("0");
    private final JLabel totalSIIaboveThresholdRankOneLabelValue = new JLabel(
            "0");
    private final JLabel totalSIIbelowThresholdLabelValue = new JLabel("0");
    private final JLabel totalSIRLabelValue = new JLabel("0");
    private final JPanel tpEvaluePanel = new JPanel();
    private final JPanel tpQvaluePanel = new JPanel();
    private final JComboBox<String> jComboBox1 = new JComboBox<>();
    private final JComboBox<String> siiComboBox = new JComboBox<>();
    private final JPanel fdrPanel = new JPanel();
    private final Map<String, String> cvTermMap = new HashMap<>();
    private final List<SpectrumIdentificationItem> sIIListPassThreshold
            = new ArrayList<>();
    private final List<ProteinDetectionHypothesis> pDHListPassThreshold
            = new ArrayList<>();

    public double falsePositiveSii;
    public double truePositiveSii;
    public double fdrSii;
    private boolean isLoaded = false;

    public GlobalStatisticsPanelNew(final ProteoIDViewerNew proteoIDViewer) {
        addComponentListener(new LazyLoadingComponentListener());
        this.proteoIDViewer = proteoIDViewer;

        siiComboBox.addActionListener(new SiiComboBoxActionPerformedNew(
                this));

        fdrPanel.setBorder(BorderFactory.createTitledBorder("FDR Graph"));
        fdrPanel.setPreferredSize(new Dimension(257, 255));

        createPanel();
    }

    private void createPanel() {
        final JTextField manualDecoyRatioValue = new JTextField("1");
        manualDecoyRatioValue.setEnabled(false);
        final JTextField manualDecoyPrefixValue = new JTextField("Rev_");
        manualDecoyPrefixValue.setEnabled(false);
        final JComboBox<String> jComboBox2 = new JComboBox<>(new String[]{
            "Better scores are lower", "Better scores are higher"});

        tpEvaluePanel.setBorder(BorderFactory
                .createTitledBorder("TP vs FP vs E-value"));
        tpEvaluePanel.setPreferredSize(new Dimension(257, 255));

        tpQvaluePanel.setBorder(BorderFactory
                .createTitledBorder("TP vs Q-value"));
        tpQvaluePanel.setPreferredSize(new Dimension(257, 255));

        final JCheckBox manualDecoy = new JCheckBox();
        manualDecoy.addActionListener(new ManualDecoyActionPerformedNew(
                manualDecoyPrefixValue, manualDecoyRatioValue));

        final JButton manualCalculate = new JButton("Calculate / Show graphs");

        ManualCalculateActionPerformedNew.Builder  builder = new ManualCalculateActionPerformedNew.Builder();
        builder.proteoIDViewer(proteoIDViewer).siiComboBox(siiComboBox).manualDecoy(manualDecoy)
                .manualDecoyPrefixValue(manualDecoyPrefixValue).isDecoySiiValue(isDecoySiiValue)
                .isDecoySiiFalseValue(isDecoySiiFalseValue).manualDecoyRatioValue(manualDecoyRatioValue)
                .fpSiiValue(fpSiiValue).siiListPassThreshold(sIIListPassThreshold).tpSiiValue(tpSiiValue)
                .fdrSiiValue(fdrSiiValue).fdrPanel(fdrPanel).jComboBox2(jComboBox2).cvTermMap(cvTermMap)
                .jComboBox1(jComboBox1).tpEvaluePanel(tpEvaluePanel).tpQvaluePanel(tpQvaluePanel)
                .globalStatisticsPanel(this);
        manualCalculate.addActionListener(builder.build());

        JPanel leftSummary = new JPanel(new GridLayout(11, 2));
        leftSummary.add(new JLabel("SII List:"));
        leftSummary.add(siiComboBox);
        leftSummary.add(new JLabel("Total SIR:"));
        leftSummary.add(totalSIRLabelValue);
        leftSummary.add(new JLabel("Total PSM:"));
        leftSummary.add(Box.createGlue());
        leftSummary.add(new JLabel("Total PSM below Threshold:"));
        leftSummary.add(totalSIIbelowThresholdLabelValue);
        leftSummary.add(new JLabel("Total PSM pass Threshold:"));
        leftSummary.add(totalSIIaboveThresholdLabelValue);
        leftSummary
                .add(new JLabel("Total PSM pass Threshold and where rank=1:"));
        leftSummary.add(totalSIIaboveThresholdRankOneLabelValue);
        leftSummary.add(new JLabel("Total PAGs:"));
        leftSummary.add(totalPAGsLabelValue);
        leftSummary.add(new JLabel("Total PDHs:"));
        leftSummary.add(Box.createGlue());
        leftSummary.add(new JLabel("Total PDH above Threshold:"));
        leftSummary.add(totalPDHsaboveThresholdLabelValue);
        leftSummary.add(new JLabel("Percent identified spectra:"));
        leftSummary.add(percentIdentifiedSpectraLabelValue);
        leftSummary.add(new JLabel(
                "Total non-redundant peptides above Threshold:"));
        leftSummary.add(totalPeptidesaboveThresholdLabelValue);

        JPanel rightSummary = new JPanel(new GridLayout(11, 2));
        rightSummary.add(new JLabel("PSM with Decoy = true:"));
        rightSummary.add(isDecoySiiValue);
        rightSummary.add(new JLabel("PSM with Decoy = false:"));
        rightSummary.add(isDecoySiiFalseValue);
        rightSummary.add(new JLabel("FP for PSM:"));
        rightSummary.add(fpSiiValue);
        rightSummary.add(new JLabel("TP for PSM:"));
        rightSummary.add(tpSiiValue);
        rightSummary.add(new JLabel("FDR for PSM:"));
        rightSummary.add(fdrSiiValue);
        rightSummary.add(new JLabel("FP for proteins:"));
        rightSummary.add(new JLabel("0"));
        rightSummary.add(new JLabel("TP for proteins:"));
        rightSummary.add(new JLabel("0"));
        rightSummary.add(new JLabel("FDR for proteins:"));
        rightSummary.add(new JLabel("0"));

        JPanel calculateParams = new JPanel(new FlowLayout());
        calculateParams.add(manualDecoy);
        calculateParams.add(new JLabel("Manual Decoy:"));
        calculateParams.add(new JLabel("Prefix"));
        calculateParams.add(manualDecoyPrefixValue);
        calculateParams.add(new JLabel("Ratio"));
        calculateParams.add(manualDecoyRatioValue);
        calculateParams.add(jComboBox1);
        // calculateParams.add(psmRankValue);
        calculateParams.add(jComboBox2);
        calculateParams.add(manualCalculate);

        JPanel bottomTables = new JPanel(new GridLayout(1, 3));
        bottomTables.add(fdrPanel);
        bottomTables.add(tpEvaluePanel);
        bottomTables.add(tpQvaluePanel);

        setLayout(new GridLayout(4, 1));
        setBorder(BorderFactory.createTitledBorder("Summary"));
        add(leftSummary);
        add(rightSummary);
        add(calculateParams);
        add(bottomTables);
    }

    public List<ProteinDetectionHypothesis> getPDHListPassThreshold() {
        return pDHListPassThreshold;
    }

    public JComboBox<String> getSiiComboBox() {
        return siiComboBox;
    }

    @Override
    public boolean isLoaded() {
        return isLoaded;
    }

    @Override
    public void load() {
        List<SpectrumIdentificationList> silList = proteoIDViewer.
                getSingleStorage().getSpectrumIdentificationLists();

        silList.stream().forEach((list) -> {
            getSiiComboBox().addItem(
                    list.getId());
        });

        this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

        List<Peptide> peptideListNonReduntant = new ArrayList<>();
        List<SpectrumIdentificationItem> sIIListIsDecoyTrue = new ArrayList<>();
        List<SpectrumIdentificationItem> sIIListIsDecoyFalse = new ArrayList<>();
        List<SpectrumIdentificationItem> sIIListPassThresholdRankOne
                = new ArrayList<>();
        List<SpectrumIdentificationItem> sIIListBelowThreshold
                = new ArrayList<>();
        sIIListPassThreshold.clear();
        Optional<SpectrumIdentificationList> listOptional = silList.stream().
                filter(p -> p.getId().equalsIgnoreCase(getSiiComboBox().
                        getSelectedItem().toString())).findAny();
        List<SpectrumIdentificationItem> siiListTemp = new ArrayList<>();
        if (listOptional.isPresent()) {
            for (SpectrumIdentificationResult result : listOptional.get()) {
                for (SpectrumIdentificationItem item : result) {
                    siiListTemp.add(item);
                }
            }
        }

        if (!siiListTemp.isEmpty()) {
            siiListTemp.get(0).getCvParam().forEach(param -> {
                if (param.getName() != null
                        && !param.getName().equals("")) {
                    boolean paramInComboBox = false;
                    for (int i = 0; i < jComboBox1.getItemCount(); i++) {
                        String boxItem = jComboBox1.getItemAt(i);
                        if (boxItem.equals(
                                param.getName())) {
                            paramInComboBox = true;
                            break;
                        }
                    }

                    if (!paramInComboBox) {
                        jComboBox1.addItem(param.getName());
                        cvTermMap.put(param.getName(),
                                param.getAccession());
                    }
                }
            });
        }

        for (SpectrumIdentificationItem spectrumIdentificationItem : siiListTemp) {
            if (spectrumIdentificationItem.isPassThreshold()) {
                sIIListPassThreshold.add(spectrumIdentificationItem);
                String p_ref = spectrumIdentificationItem.getPeptideRef();

                // Update FG 18-03-2013 escape XML
                Peptide peptide = proteoIDViewer.getSingleStorage().
                        findPeptideByRef(StringEscapeUtils.escapeXml(p_ref));

                if (!peptideListNonReduntant.contains(peptide)) {
                    peptideListNonReduntant.add(peptide);
                }
            } else {
                sIIListBelowThreshold.add(spectrumIdentificationItem);
            }

            if (spectrumIdentificationItem.isPassThreshold()
                    && spectrumIdentificationItem.getRank() == 1) {
                sIIListPassThresholdRankOne.add(spectrumIdentificationItem);
            }

            boolean isdecoy = true;
            List<PeptideEvidenceRef> peRefLst = spectrumIdentificationItem
                    .getPeptideEvidenceRef();
            for (PeptideEvidenceRef peptideEvidenceRef : peRefLst) {
                PeptideEvidence peptideEvidence = proteoIDViewer.
                        getSingleStorage().findPeptideEvidenceByRef(
                                peptideEvidenceRef);
                if (!peptideEvidence.isDecoy()) {
                    sIIListIsDecoyFalse.add(spectrumIdentificationItem);
                    isdecoy = false;
                    break;
                }
            }
            if (isdecoy) {
                sIIListIsDecoyTrue.add(spectrumIdentificationItem);
            }
        }

        int sirSize = CollectionUtils.sizeOfIterable(CollectionUtils.concat(
                silList));

        totalSIRLabelValue.setText("" + sirSize);

        totalSIIaboveThresholdLabelValue.setText(
                String.valueOf(sIIListPassThreshold.size()));

        totalSIIbelowThresholdLabelValue.setText(
                String.valueOf(sIIListBelowThreshold.size()));

        totalSIIaboveThresholdRankOneLabelValue.setText(
                String.valueOf(sIIListPassThresholdRankOne.size()));

        if (sirSize > 0) {
            double percent = IdViewerUtils
                    .roundTwoDecimals((float) sIIListPassThresholdRankOne
                            .size() * 100 / sirSize);
            percentIdentifiedSpectraLabelValue.setText(
                    String.valueOf(percent) + "%");
        }

        totalPeptidesaboveThresholdLabelValue.setText(
                String.valueOf(peptideListNonReduntant.size()));

        int pagSize = CollectionUtils.sizeOfIterable(CollectionUtils.concat(
                proteoIDViewer.getSingleStorage().getProteinDetectionLists()));
        totalPAGsLabelValue.setText("" + pagSize);
        if (getPDHListPassThreshold() != null) {
            totalPDHsaboveThresholdLabelValue.setText(
                    String.valueOf(getPDHListPassThreshold().size()));
        }

        isDecoySiiValue.setText(
                String.valueOf(sIIListIsDecoyTrue.size()));

        isDecoySiiFalseValue.setText(
                String.valueOf(sIIListIsDecoyFalse.size()));

        falsePositiveSii = IdViewerUtils
                .roundThreeDecimals(sIIListIsDecoyTrue.size());
        fpSiiValue.setText(String.valueOf(falsePositiveSii));

        truePositiveSii = IdViewerUtils
                .roundThreeDecimals(sIIListPassThreshold.size()
                        - sIIListIsDecoyTrue.size());

        tpSiiValue.setText(String.valueOf(truePositiveSii));

        if (falsePositiveSii + truePositiveSii == 0) {
            fdrSiiValue.setText("0.0");
        } else {
            fdrSii = IdViewerUtils
                    .roundThreeDecimals((falsePositiveSii)
                            / (falsePositiveSii + truePositiveSii));
            fdrSiiValue.setText(String.valueOf(fdrSii));
        }

        this.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

        isLoaded = true;
    }

    public void reset() {
        tpSiiValue.setText("0");
        fdrSiiValue.setText("0");

        totalSIRLabelValue.setText("0");
        totalSIIaboveThresholdLabelValue.setText("0");
        totalSIIbelowThresholdLabelValue.setText("0");
        totalSIIaboveThresholdRankOneLabelValue.setText("0");
        percentIdentifiedSpectraLabelValue.setText("0");
        totalPeptidesaboveThresholdLabelValue.setText("0");
        totalPAGsLabelValue.setText("0");
        totalPDHsaboveThresholdLabelValue.setText("0");
        isDecoySiiValue.setText("0");
        isDecoySiiFalseValue.setText("0");

        fpSiiValue.setText("0");

        tpEvaluePanel.removeAll();
        tpEvaluePanel.validate();
        tpEvaluePanel.repaint();

        tpQvaluePanel.removeAll();
        tpQvaluePanel.validate();
        tpQvaluePanel.repaint();
        jComboBox1.removeAllItems();

        siiComboBox.removeAllItems();

        fdrPanel.removeAll();
        fdrPanel.validate();
        fdrPanel.repaint();
    }
}
