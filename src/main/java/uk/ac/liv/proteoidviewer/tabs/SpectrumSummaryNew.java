package uk.ac.liv.proteoidviewer.tabs;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

import uk.ac.liv.proteoidviewer.ProteoIDViewerNew;
import uk.ac.liv.proteoidviewer.interfaces.LazyLoading;
import uk.ac.liv.proteoidviewer.listener.LazyLoadingComponentListener;
import uk.ac.liv.proteoidviewer.listener.PeptideEvidenceTableMouseClickedNew;
import uk.ac.liv.proteoidviewer.listener.SpectrumIdentificationItemTableMouseClickedNew;
import uk.ac.liv.proteoidviewer.listener.SpectrumIdentificationResultTableMouseClickedNew;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.SpectrumIdentificationItem;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.CvParam;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.SpectrumIdentificationResult;
import uk.ac.liv.pgb.psi.utils.ident.standard.utils.CollectionUtils;

public final class SpectrumSummaryNew extends JSplitPane implements LazyLoading {

    private static final String[] FRAGMENTATION_TABLE_HEADERS = {"M/Z",
        "Intensity", "M Error", "Ion Type", "Charge"};

    private static final String[] PEPTIDE_EVIDENCE_TABLE_HEADERS = {"Start",
        "End", "Pre", "Post", "IsDecoy", "Peptide Sequence",
        "dBSequence_ref"};

    private static final long serialVersionUID = 1L;

    private final JTable spectrumIdentificationResultTable = new JTable();
    private final JTable fragmentationTable = new JTable();
    private final JTable peptideEvidenceTable = new JTable();
    private final JTable spectrumIdentificationItemTable = new JTable();
    private final JPanel jExperimentalFilterPanel = new JPanel();
    private final JPanel jGraph = new JPanel();

    private List<SpectrumIdentificationItem> spectrumIdentificationItemListForSpecificResult;
    private final ProteoIDViewerNew proteoIDViewer;

    private boolean isLoaded = false;

    public SpectrumSummaryNew(final ProteoIDViewerNew proteoIDViewer,
            final ProteinDBViewNew proteinDBView) {
        addComponentListener(new LazyLoadingComponentListener());
        this.proteoIDViewer = proteoIDViewer;
        createTables(proteoIDViewer, proteinDBView);
        createSpectrumSummary();
    }

    public void setSpectrumIdentificationItemListForSpecificResult(final List<SpectrumIdentificationItem> itemList) {
        this.spectrumIdentificationItemListForSpecificResult = itemList;
    }

    public List<SpectrumIdentificationItem> getSpectrumIdentificationItemListForSpecificResult() {
        return this.spectrumIdentificationItemListForSpecificResult;
    }

    public void addEvidence(final Object[] objects) {
        ((DefaultTableModel) peptideEvidenceTable.getModel()).addRow(objects);
    }

    private void createSpectrumSummary() {
        final JPanel jFragmentationPanel = new JPanel();
        jFragmentationPanel.setLayout(new BorderLayout());
        jFragmentationPanel.add(new JScrollPane(fragmentationTable));
        jFragmentationPanel.setBorder(BorderFactory
                .createTitledBorder("Fragmentation"));
        jFragmentationPanel.setAutoscrolls(true);
        jFragmentationPanel.setMinimumSize(new Dimension(0, 0));
        jFragmentationPanel.setPreferredSize(new Dimension(383, 447));

        jGraph.setBorder(BorderFactory.createTitledBorder("Graph"));
        jGraph.setLayout(new BoxLayout(jGraph, BoxLayout.LINE_AXIS));

        jExperimentalFilterPanel.setBorder(BorderFactory
                .createTitledBorder("Experimental Filtering"));

        final JPanel jSpectrumIdentificationResultPanel = new JPanel();
        jSpectrumIdentificationResultPanel.getAccessibleContext()
                .setAccessibleDescription("Spectrum Identification Result");
        jSpectrumIdentificationResultPanel.setLayout(new BorderLayout());
        jSpectrumIdentificationResultPanel.add(new JScrollPane(
                spectrumIdentificationResultTable));
        jSpectrumIdentificationResultPanel.setBorder(BorderFactory
                .createTitledBorder("Spectrum List"));
        jSpectrumIdentificationResultPanel
                .setToolTipText("Protein Ambiguity Group");
        jSpectrumIdentificationResultPanel.setMinimumSize(new Dimension(404,
                569));

        final JPanel jSpectrumIdentificationItemPanel = new JPanel();
        jSpectrumIdentificationItemPanel.setLayout(new BorderLayout());
        jSpectrumIdentificationItemPanel.add(new JScrollPane(
                spectrumIdentificationItemTable));
        jSpectrumIdentificationItemPanel.setBorder(BorderFactory
                .createTitledBorder("Peptide-Spectrum matches"));
        jSpectrumIdentificationItemPanel
                .setToolTipText("Spectrum Identification Item");
        jSpectrumIdentificationItemPanel.setAutoscrolls(true);

        final JPanel jPeptideEvidencePanel = new JPanel();
        jPeptideEvidencePanel.setLayout(new BorderLayout());
        jPeptideEvidencePanel.add(new JScrollPane(peptideEvidenceTable));
        jPeptideEvidencePanel.setBorder(BorderFactory
                .createTitledBorder("Peptide Evidence"));
        jPeptideEvidencePanel.setToolTipText("Peptide Evidence");
        jPeptideEvidencePanel.setAutoscrolls(true);

        final JPanel leftPanel = new JPanel(new GridLayout(3, 1));
        leftPanel.add(jSpectrumIdentificationResultPanel);
        leftPanel.add(jSpectrumIdentificationItemPanel);
        leftPanel.add(jPeptideEvidencePanel);

        final JPanel spectrumPanel = new JPanel(new GridLayout(3, 1));
        spectrumPanel.setBorder(BorderFactory.createTitledBorder("Spectrum"));
        spectrumPanel.setAutoscrolls(true);
        spectrumPanel.add(jGraph);
        spectrumPanel.add(jExperimentalFilterPanel);
        spectrumPanel.add(jFragmentationPanel);

        setBorder(null);
        setDividerLocation(500);
        setRightComponent(spectrumPanel);
        setLeftComponent(leftPanel);
    }

    private void createTables(final ProteoIDViewerNew proteoIDViewer,
            final ProteinDBViewNew proteinDBView) {
        spectrumIdentificationResultTable.setAutoCreateRowSorter(true);
        fragmentationTable.setAutoCreateRowSorter(true);
        peptideEvidenceTable.setAutoCreateRowSorter(true);
        spectrumIdentificationItemTable.setAutoCreateRowSorter(true);

        spectrumIdentificationResultTable
                .setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        fragmentationTable
                .setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        peptideEvidenceTable
                .setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        spectrumIdentificationItemTable
                .setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        fragmentationTable.getTableHeader().setReorderingAllowed(false);
        peptideEvidenceTable.getTableHeader().setReorderingAllowed(false);
        spectrumIdentificationResultTable.getTableHeader()
                .setReorderingAllowed(false);
        spectrumIdentificationItemTable.getTableHeader().setReorderingAllowed(
                false);

        spectrumIdentificationResultTable
                .addMouseListener(
                        new SpectrumIdentificationResultTableMouseClickedNew(
                                proteoIDViewer, SpectrumSummaryNew.this));
        peptideEvidenceTable
                .addMouseListener(new PeptideEvidenceTableMouseClickedNew(
                        proteoIDViewer, proteinDBView));
        spectrumIdentificationItemTable
                .addMouseListener(
                        new SpectrumIdentificationItemTableMouseClickedNew(
                                proteoIDViewer, SpectrumSummaryNew.this));

        spectrumIdentificationResultTable
                .setToolTipText(
                        "this corresponds to Spectrum Identification Result in mzIdentML");

        spectrumIdentificationResultTable.setCursor(Cursor
                .getPredefinedCursor(Cursor.HAND_CURSOR));
        spectrumIdentificationItemTable.setCursor(Cursor
                .getPredefinedCursor(Cursor.HAND_CURSOR));
        peptideEvidenceTable.setCursor(Cursor
                .getPredefinedCursor(Cursor.HAND_CURSOR));
    }

    public JTable getFragmentationTable() {
        return fragmentationTable;
    }

    public JPanel getGraph() {
        return jGraph;
    }

    public JTable getIdentificationItemTable() {
        return spectrumIdentificationItemTable;
    }

    public JTable getIdentificationResultTable() {
        return spectrumIdentificationResultTable;
    }

    @Override
    public boolean isLoaded() {
        return isLoaded;
    }

    @Override
    public void load() {
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        Iterable<SpectrumIdentificationResult> sirIterable = CollectionUtils.
                concat(proteoIDViewer.getSingleStorage().
                        getSpectrumIdentificationLists());

        DefaultTableModel spectrumIdentificationResultTableModel
                = (DefaultTableModel) getIdentificationResultTable()
                .getModel();
        spectrumIdentificationResultTableModel.setRowCount(0);
        getIdentificationResultTable().getTableHeader().setEnabled(false);
        for (SpectrumIdentificationResult result : sirIterable) {

            spectrumIdentificationResultTableModel.addRow(new String[]{
                result.getId(),
                result.getSpectrumID()});

            List<CvParam> cvParamListspectrumIdentificationResult = result
                    .getCvParam();

            for (CvParam cvParam : cvParamListspectrumIdentificationResult) {
                String name = cvParam.getName();
                for (int j = 0; j < spectrumIdentificationResultTableModel
                        .getColumnCount(); j++) {
                    if (!spectrumIdentificationResultTableModel
                            .getColumnName(j).equals(name)) {
                        continue;
                    }
                    spectrumIdentificationResultTableModel.setValueAt(cvParam
                            .getValue(), spectrumIdentificationResultTableModel
                            .getRowCount() - 1, j);

                }
            }
        }
        getIdentificationResultTable().getTableHeader().setEnabled(true);

        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    }

    public void removeAllEvidence() {
        ((DefaultTableModel) peptideEvidenceTable.getModel()).setNumRows(0);
    }

    public void removeAllFragmentation() {
        ((DefaultTableModel) fragmentationTable.getModel()).setNumRows(0);

    }

    public void removeAllIdentificationItems() {
        ((DefaultTableModel) spectrumIdentificationItemTable.getModel())
                .setNumRows(0);
    }

    public void reset(final List<String> sirParamNameList,
            final int spectrumIdentificationResultCvParamLengs,
            final String[] spectrumIdentificationItemTableHeaders) {
        String[] spectrumIdentificationResultTableHeaders
                = new String[spectrumIdentificationResultCvParamLengs + 2];
        spectrumIdentificationResultTableHeaders[0] = "ID";
        spectrumIdentificationResultTableHeaders[1] = "Spectrum ID";

        if (sirParamNameList != null) {
            for (int i = 0; i < sirParamNameList.size(); i++) {
                String string = sirParamNameList.get(i);
                spectrumIdentificationResultTableHeaders[2 + i] = string;

            }
        }

        spectrumIdentificationResultTable.setModel(new DefaultTableModel(
                new Object[][]{}, spectrumIdentificationResultTableHeaders) {
            private static final long serialVersionUID = 1L;

            @Override
            public boolean isCellEditable(final int row, final int col) {
                return false;
            }
        });
        // spectrumIdentificationResultTable.setAutoCreateRowSorter(true);

        spectrumIdentificationResultTable.removeAll();

        fragmentationTable.setModel(new DefaultTableModel(new Object[][]{},
                FRAGMENTATION_TABLE_HEADERS) {
            private static final long serialVersionUID = 1L;

            @Override
            public boolean isCellEditable(final int row, final int col) {
                return false;
            }
        });

        fragmentationTable.removeAll();
        // fragmentationTable.setAutoCreateRowSorter(true);

        peptideEvidenceTable.setModel(new DefaultTableModel(new Object[][]{},
                PEPTIDE_EVIDENCE_TABLE_HEADERS) {
            private static final long serialVersionUID = 1L;

            @Override
            public boolean isCellEditable(final int row, final int col) {
                return false;
            }
        });
        peptideEvidenceTable.removeAll();

        spectrumIdentificationItemTable.setModel(new DefaultTableModel(
                new Object[][]{}, spectrumIdentificationItemTableHeaders) {
            private static final long serialVersionUID = 1L;

            @Override
            public boolean isCellEditable(final int row, final int col) {
                return false;
            }
        });
        spectrumIdentificationItemTable.removeAll();
        // spectrumIdentificationItemTable.setAutoCreateRowSorter(true);
        // peptideEvidenceTable.setAutoCreateRowSorter(true);

        jExperimentalFilterPanel.removeAll();

        jGraph.removeAll();

        jGraph.validate();
        jGraph.repaint();
    }
}
