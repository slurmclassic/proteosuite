package org.proteosuite.jopenms;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.proteosuite.executor.Executor;
import org.proteosuite.jopenms.command.JOpenMS;
import org.proteosuite.jopenms.config.jaxb.AbstractITEM;
import org.proteosuite.jopenms.config.jaxb.ITEM;
import org.proteosuite.jopenms.config.jaxb.ITEMLIST;
import org.proteosuite.jopenms.config.jaxb.NODE;
import org.proteosuite.jopenms.config.jaxb.PARAMETERS;

/**
 *
 * @author Da Qi
 */
public final class OpenMSModule {

    private Map<String, Object> cfgMap;
    private File cfgFile;
    private static Unmarshaller UNMARSHALLER = null;
    private static Marshaller MARSHALLER = null;
    public static final String SEPARATOR = "$";

    static {
        try {
            JAXBContext context = JAXBContext
                    .newInstance(new Class[]{PARAMETERS.class});
            UNMARSHALLER = context.createUnmarshaller();

            MARSHALLER = context.createMarshaller();
            MARSHALLER.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        } catch (JAXBException ex) {
            Logger.getLogger(OpenMSModule.class.getName()).log(Level.SEVERE,
                    null, ex);

        }
    }

    public OpenMSModule(final File executable) {
        try {
            String executableTrimmed = executable.getName().replaceFirst(
                    "\\.[Ee][Xx][Ee]", "");
            // create a default config file using "-write_ini <file>" argument
            cfgFile = File.createTempFile(executableTrimmed, ".ini");

            Executor exe = new Executor(executable);
            String[] args = new String[2];
            args[0] = "-write_ini";
            args[1] = cfgFile.getAbsolutePath();
            exe.callExe(args);

            // build a config map and options
            cfgMap = initConfigMap(cfgFile, executableTrimmed);
        } catch (IOException ex) {
            Logger.getLogger(OpenMSModule.class.getName()).log(Level.SEVERE,
                    null, ex);
            System.out.println(ex.getLocalizedMessage());
        } finally {
            cfgFile.deleteOnExit();
        }
    }

    public Map<String, Object> getCfgMap() {
        return cfgMap;
    }

    public static Unmarshaller getUnmarshaller() {
        return UNMARSHALLER;
    }

    public static Marshaller getMarshaller() {
        return MARSHALLER;
    }

    /**
     * Build a map structure according to the INI file. The INI file comply to
     * Param_1_6_2.xsd.
     *
     * @param ini the INI file.
     * @param executableTrimmed The executable that the config file is intended for.
     *
     * @return a map version of INI file.
     */
    private static Map<String, Object> initConfigMap(final File ini,
            final String executableTrimmed) {

        Map<String, Object> configMap = new HashMap<>();

        configMap.put("iniFile", ini); // store the input file name with key
        // "inputFileName"

        // URL fileURL = JOpenMS.class.getClassLoader().getResource(ini);
        try {

            // initial map
            // root node
            PARAMETERS parameters = (PARAMETERS) UNMARSHALLER.unmarshal(ini);

            List<AbstractITEM> items = parameters.getITEMS();
            if (items != null) {
                for (Object i : items) {
                    if (i instanceof ITEM) {
                        ITEM item = (ITEM) i;

                        if (configMap.get(item.getName()) == null) {
                            configMap.put(item.getName(), item.getValue());
                        }
                    } else if (i instanceof ITEMLIST) {
                        ITEMLIST itlst = (ITEMLIST) i;
                        String lstName = itlst.getName();
                        String lstValue = getItemListValue(itlst);
                        if (configMap.get(lstName) == null) {
                            configMap.put(lstName, lstValue);
                        }
                    }
                }
            }

            List<NODE> nodes = parameters.getNODE();
            if (nodes != null) {
                for (NODE node : nodes) {
                    putInConfigMap(configMap, node.getName(),
                            node.getITEMOrITEMLISTOrNODE(), executableTrimmed);
                }
            }
        } catch (JAXBException ex) {
            Logger.getLogger(JOpenMS.class.getName()).log(Level.SEVERE, null,
                    ex);
        }
        return configMap;
    }

    private static boolean putInConfigMap(final Map<String, Object> m, final String nodeName,
            final List<Object> objs, final String executableTrimmed) {
        boolean ret = true;

        if (objs != null) {
            for (Object obj : objs) {
                if (obj instanceof ITEM) {
                    ITEM item = (ITEM) obj;
                    Map<String, Object> subMap;
                    Object value = m.get(nodeName);

                    if (value == null) {
                        subMap = new HashMap<>();
                        m.put(nodeName, subMap);
                        subMap.put(item.getName(), item.getValue());
                        // subMap.put(item.getName(), item);
                    } else if (value instanceof Map) {
                        subMap = (Map<String, Object>) value;
                        subMap.put(item.getName(), item.getValue());
                        // subMap.put(item.getName(), item);
                    } else if (!(value instanceof String)) {
                        System.out.println("Unexpected type: "
                                + value.getClass().getName() + "!\n");
                        ret = false;
                    }
                } else if (obj instanceof ITEMLIST) {
                    ITEMLIST itlst = (ITEMLIST) obj;
                    Map<String, Object> subMap;
                    Object value = m.get(nodeName);

                    if (value == null) {
                        subMap = new HashMap<>();
                        m.put(nodeName, subMap);
                        subMap.put(itlst.getName(), getItemListValue(itlst));
                        // subMap.put(item.getName(), item);
                    } else if (value instanceof Map) {
                        subMap = (Map<String, Object>) value;
                        subMap.put(itlst.getName(), getItemListValue(itlst));
                        // subMap.put(item.getName(), item);
                    }
                } else if (obj instanceof NODE) {
                    NODE node = (NODE) obj;
                    Map<String, Object> subMap;
                    Object value = m.get(nodeName);

                    if (value == null) {
                        subMap = new HashMap<>();
                        m.put(nodeName, subMap);

                        // Param_1_6_2.xsd
                        ret = putInConfigMap(subMap, node.getName(),
                                node.getITEMOrITEMLISTOrNODE(),
                                executableTrimmed);
                    } else if (value instanceof Map) {
                        subMap = (Map<String, Object>) value;
                        // Param_1_6_2.xsd
                        ret = putInConfigMap(subMap, node.getName(),
                                node.getITEMOrITEMLISTOrNODE(),
                                executableTrimmed);
                    }
                }
            }
        }

        return ret;
    }

    private static String getItemListValue(final ITEMLIST itlst) {
        StringBuilder lstValue = new StringBuilder("::");
        itlst.getLISTITEM().stream().forEach((lstitem) -> {
            lstValue.append(lstitem.getValue());
        });

        return lstValue.toString();
    }
}
