package org.proteosuite.jopenms.config.jaxb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for AbstractITEMType complex type.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 *
 * <pre>
 * &lt;complexType name="AbstractITEMType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;attribute name="name" use="required"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;minLength value="1"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="type" use="required" type="{}PossibleITEMType" /&gt;
 *       &lt;attribute name="description" type="{http://www.w3.org/2001/XMLSchema}string" default="" /&gt;
 *       &lt;attribute name="tags" type="{http://www.w3.org/2001/XMLSchema}string" default="false" /&gt;
 *       &lt;attribute name="restrictions" type="{http://www.w3.org/2001/XMLSchema}string" default="" /&gt;
 *       &lt;attribute name="supported_formats" type="{http://www.w3.org/2001/XMLSchema}string" default="false" /&gt;
 *       &lt;attribute name="required" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *       &lt;attribute name="advanced" type="{http://www.w3.org/2001/XMLSchema}boolean" default="false" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AbstractITEMType")
@XmlSeeAlso({
    ITEMLIST.class,
    ITEM.class
})
public abstract class AbstractITEM {

    @XmlAttribute(name = "name", required = true)
    protected String name;
    @XmlAttribute(name = "type", required = true)
    protected PossibleITEMType type;
    @XmlAttribute(name = "description")
    protected String description;
    @XmlAttribute(name = "tags")
    protected String tags;
    @XmlAttribute(name = "restrictions")
    protected String restrictions;
    @XmlAttribute(name = "supported_formats")
    protected String supportedFormats;
    @XmlAttribute(name = "required")
    protected Boolean required;
    @XmlAttribute(name = "advanced")
    protected Boolean advanced;

    /**
     * Gets the value of the name property.
     *
     * @return possible object is {@link String }
     *
     */
    public final String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public final void setName(final String value) {
        this.name = value;
    }

    /**
     * Gets the value of the type property.
     *
     * @return possible object is {@link PossibleITEMType }
     *
     */
    public final PossibleITEMType getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     *
     * @param value allowed object is {@link PossibleITEMType }
     *
     */
    public final void setType(final PossibleITEMType value) {
        this.type = value;
    }

    /**
     * Gets the value of the description property.
     *
     * @return possible object is {@link String }
     *
     */
    public final String getDescription() {
        if (description == null) {
            return "";
        } else {
            return description;
        }
    }

    /**
     * Sets the value of the description property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public final void setDescription(final String value) {
        this.description = value;
    }

    /**
     * Gets the value of the tags property.
     *
     * @return possible object is {@link String }
     *
     */
    public final String getTags() {
        if (tags == null) {
            return "false";
        } else {
            return tags;
        }
    }

    /**
     * Sets the value of the tags property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public final void setTags(final String value) {
        this.tags = value;
    }

    /**
     * Gets the value of the restrictions property.
     *
     * @return possible object is {@link String }
     *
     */
    public final String getRestrictions() {
        if (restrictions == null) {
            return "";
        } else {
            return restrictions;
        }
    }

    /**
     * Sets the value of the restrictions property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public final void setRestrictions(final String value) {
        this.restrictions = value;
    }

    /**
     * Gets the value of the supportedFormats property.
     *
     * @return possible object is {@link String }
     *
     */
    public final String getSupportedFormats() {
        if (supportedFormats == null) {
            return "false";
        } else {
            return supportedFormats;
        }
    }

    /**
     * Sets the value of the supportedFormats property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public final void setSupportedFormats(final String value) {
        this.supportedFormats = value;
    }

    /**
     * Gets the value of the required property.
     *
     * @return possible object is {@link Boolean }
     *
     */
    public final boolean isRequired() {
        if (required == null) {
            return false;
        } else {
            return required;
        }
    }

    /**
     * Sets the value of the required property.
     *
     * @param value allowed object is {@link Boolean }
     *
     */
    public final void setRequired(final Boolean value) {
        this.required = value;
    }

    /**
     * Gets the value of the advanced property.
     *
     * @return possible object is {@link Boolean }
     *
     */
    public final boolean isAdvanced() {
        if (advanced == null) {
            return false;
        } else {
            return advanced;
        }
    }

    /**
     * Sets the value of the advanced property.
     *
     * @param value allowed object is {@link Boolean }
     *
     */
    public final void setAdvanced(final Boolean value) {
        this.advanced = value;
    }

}
