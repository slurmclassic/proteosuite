package org.proteosuite.jopenms.config.jaxb;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlType;

/**
 *
 *
 * <p>
 * Java class for NODEType complex type.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 *
 * <pre>
 * &lt;complexType name="NODEType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;choice maxOccurs="unbounded" minOccurs="0"&gt;
 *         &lt;group ref="{}ITEMS"/&gt;
 *         &lt;element name="NODE" type="{}NODEType"/&gt;
 *       &lt;/choice&gt;
 *       &lt;attribute name="name" use="required"&gt;
 *         &lt;simpleType&gt;
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *             &lt;minLength value="1"/&gt;
 *           &lt;/restriction&gt;
 *         &lt;/simpleType&gt;
 *       &lt;/attribute&gt;
 *       &lt;attribute name="description" type="{http://www.w3.org/2001/XMLSchema}string" default="" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NODEType", propOrder = {
    "itemOrITEMLISTOrNODE"
})
public final class NODE {

    @XmlElements({
        @XmlElement(name = "ITEM", type = ITEM.class),
        @XmlElement(name = "ITEMLIST", type = ITEMLIST.class),
        @XmlElement(name = "NODE", type = NODE.class)
    })
    protected List<Object> itemOrITEMLISTOrNODE;
    @XmlAttribute(name = "name", required = true)
    protected String name;
    @XmlAttribute(name = "description")
    protected String description;

    /**
     * Gets the value of the itemOrITEMLISTOrNODE property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a
     * snapshot. Therefore any modification you make to the returned list will
     * be present inside the JAXB object. This is why there is not a
     * <CODE>set</CODE> method for the itemOrITEMLISTOrNODE property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getITEMOrITEMLISTOrNODE().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list null null null
     * null null null null     {@link ITEM }
     * {@link ITEMLIST }
     * {@link NODE }
     *
     *
     * @return A list of ITEM or ITEMLIST.
     */
    public List<Object> getITEMOrITEMLISTOrNODE() {
        if (itemOrITEMLISTOrNODE == null) {
            itemOrITEMLISTOrNODE = new ArrayList<Object>();
        }
        return this.itemOrITEMLISTOrNODE;
    }

    /**
     * Gets the value of the name property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setName(final String value) {
        this.name = value;
    }

    /**
     * Gets the value of the description property.
     *
     * @return possible object is {@link String }
     *
     */
    public String getDescription() {
        if (description == null) {
            return "";
        } else {
            return description;
        }
    }

    /**
     * Sets the value of the description property.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setDescription(final String value) {
        this.description = value;
    }

}
