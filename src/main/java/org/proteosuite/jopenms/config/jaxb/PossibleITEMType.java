package org.proteosuite.jopenms.config.jaxb;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for PossibleITEMType.
 *
 * <p>
 * The following schema fragment specifies the expected content contained within
 * this class.
 *
 * <pre>
 * &lt;simpleType name="PossibleITEMType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="int"/&gt;
 *     &lt;enumeration value="double"/&gt;
 *     &lt;enumeration value="float"/&gt;
 *     &lt;enumeration value="string"/&gt;
 *     &lt;enumeration value="int-pair"/&gt;
 *     &lt;enumeration value="double-pair"/&gt;
 *     &lt;enumeration value="input-prefix"/&gt;
 *     &lt;enumeration value="output-prefix"/&gt;
 *     &lt;enumeration value="input-file"/&gt;
 *     &lt;enumeration value="output-file"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 *
 */
@XmlType(name = "PossibleITEMType")
@XmlEnum
public enum PossibleITEMType {

    @XmlEnumValue("int")
    INT("int"),
    @XmlEnumValue("double")
    DOUBLE("double"),
    @XmlEnumValue("float")
    FLOAT("float"),
    @XmlEnumValue("string")
    STRING("string"),
    @XmlEnumValue("int-pair")
    INT_PAIR("int-pair"),
    @XmlEnumValue("double-pair")
    DOUBLE_PAIR("double-pair"),
    @XmlEnumValue("input-prefix")
    INPUT_PREFIX("input-prefix"),
    @XmlEnumValue("output-prefix")
    OUTPUT_PREFIX("output-prefix"),
    @XmlEnumValue("input-file")
    INPUT_FILE("input-file"),
    @XmlEnumValue("output-file")
    OUTPUT_FILE("output-file");
    private final String value;

    PossibleITEMType(final String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static PossibleITEMType fromValue(final String v) {
        for (PossibleITEMType c : PossibleITEMType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
