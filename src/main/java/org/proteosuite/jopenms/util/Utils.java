package org.proteosuite.jopenms.util;

/**
 * General utilities class
 *
 * @author Da Qi
 *
 */
public final class Utils {

    /**
     * Private constructor to prevent instantiation.
     */
    private Utils() { }

    public static String nameDecode(final String original) {
        return original.replace("_", "-");
    }
}
