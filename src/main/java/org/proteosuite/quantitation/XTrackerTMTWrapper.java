package org.proteosuite.quantitation;

import java.util.HashMap;
import java.util.List;
import org.proteosuite.model.AnalyseData;
import org.proteosuite.model.RawDataFile;

/**
 *
 * @author SPerkins
 */
public final class XTrackerTMTWrapper extends XTrackerAbstractWrapper {
    private final String plex;

    public XTrackerTMTWrapper(final List<RawDataFile> rawData) {
        super(rawData, "TMT");
        plex = AnalyseData.getInstance().getMultiplexing();
        searchableMods = new HashMap<>();

        // These two terms are obviously wrong, but they will do for now until this is looked at again.
        searchableMods.put("iTRAQ4plex (K)", 57.021469);
        searchableMods.put("iTRAQ4plex (N-term)", 144.102063);
    }

    @Override
    protected String getPlexAsString() {
        return plex.contains("2-plex")
                ? "2-plex" : plex.contains("6-plex") ? "6-plex" : plex.contains(
                "8-plex") ? "8-plex" : "10-plex";
    }

    @Override
    protected IsobaricReagent getIsobaricReagent(final String channel) {
        TMTReagent reagent = TMTReagent.getReagent(plex.contains(
                        "2-plex") ? TMTReagent.TWO_PLEX : plex.
                                contains("6-plex") ? TMTReagent.SIX_PLEX : plex.
                                contains("8-plex") ? TMTReagent.EIGHT_PLEX
                                : TMTReagent.TEN_PLEX, channel);
        return reagent;
    }
}
