package org.proteosuite.quantitation;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.xml.bind.JAXBException;
import org.proteosuite.ProteoSuiteException;
import org.proteosuite.actions.ProteoSuiteAction;
import org.proteosuite.config.Config;
import org.proteosuite.config.GlobalConfig;
import org.proteosuite.gui.analyse.AnalyseDynamicTab;
import org.proteosuite.jopenms.command.JOpenMS;
import org.proteosuite.model.BackgroundTask;
import org.proteosuite.model.BackgroundTaskManager;
import org.proteosuite.model.ProteoSuiteActionSubject;
import org.proteosuite.model.MzQuantMLFile;
import org.proteosuite.model.ProteoSuiteActionResult;
import org.proteosuite.model.QuantDataFile;
import org.proteosuite.model.RawDataFile;
import org.proteosuite.utils.MappingHelper;
import org.proteosuite.utils.SystemUtils;
import org.proteosuite.utils.SystemUtils.SystemType;
import uk.ac.liv.mzqlib.openms.converter.ConsensusXMLProcessor;
import uk.ac.liv.mzqlib.openms.converter.ConsensusXMLProcessorFactory;

/**
 *
 * @author SPerkins
 */
public final class OpenMSLabelFreeWrapper {

    private static final GlobalConfig CONFIG = GlobalConfig.getInstance();
    private static boolean IS_WINDOWS = false;
    private static final String INSTALLED_LOCATION_HINTS_AS_STRING = "c:\\Program Files\\OpenMS-1.11\\bin;c:\\Program Files (x86)\\OpenMS-1.11\\bin;c:\\Program Files;"
            + "c:\\Program Files (x86);c:";
    private static final List<File> INSTALLED_LOCATION_HINTS = Arrays.stream(INSTALLED_LOCATION_HINTS_AS_STRING.split(";")).map(p -> new File(p)).collect(Collectors.toList());

    private final List<RawDataFile> rawDataFiles;
    private final CountDownLatch featureFinderCentroidedLatch;
    private final CountDownLatch mapAlignerPoseClusteringLatch;
    private final CountDownLatch featureLinkerUnlabeledQTLatch;

    public static boolean checkIsInstalled() {
        IS_WINDOWS = SystemUtils.getSystemType().equals(SystemType.WIN32)
                || SystemUtils.getSystemType().equals(SystemType.WIN64);
        File executable = null;
        if (IS_WINDOWS) {
            executable = SystemUtils.findExecutionCommand("FeatureFinderCentroided.exe", INSTALLED_LOCATION_HINTS);
        } else {
            executable = SystemUtils.findExecutionCommand("FeatureFinderCentroided", INSTALLED_LOCATION_HINTS);
        }

        return executable != null;
    }

    public OpenMSLabelFreeWrapper(final List<RawDataFile> rawDataFiles) {
        this.rawDataFiles = rawDataFiles;
        featureFinderCentroidedLatch = new CountDownLatch(rawDataFiles.size());
        mapAlignerPoseClusteringLatch = new CountDownLatch(1);
        featureLinkerUnlabeledQTLatch = new CountDownLatch(1);
    }

    public void compute() {
        AnalyseDynamicTab.getInstance().getAnalyseStatusPanel()
                .setQuantitationProcessing();

        GlobalConfig config = Config.getInstance().getGlobalConfig();

        List<String> featureFinderCentroidedFiles = new ArrayList<>();
        List<String> mapAlignerPoseClusteringFiles = new ArrayList<>();
        int featureFinderExecutionDelay = 0;
        for (RawDataFile dataFile : rawDataFiles) {
            String featureFinderCentroidedFile = dataFile.getAbsoluteFileName()
                    .replaceAll("\\." + dataFile.getFormat() + '$',
                            "_FFC.featureXML");
            String mapAlignerPostClusteringFile = featureFinderCentroidedFile
                    .replaceAll("\\." + "featureXML" + '$', "_MAPC.featureXML");
            doFeatureFinderCentroided(dataFile, featureFinderCentroidedFile,
                    featureFinderCentroidedLatch, featureFinderExecutionDelay);
            featureFinderCentroidedFiles.add(featureFinderCentroidedFile);
            mapAlignerPoseClusteringFiles.add(mapAlignerPostClusteringFile);
            featureFinderExecutionDelay += 5;
        }

        String unlabeledOutputFile = rawDataFiles.get(0).getFile().getParent()
                + "\\unlabeled_result_FLUQT.consensusXML";
        String mzQuantMLFile = unlabeledOutputFile.replace(".consensusXML",
                ".mzq");

        if (config.isRequestingSkipInternalAlignment()) {
            doFeatureLinkerUnlabeledQT(featureFinderCentroidedFiles,
                    unlabeledOutputFile, featureFinderCentroidedLatch,
                    featureLinkerUnlabeledQTLatch);
        } else {
            doMapAlignerPoseClustering(featureFinderCentroidedFiles,
                    mapAlignerPoseClusteringFiles, featureFinderCentroidedLatch,
                    mapAlignerPoseClusteringLatch);

            doFeatureLinkerUnlabeledQT(mapAlignerPoseClusteringFiles,
                    unlabeledOutputFile, mapAlignerPoseClusteringLatch,
                    featureLinkerUnlabeledQTLatch);
        }

        doConsensusXMLToMzQuantMLConversion(unlabeledOutputFile, mzQuantMLFile,
                featureLinkerUnlabeledQTLatch);
    }

    private void doFeatureFinderCentroided(final RawDataFile inputDataFile,
            final String outputFile,
            final CountDownLatch featureFinderCentroidedLatch,
            final int executionDelay) {

        BackgroundTask<ProteoSuiteActionSubject> task = new BackgroundTask<>(
                inputDataFile, "Finding Features");

        task.addAsynchronousProcessingAction(new ProteoSuiteAction<ProteoSuiteActionResult, ProteoSuiteActionSubject>() {
            @Override
            public ProteoSuiteActionResult act(final ProteoSuiteActionSubject argument) {
                try {
                    Thread.sleep(executionDelay * 1000L);
                    if (CONFIG.isRequestingDebugSkipFeatureFinder()) {
                        return ProteoSuiteActionResult.emptyResult();
                    }

                    if (CONFIG.isRequestingCheckForPreexistingFeatureXML()) {
                        File preexistingFile = new File(outputFile);
                        if (preexistingFile.exists()) {
                            System.out.println(
                                    "FeatureXML file already exists, reusing : "
                                    + preexistingFile.getName());
                            return ProteoSuiteActionResult.emptyResult();
                        }
                    }

                    String command = "FeatureFinderCentroided" + (IS_WINDOWS
                            ? ".exe" : "");
                    File executable = SystemUtils.findExecutionCommand(command,
                            INSTALLED_LOCATION_HINTS);
                    JOpenMS.performOpenMSTask(executable,
                            Arrays.asList(inputDataFile.getAbsoluteFileName()),
                            Arrays.asList(outputFile));
                } catch (InterruptedException ex) {
                    ProteoSuiteException pex = new ProteoSuiteException(
                            "Thread sleep error in openMS thread.", ex, true);
                    Logger.getLogger(OpenMSLabelFreeWrapper.class.getName()).
                            log(Level.SEVERE, null, ex);
                    return new ProteoSuiteActionResult(pex);
                } catch (IOException ex) {
                    ProteoSuiteException pex = new ProteoSuiteException(
                            "Error executing FeatureFinderCentroided.", ex, true);
                    Logger.getLogger(OpenMSLabelFreeWrapper.class.getName()).
                            log(Level.SEVERE, null, ex);
                    return new ProteoSuiteActionResult(pex);
                }

                return ProteoSuiteActionResult.emptyResult();
            }
        });

        task.addCompletionAction(
                new ProteoSuiteAction<ProteoSuiteActionResult, ProteoSuiteActionSubject>() {
            @Override
            public ProteoSuiteActionResult act(final ProteoSuiteActionSubject argument) {
                featureFinderCentroidedLatch.countDown();
                return ProteoSuiteActionResult.emptyResult();
            }
        });

        BackgroundTaskManager.getInstance().submit(task);
    }

    private void doMapAlignerPoseClustering(final List<String> inputFiles,
            final List<String> outputFiles,
            final CountDownLatch featureFinderCentroidedLatch,
            final CountDownLatch mapAlignerPoseClusteringLatch) {

        ProteoSuiteActionSubject subject = new ProteoSuiteActionSubject() {
            private final File file = new File(inputFiles.get(0));

            @Override
            public String getSubjectName() {
                return file.getName();
            }
        };

        BackgroundTask<ProteoSuiteActionSubject> task = new BackgroundTask<>(
                subject, "Aligning Features");

        task.addProcessingCondition(featureFinderCentroidedLatch);

        task.addAsynchronousProcessingAction(new ProteoSuiteAction<ProteoSuiteActionResult, ProteoSuiteActionSubject>() {
            @Override
            public ProteoSuiteActionResult act(final ProteoSuiteActionSubject argument) {
                String command = "MapAlignerPoseClustering" + (IS_WINDOWS
                        ? ".exe" : "");
                File executable = SystemUtils.findExecutionCommand(command,
                        INSTALLED_LOCATION_HINTS);
                try {
                    if (CONFIG.isRequestingDebugSkipFeatureAligner()) {
                        return ProteoSuiteActionResult.emptyResult();
                    }

                    JOpenMS.performOpenMSTask(executable, inputFiles,
                            outputFiles);
                } catch (IOException ex) {
                    ProteoSuiteException pex = new ProteoSuiteException(
                            "Error executing MapAlignerPoseClustering.", ex, true);
                    Logger.getLogger(OpenMSLabelFreeWrapper.class.getName()).
                            log(Level.SEVERE, null, ex);
                    return new ProteoSuiteActionResult(pex);
                }
                return ProteoSuiteActionResult.emptyResult();
            }
        });

        task.addCompletionAction(
                new ProteoSuiteAction<ProteoSuiteActionResult, ProteoSuiteActionSubject>() {
            @Override
            public ProteoSuiteActionResult act(final ProteoSuiteActionSubject argument) {
                mapAlignerPoseClusteringLatch.countDown();
                return ProteoSuiteActionResult.emptyResult();
            }
        });

        BackgroundTaskManager.getInstance().submit(task);
    }

    private void doFeatureLinkerUnlabeledQT(final List<String> inputFiles,
            final String outputFile,
            final CountDownLatch mapAlignerPoseClusteringLatch,
            final CountDownLatch featureLinkerUnlabeledQTLatch) {

        ProteoSuiteActionSubject subject = new ProteoSuiteActionSubject() {
            private final File file = new File(inputFiles.get(0));

            @Override
            public String getSubjectName() {
                return file.getName();
            }
        };

        BackgroundTask<ProteoSuiteActionSubject> task = new BackgroundTask<>(
                subject, "Linking Features");

        task.addProcessingCondition(mapAlignerPoseClusteringLatch);

        task.addAsynchronousProcessingAction(new ProteoSuiteAction<ProteoSuiteActionResult, ProteoSuiteActionSubject>() {
            @Override
            public ProteoSuiteActionResult act(final ProteoSuiteActionSubject argument) {
                String command = "FeatureLinkerUnlabeledQT" + (IS_WINDOWS
                        ? ".exe" : "");
                File executable = SystemUtils.findExecutionCommand(command,
                        INSTALLED_LOCATION_HINTS);
                try {
                    if (CONFIG.isRequestingDebugSkipFeatureLinker()) {
                        return ProteoSuiteActionResult.emptyResult();
                    }

                    JOpenMS.performOpenMSTask(executable, inputFiles,
                            Arrays.asList(outputFile));
                } catch (IOException ex) {
                    ProteoSuiteException pex = new ProteoSuiteException(
                            "Error executing FeatureLinkerUnlabeledQT.", ex, true);
                    Logger.getLogger(OpenMSLabelFreeWrapper.class.getName()).
                            log(Level.SEVERE, null, ex);
                    return new ProteoSuiteActionResult(pex);
                }

                return ProteoSuiteActionResult.emptyResult();
            }
        });

        task.addCompletionAction(
                new ProteoSuiteAction<ProteoSuiteActionResult, ProteoSuiteActionSubject>() {

            @Override
            public ProteoSuiteActionResult act(final ProteoSuiteActionSubject argument) {
                AnalyseDynamicTab.getInstance().getAnalyseStatusPanel()
                        .setQuantitationDone();

                featureLinkerUnlabeledQTLatch.countDown();
                return ProteoSuiteActionResult.emptyResult();
            }
        });

        BackgroundTaskManager.getInstance().submit(task);
    }

    private void doConsensusXMLToMzQuantMLConversion(
            final String consensusXMLFile, final String mzqFile,
            final CountDownLatch featureLinkerUnlabeledQTLatch) {

        ProteoSuiteActionSubject subject = new ProteoSuiteActionSubject() {
            private final File file = new File(consensusXMLFile);

            @Override
            public String getSubjectName() {
                return file.getName();
            }
        };

        BackgroundTask<ProteoSuiteActionSubject> task = new BackgroundTask<>(
                subject, "Converting to mzQuantML");
        task.addProcessingCondition(featureLinkerUnlabeledQTLatch);
        task.addAsynchronousProcessingAction(new ProteoSuiteAction<ProteoSuiteActionResult, ProteoSuiteActionSubject>() {
            @Override
            public ProteoSuiteActionResult act(final ProteoSuiteActionSubject argument) {
                try {
                    if (CONFIG.isRequestingDebugSkipMzqConverter()) {
                        return ProteoSuiteActionResult.emptyResult();
                    }

                    ConsensusXMLProcessor conProc = ConsensusXMLProcessorFactory
                            .getInstance().buildConsensusXMLProcessor(
                                    new File(consensusXMLFile));
                    Map<String, Set<File>> conditionsToFeatureFiles
                            = new HashMap<>();

                    for (RawDataFile rawData : rawDataFiles) {
                        String condition = rawData.getConditions().get("");
                        if (!conditionsToFeatureFiles.keySet().stream().anyMatch(p -> p.equalsIgnoreCase(condition))) {
                            conditionsToFeatureFiles.put(condition,
                                    new HashSet<>());
                        }

                        if (CONFIG.isRequestingSkipInternalAlignment()) {
                            conditionsToFeatureFiles.entrySet().stream().filter(p -> p.getKey().equalsIgnoreCase(condition)).findAny().get()
                                    .getValue().add(
                                    new File(rawData.getAbsoluteFileName().
                                            replaceFirst("\\.[Mm][Zz][Mm][Ll]",
                                                    "_FFC.featureXML")));
                        } else {
                            conditionsToFeatureFiles.entrySet().stream().filter(p -> p.getKey().equalsIgnoreCase(condition)).findAny().get()
                                    .getValue().add(
                                    new File(rawData.getAbsoluteFileName().
                                            replaceFirst("\\.[Mm][Zz][Mm][Ll]",
                                                    "_FFC_MAPC.featureXML")));
                        }
                    }

                    conProc.convert(mzqFile, conditionsToFeatureFiles);
                } catch (IOException | JAXBException ex) {
                    ProteoSuiteException pex = new ProteoSuiteException(
                            "Error doing conversion from consensus XML to mzq.",
                            ex, true);
                    Logger.getLogger(OpenMSLabelFreeWrapper.class.getName()).
                            log(Level.SEVERE, null, ex);
                    return new ProteoSuiteActionResult(pex);
                }

                return ProteoSuiteActionResult.emptyResult();
            }
        });

        task.addCompletionAction(new ProteoSuiteAction<ProteoSuiteActionResult, ProteoSuiteActionSubject>() {
            @Override
            public ProteoSuiteActionResult act(final ProteoSuiteActionSubject argument) {
                QuantDataFile quantFile = new MzQuantMLFile(new File(
                        mzqFile));
                AnalyseDynamicTab.getInstance().getAnalyseStatusPanel()
                        .setQuantitationDone();
                MappingHelper.map(quantFile, CONFIG.
                        isRequestingSkipInternalAlignment());
                return ProteoSuiteActionResult.emptyResult();
            }
        });

        BackgroundTaskManager.getInstance().submit(task);
    }
}
