package org.proteosuite.quantitation;

import java.util.HashMap;
import java.util.List;
import org.proteosuite.model.AnalyseData;
import org.proteosuite.model.RawDataFile;

/**
 *
 * @author SPerkins
 */
public final class XTrackerITRAQWrapper extends XTrackerAbstractWrapper {
    private String plex;

    public XTrackerITRAQWrapper(final List<RawDataFile> rawData) {
        super(rawData, "iTRAQ");
        plex = AnalyseData.getInstance().getMultiplexing();

        searchableMods = new HashMap<>();
        searchableMods.put("iTRAQ4plex (K)", 57.021469);
        searchableMods.put("iTRAQ4plex (N-term)", 144.102063);
    }

    @Override
    protected String getPlexAsString() {
        return plex.contains("4-plex") ? "4-plex" : "8-plex";
    }

    @Override
    protected IsobaricReagent getIsobaricReagent(final String channel) {
        ITRAQReagent reagent = ITRAQReagent.getReagent(plex.contains("4-plex") ? ITRAQReagent.FOUR_PLEX : ITRAQReagent.EIGHT_PLEX, channel);
        return reagent;
    }
}
