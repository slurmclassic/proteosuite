package org.proteosuite.quantitation;

/**
 *
 * @author SPerkins
 */
public final class TMTTechnique {
    /**
     * Private constructor to prevent instantiation.
     */
    private TMTTechnique() { }

    public static final int SEARCH_SCORE = 20;
    public static final double MZ_RANGE_MINUS = 0.05;
    public static final double MZ_RANGE_PLUS = 0.05;
    public static final String INTEGRATION_METHOD = "SumIntensities";
}
