package org.proteosuite.quantitation;

import java.util.Arrays;
import java.util.List;

/**
 *
 * @author SPerkins
 */
public abstract class IsobaricReagent {

    protected String id;
    protected String name;
    protected double reporterMz;
    protected double tagMz;
    protected Double[] correctionFactors;

    public final String getId() {
        return id;
    }

    public final String getName() {
        return name;
    }

    public final double getReporterMz() {
        return reporterMz;
    }

    public final double getTagMz() {
        return tagMz;
    }

    public final List<Double> getCorrectionFactors() {
        return Arrays.asList(correctionFactors);
    }
}
