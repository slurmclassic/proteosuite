package org.proteosuite.model;

public class PeptideViewModel {

    private final String sequence;
    private final ModificationViewModel[] modifications;

    public PeptideViewModel(final PeptideViewModel old) {
        this.sequence = old.sequence;
        this.modifications = new ModificationViewModel[old.modifications.length];
        for (int i = 0; i < old.modifications.length; i++) {
            this.modifications[i] = new ModificationViewModel(
                    old.modifications[i]);
        }
    }

    public PeptideViewModel(final String sequence,
            final ModificationViewModel[] modifications) {
        this.sequence = sequence;
        this.modifications = new ModificationViewModel[modifications.length];
        for (int i = 0; i < modifications.length; i++) {
            this.modifications[i] = new ModificationViewModel(modifications[i]);
        }
    }

    public final String getSequence() {
        return sequence;
    }

    public final ModificationViewModel[] getModifications() {
        return modifications.clone();
    }
}
