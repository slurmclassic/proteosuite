package org.proteosuite.model;

import java.io.File;
import org.proteosuite.ProteoSuiteException;
import org.proteosuite.actions.ProteoSuiteAction;
import org.proteosuite.actions.QuantFilePostLoadAction;
import uk.ac.liv.pgb.jmzqml.xml.io.MzQuantMLUnmarshaller;

/**
 *
 * @author SPerkins
 */
public final class MzQuantMLFile extends QuantDataFile {

    private MzQuantMLUnmarshaller unmarshaller = null;

    public MzQuantMLFile(final File file) {
        super(file);
    }

    @Override
    public boolean isLoaded() {
        return unmarshaller != null;
    }

    @Override
    public void initiateLoading() {

        final BackgroundTask<QuantDataFile> task = new BackgroundTask(this,
                "Load Quantitation Data");
        task.setInvisibility(true);

        task.addAsynchronousProcessingAction(
                new ProteoSuiteAction<ProteoSuiteActionResult, QuantDataFile>() {
            @Override
            public ProteoSuiteActionResult<MzQuantMLUnmarshaller> act(
                    final QuantDataFile argument) {
                try {
                    MzQuantMLUnmarshaller unm = new MzQuantMLUnmarshaller(file);
                    return new ProteoSuiteActionResult<>(unm);
                } catch (Exception e) {
                    return new ProteoSuiteActionResult<>(
                            new ProteoSuiteException(
                                    "Error creating mzQuantMLUnmarshaller.", e, true));
                }
            }
        });

        task.addCompletionAction(
                new ProteoSuiteAction<ProteoSuiteActionResult, QuantDataFile>() {
            @Override
            public ProteoSuiteActionResult act(final QuantDataFile argument) {
                unmarshaller = task.
                        getResultOfClass(MzQuantMLUnmarshaller.class);
                if (unmarshaller == null) {
                    return new ProteoSuiteActionResult(new ProteoSuiteException(
                            "mzQuantML file not read correctly from task results.", true));
                }

                return ProteoSuiteActionResult.emptyResult();
            }
        });

        task.addCompletionAction(new QuantFilePostLoadAction());

        BackgroundTaskManager.getInstance().submit(task);
    }

    public MzQuantMLUnmarshaller getUnmarshaller() {
        return unmarshaller;
    }

    @Override
    public String getSubjectName() {
        return this.getFileName();
    }
}
