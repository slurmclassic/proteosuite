package org.proteosuite.model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionException;
import javax.swing.SwingWorker;
import org.proteosuite.Launcher;
import org.proteosuite.actions.Actions;
import org.proteosuite.actions.ProteoSuiteAction;
import org.slf4j.LoggerFactory;

/**
 *
 * @author SPerkins
 * @param <T> The the type of ProteoSuiteActionSubject
 */
public class BackgroundTask<T extends ProteoSuiteActionSubject> {

    /**
     * The name of the task.
     */
    private final String taskName;

    /**
     * Conditions that must be met before synchronous or asynchronous actions
     * can be done.
     */
    private final Set<CountDownLatch> processingConditions = new HashSet<>();

    /**
     * Synchronous processing actions to perform.
     */
    private final Set<ProteoSuiteAction<ProteoSuiteActionResult, T>> synchronousProcessingActions
            = new LinkedHashSet<>();

    /**
     * Asynchronous processing actions to perform.
     */
    private final Set<ProteoSuiteAction<ProteoSuiteActionResult, T>> asynchronousProcessingActions
            = new LinkedHashSet<>();

    /**
     * Actions to perform once all other actions have been performed.
     */
    private final Set<ProteoSuiteAction<ProteoSuiteActionResult, T>> completionActions
            = new LinkedHashSet<>();
    private ProteoSuiteAction<ProteoSuiteActionResult, T> refreshAction;
    private final Set<ProteoSuiteActionResult> processingResults
            = new LinkedHashSet<>();
    private final T taskSubject;
    private final CountDownLatch taskLatch = new CountDownLatch(1);
    private BlockingQueue<String> errorQueue = null;
    private BlockingQueue<String> outputQueue = null;
    private TaskStreams streams = null;
    private String id = null;
    private boolean invisibility = false;
    private boolean slave = false;
    private String taskStatus = "Pending...";
    private boolean inErrorState = false;

    public BackgroundTask(final T taskSubject, final String taskName) {
        this.taskSubject = taskSubject;
        this.taskName = taskName;
    }

    public final void setId(final String id) {
        this.id = id;
    }

    public final String getId() {
        return this.id;
    }

    public final void setSlaveStatus(final boolean slaveStatus) {
        this.slave = slaveStatus;
    }

    public final boolean isSlave() {
        return this.slave;
    }

    public final void setInvisibility(final boolean invisibility) {
        this.invisibility = invisibility;
    }

    public final boolean isInvisible() {
        return this.invisibility;
    }

    public final String getName() {
        return this.taskName;
    }

    public final String getStatus() {
        if (inErrorState) {
            return "Error: Click For Information";
        }

        return this.taskStatus;
    }

    public final ProteoSuiteActionSubject getSubject() {
        return this.taskSubject;
    }

    public final void addProcessingCondition(final CountDownLatch condition) {
        if (condition != null) {
            processingConditions.add(condition);
        }
    }

    public final void addSynchronousProcessingAction(
            final ProteoSuiteAction<ProteoSuiteActionResult, T> action) {
        this.synchronousProcessingActions.add(action);
    }

    public final void addAsynchronousProcessingAction(
            final ProteoSuiteAction<ProteoSuiteActionResult, T> action) {
        this.asynchronousProcessingActions.add(action);
    }

    public final void addCompletionAction(
            final ProteoSuiteAction<ProteoSuiteActionResult, T> action) {
        this.completionActions.add(action);
    }

    public final void setRefreshAction(
            final ProteoSuiteAction<ProteoSuiteActionResult, T> action) {
        this.refreshAction = action;
    }

    public final CountDownLatch getTaskLatch() {
        return this.taskLatch;
    }

    public final void queueForExecution(final ExecutorService service) {
        if (refreshAction == null) {
            refreshAction = Actions.emptyAction(taskSubject);
        }

        if (!invisibility) {
            refreshAction.act(taskSubject);
        }

        BackgroundTask.this.synchronousProcessingActions.stream().forEach((
                action) -> {
            processingResults.add(action.act(taskSubject));
        });

        SwingWorker<String, Void> worker = new SwingWorker<String, Void>() {
            @Override
            protected String doInBackground() throws Exception {
                for (CountDownLatch condition
                        : BackgroundTask.this.processingConditions) {
                    condition.await();
                }

                taskStatus = "In Progress...";
                if (!invisibility) {
                    refreshAction.act(taskSubject);
                }

                BackgroundTask.this.asynchronousProcessingActions.stream().
                        forEach(action -> {
                            processingResults.add(action.act(taskSubject));
                        });

                return "OK";
            }

            @Override
            protected void done() {
                try {
                    String taskResult = get();
                    if (!taskResult.equals("OK")) {
                        inErrorState = true;
                    }
                } catch (InterruptedException | ExecutionException ex) {
                    LoggerFactory.getLogger(BackgroundTask.class).error(
                            "There was a problem getting the expected \"OK\" result from the SwingWorker.", ex);
                    inErrorState = true;
                }

                taskStatus = "Complete";
                if (!invisibility) {
                    refreshAction.act(taskSubject);
                }

                processingResults.stream().filter((result) -> (result.
                        hasException() && result.getException().isTerminal())).
                        forEach((result) -> {
                            Launcher.handleException(result.getException());
                        });

                BackgroundTask.this.completionActions.stream().forEachOrdered(
                        action -> {
                            action.act(taskSubject);
                        });

                taskLatch.countDown();
            }
        };

        try {
            service.submit(worker);
        } catch (RejectedExecutionException ex) {
            System.out.println("Failed to schedule task for execution.");
            inErrorState = true;
        }
    }

    public final void setStreams(final TaskStreams streams) {
        this.streams = streams;
        errorQueue = new LinkedBlockingQueue<>();
        outputQueue = new LinkedBlockingQueue<>();
        readStreams();

    }

    public final <S> S getResultOfClass(final Class<S> clazz) {
        for (ProteoSuiteActionResult result : this.processingResults) {
            Object resultValue = result.getResultObject();
            if (resultValue != null && resultValue.getClass().equals(clazz)) {
                return (S) resultValue;
            }
        }

        return (S) null;
    }

    private void readStreams() {
        if (streams != null && streams.getOutputStream() != null) {
            BackgroundTaskManager.getInstance().submit(new StreamReader(streams.
                    getOutputStream(), false, true));
        }

//        try {
//            Thread.sleep(5000);
//        }
//        catch (InterruptedException ex) {
//            Logger.getLogger(BackgroundTask.class.getName()).log(Level.SEVERE, null, ex);
//        }
        if (streams != null && streams.getErrorStream() != null) {
            BackgroundTaskManager.getInstance().submit(new StreamReader(streams.
                    getErrorStream(), true, true));
        }
    }

    public final BlockingQueue getOutputQueue() {
        return this.outputQueue;
    }

    public final BlockingQueue getErrorQueue() {
        return this.errorQueue;
    }

    private class StreamReader extends BackgroundTask<ProteoSuiteActionSubject> {

        private boolean printAlso;
        private boolean errorOutput;
        private InputStream stream;

        StreamReader(final InputStream streamIn,
                final boolean errorOutputIn, final boolean printAlsoIn) {
            super(() -> "External Program Feed", "Print Output");

            this.stream = streamIn;
            this.errorOutput = errorOutputIn;
            this.printAlso = printAlsoIn;
            super.setSlaveStatus(true);
            super.setInvisibility(true);

            super.addAsynchronousProcessingAction(
                    (ProteoSuiteAction<ProteoSuiteActionResult, ProteoSuiteActionSubject>) (
                            ProteoSuiteActionSubject ignored) -> {
                        try (BufferedReader reader = new BufferedReader(
                                new InputStreamReader(stream))) {
                            String line;
                            while ((line = reader.readLine()) != null) {
                                if (line.isEmpty()) {
                                    continue;
                                }

                                if (line.startsWith("Target:")) {
                                    continue;
                                }

                                if (line.equals("Info: omssa: not enough peaks in spectra")) {
                                    continue;
                                }

                                if (line.equals("Error: modification with mass not recognized")) {
                                    continue;
                                }

                                if (line.equals("\tNot found, so look to see if it is N-terminal")) {
                                    continue;
                                }


                                if (line.matches("[A-Z]+,\\s;\\s\\d(;\\s\\d)?")) {
                                    continue;
                                }

                                if (line.matches("PDH_\\d+,dbseq_generic|\\S+|,\\d+\\.\\d+")) {
                                    continue;
                                }

                                if (this.printAlso) {
                                    System.out.println(line);
                                }

                                if (this.errorOutput) {
                                    line = "<font color=\"red\">" + line + "</font>";
                                    errorQueue.add(line);
                                    inErrorState = true;
                                    refreshAction.act(taskSubject);
                                } else {
                                    if (line.toUpperCase().contains("EXCEPTION")) {
                                        inErrorState = true;
                                    }

                                    outputQueue.add(line);
                                }
                            }

                            if (this.errorOutput) {
                                errorQueue.add("<END>");
                            } else {
                                outputQueue.add("<END>");
                            }
                        } catch (IOException ex) {
                            LoggerFactory.getLogger(StreamReader.class).error("There was a problem reading the output stream from the background task.", ex);
                        }

                        return ProteoSuiteActionResult.emptyResult();
                    });
        }
    }
}
