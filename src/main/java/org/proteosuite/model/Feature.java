/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.proteosuite.model;

/**
 *
 * @author SPerkins
 */
public final class Feature extends MzIntensityPair {

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 29 * hash + (int) (Double.doubleToLongBits(super.getMz())
                ^ (Double.doubleToLongBits(super.getMz()) >>> 32));
        hash = 29 * hash + (int) (Double.doubleToLongBits(super.getIntensity())
                ^ (Double.doubleToLongBits(super.getIntensity()) >>> 32));
        hash = 59 * hash + this.charge;
        return hash;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Feature that = (Feature) obj;
        if (this.charge != that.charge) {
            return false;
        }

        if (Double.doubleToLongBits(super.getMz()) != Double.doubleToLongBits(
                that.getMz())) {
            return false;
        }

        if (Double.doubleToLongBits(super.getIntensity()) != Double.
                doubleToLongBits(that.getIntensity())) {
            return false;
        }

        return true;
    }
    private int charge;

    public Feature(final double mzValue, final double intensity, final int charge) {
        super(mzValue, intensity);
        this.charge = charge;
    }

    public Feature(final MzIntensityPair mzIntensity) {
        this(mzIntensity.getMz(), mzIntensity.getIntensity(), 0);
    }

    public int getCharge() {
        return this.charge;
    }
}
