package org.proteosuite.model;

/**
 *
 * @author SPerkins
 */
public class PrimitiveDoublePair {

    protected double firstValue;
    protected double secondValue;

    public final void setFirstValue(final double firstValue) {
        this.firstValue = firstValue;
    }

    public final double getFirstValue() {
        return this.firstValue;
    }

    public final void setSecondValue(final double secondValue) {
        this.secondValue = secondValue;
    }

    public final double getSecondValue() {
        return this.secondValue;
    }
}
