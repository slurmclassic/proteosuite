package org.proteosuite.model;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.LinkedList;

/**
 *
 * @author SPerkins
 */
public class Spectrum extends LinkedList<MzIntensityPair> {
    private static final long serialVersionUID = 1L;
    private MzIntensityPair basePeak = new MzIntensityPair(0.0, 0.0);
    private double retentionTimeInSeconds = 0.0;
    private int spectrumIndex = -1;
    private String spectrumID = null;

    public final int getSpectraCount() {
        return super.size();
    }

    public final void setBasePeak(final MzIntensityPair basePeak) {
        this.basePeak = basePeak;
    }

    public final void setRetentionTimeInMinutes(final double minutes) {
        this.retentionTimeInSeconds = minutes * 60;
    }

    public final void setRetentionTimeInSeconds(final double seconds) {
        this.retentionTimeInSeconds = seconds;
    }

    public final double getRetentionTimeInSeconds() {
        return retentionTimeInSeconds;
    }

    public final MzIntensityPair getBasePeak() {
        return basePeak;
    }

    public final void setSpectrumIndex(final int spectrumIndex) {
        this.spectrumIndex = spectrumIndex;
    }

    public final int getSpectrumIndex() {
        return spectrumIndex;
    }

    public final void setSpectrumID(final String spectrumID) {
        this.spectrumID = spectrumID;
    }

    public final String getSpectrumID() {
        return this.spectrumID;
    }

    private void writeObject(final ObjectOutputStream stream)
            throws IOException {
        stream.defaultWriteObject();
    }

    private void readObject(final ObjectInputStream stream)
            throws IOException, ClassNotFoundException {
        stream.defaultReadObject();
    }
}
