/*
 * ProteoSuite is releases under the Apache 2 license.
 * This means that you are free to modify, use and distribute the software in all legislations provided you give credit to our project.
 */
package org.proteosuite.model;

import org.proteosuite.ProteoSuiteException;

/**
 *
 * @author SPerkins
 * @param <T> The type of the result.
 */
public final class ProteoSuiteActionResult<T> {

    private final T result;
    private final ProteoSuiteException pex;
    private static final ProteoSuiteActionResult EMPTY_RESULT
            = new ProteoSuiteActionResult();

    public ProteoSuiteActionResult(final T result, final ProteoSuiteException exception) {
        this.result = result;
        this.pex = exception;
    }

    public ProteoSuiteActionResult(final ProteoSuiteException exception) {
        this(null, exception);
    }

    public ProteoSuiteActionResult(final T result) {
        this(result, null);
    }

    public ProteoSuiteActionResult() {
        this(null, null);
    }

    public T getResultObject() {
        return result;
    }

    public boolean hasException() {
        return pex != null;
    }

    public ProteoSuiteException getException() {
        return pex;
    }

    public static ProteoSuiteActionResult emptyResult() {
        return EMPTY_RESULT;
    }
}
