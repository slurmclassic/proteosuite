package org.proteosuite.model;

/**
 *
 * @author SPerkins
 */
public class MzIntensityPair extends PrimitiveDoublePair {

    public MzIntensityPair() {
        this(0.0, 0.0);
    }

    public MzIntensityPair(final double mz, final double intensity) {
        super.firstValue = mz;
        super.secondValue = intensity;
    }

    public final void setMz(final double mz) {
        super.setFirstValue(mz);
    }

    public final double getMz() {
        return super.getFirstValue();
    }

    public final void setIntensity(final double intensity) {
        super.setSecondValue(intensity);
    }

    public final double getIntensity() {
        return super.getSecondValue();
    }

    @Override
    public final String toString() {
        return "M/Z=" + this.getMz() + "|Intensity=" + this.getIntensity();
    }
}
