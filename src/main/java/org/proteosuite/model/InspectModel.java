package org.proteosuite.model;

import com.compomics.util.gui.spectrum.ChromatogramPanel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JPanel;

/**
 *
 * @author SPerkins
 */
public class InspectModel {

    private final Map<String, ChromatogramPanel> cachedChromatograms
            = new HashMap<>();
    private final Map<String, JPanel> cached2DViews = new HashMap<>();

    private final List<RawDataFile> rawData = new ArrayList<>();
    private final List<IdentDataFile> identData = new ArrayList<>();
    private final List<QuantDataFile> quantData = new ArrayList<>();

    public final synchronized void addRawDataFile(final RawDataFile rawDataFile) {
        rawData.add(rawDataFile);
    }

    public final synchronized void removeRawDataFile(
            final RawDataFile rawDataFile) {
        rawData.remove(rawDataFile);
    }

    public final synchronized List<RawDataFile> getRawData() {
        return rawData;
    }

    public final synchronized void addIdentDataFile(
            final IdentDataFile identDataFile) {
        identData.add(identDataFile);
    }

    public final synchronized void removeIdentDataFile(
            final IdentDataFile identDataFile) {
        identData.remove(identDataFile);
    }

    public final synchronized List<IdentDataFile> getIdentData() {
        return identData;
    }

    public final synchronized void addQuantDataFile(
            final QuantDataFile quantDataFile) {
        quantData.add(quantDataFile);
    }

    public final synchronized List<QuantDataFile> getQuantData() {
        return quantData;
    }

    public final ChromatogramPanel getCachedChromatogramOrNull(
            final String fileName) {
        return cachedChromatograms.get(fileName);
    }

    public final void addCachedChromatogram(final String fileName,
            final ChromatogramPanel chromatogram) {
        cachedChromatograms.put(fileName, chromatogram);
    }

    public final JPanel getCached2DViewOrNull(final String fileName) {
        return cached2DViews.get(fileName);
    }

    public final void addCached2DView(final String fileName,
            final JPanel twoDView) {
        cached2DViews.put(fileName, twoDView);
    }

    public final boolean isRawDataFile(final String fileName) {
        for (RawDataFile dataFile : rawData) {
            if (dataFile.getFileName().equals(fileName)) {
                return true;
            }
        }

        return false;
    }

    public final RawDataFile getRawDataFile(final String fileName) {
        for (RawDataFile dataFile : rawData) {
            if (dataFile.getFileName().equals(fileName)) {
                return dataFile;
            }
        }

        return null;
    }

    public final boolean isIdentFile(final String fileName) {
        for (IdentDataFile identFile : identData) {
            if (identFile.getFileName().equals(fileName)) {
                return true;
            }
        }

        return false;
    }

    public final IdentDataFile getIdentDataFile(final String fileName) {
        for (IdentDataFile identFile : identData) {
            if (identFile.getFileName().equals(fileName)) {
                return identFile;
            }
        }

        return null;
    }

    public final boolean isQuantFile(final String fileName) {
        for (QuantDataFile identFile : quantData) {
            if (identFile.getFileName().equals(fileName)) {
                return true;
            }
        }

        return false;
    }

    public final QuantDataFile getQuantDataFile(final String fileName) {
        for (QuantDataFile quantFile : quantData) {
            if (quantFile.getFileName().equals(fileName)) {
                return quantFile;
            }
        }

        return null;
    }

    public final void clear() {
        rawData.clear();
        identData.clear();
        quantData.clear();
    }
}
