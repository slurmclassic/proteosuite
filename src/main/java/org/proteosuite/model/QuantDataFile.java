package org.proteosuite.model;

import java.io.File;

/**
 *
 * @author SPerkins
 */
public abstract class QuantDataFile implements ProteoSuiteActionSubject {

    protected File file;

    public QuantDataFile(final File file) {
        this.file = file;
        initiateLoading();
    }

    public final String getFileName() {
        return file.getName();
    }

    public final File getFile() {
        return file;
    }

    public final String getAbsoluteFileName() {
        return file.getAbsolutePath();
    }

    public abstract boolean isLoaded();

    public abstract void initiateLoading();
}
