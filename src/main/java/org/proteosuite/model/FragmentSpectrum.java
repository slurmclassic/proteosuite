package org.proteosuite.model;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 *
 * @author SPerkins
 */
public final class FragmentSpectrum extends Spectrum {

    private static final long serialVersionUID = 1L;
    private final Feature precursorMzChargeIntensity;

    public FragmentSpectrum(final double precursorMz, final double precursorIntensity,
            final int charge) {
        this(new Feature(precursorMz, precursorIntensity, charge));
    }

    public FragmentSpectrum(final Feature mzIntensityCharge) {
        this.precursorMzChargeIntensity = mzIntensityCharge;
    }

    public FragmentSpectrum(final MzIntensityPair mzIntensityPair) {
        this.precursorMzChargeIntensity = new Feature(mzIntensityPair);
    }

    public Feature getPrecursor() {
        return precursorMzChargeIntensity;
    }

    private void writeObject(final ObjectOutputStream stream)
            throws IOException {
        stream.defaultWriteObject();
    }

    private void readObject(final ObjectInputStream stream)
            throws IOException, ClassNotFoundException {
        stream.defaultReadObject();
    }
}
