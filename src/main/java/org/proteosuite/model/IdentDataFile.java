package org.proteosuite.model;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import uk.ac.liv.mzqlib.idmapper.data.Tolerance;

/**
 *
 * @author SPerkins
 */
public abstract class IdentDataFile implements ProteoSuiteActionSubject {

    protected RawDataFile parent;
    protected File file;
    protected int psmCountPassingThreshold = -1;
    protected int psmCountNotPassingThrehsold = -1;
    protected int peptideCountPassingThreshold = -1;
    protected String thresholdingUsed = "";
    protected Map<String, String> thresholdables = new HashMap<>();
    private String thresholdStatus = "Needed";
    private boolean fromExternal = false;

    private boolean mayNeedCleaning = false;

    public IdentDataFile(final File file, final RawDataFile parent) {
        this.file = file;
        this.parent = parent;
        initiateLoading();
    }

    public final String getAbsoluteFileName() {
        return file.getAbsolutePath();
    }

    public final void setThresholdStatus(final String status) {
        this.thresholdStatus = status;
    }

    public final String getThresholdStatus() {
        return mayNeedCleaning ? thresholdStatus : "Complete";
    }

    public final String getFileName() {
        return file.getName();
    }

    public final File getFile() {
        return file;
    }

    public final RawDataFile getParent() {
        return parent;
    }

    public final void setFromExternal(final boolean fromExternal) {
        this.fromExternal = fromExternal;
    }

    public final void setCleanable(final boolean cleanable) {
        this.mayNeedCleaning = cleanable;
    }

    public final boolean isCleanable() {
        return this.mayNeedCleaning;
    }

    public final boolean isFromExternal() {
        return this.fromExternal;
    }

    public abstract String getFormat();

    public abstract boolean isLoaded();

    protected abstract void initiateLoading();

    public abstract void computePSMStats();

    public abstract boolean hasRetentionTimes();

    public abstract int getPSMCountPassingThreshold();

    public abstract int getPSMCountNotPassingThreshold();

    public abstract int getPeptideCountPassingThreshold();

    public abstract String getThresholdingUsed();

    public abstract Map<String, String> getThresholdables();

    public abstract Tolerance getSearchPrecursorTolerance();
}
