package org.proteosuite.model;

public class ModificationViewModel {

    private final int location;
    private final String[] residues;
    private final String name;

    public ModificationViewModel(final ModificationViewModel old) {
        this.location = old.location;
        this.name = old.name;

        this.residues = old.residues.clone();
    }

    public ModificationViewModel(final int location, final String[] residues,
            final String name) {
        this.location = location;
        this.residues = residues.clone();
        this.name = name;
    }

    public final int getLocation() {
        return location;
    }

    public final String[] getResidues() {
        return residues.clone();
    }

    public final String getName() {
        return name;
    }

}
