package org.proteosuite.model;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import org.proteosuite.ProteoSuiteException;
import org.proteosuite.actions.IdentFilePostLoadAction;
import org.proteosuite.actions.ProteoSuiteAction;
import org.proteosuite.gui.analyse.AnalyseDynamicTab;
import org.proteosuite.gui.analyse.CleanIdentificationsStep;
import org.proteosuite.gui.analyse.CreateOrLoadIdentificationsStep;

import org.proteosuite.utils.PrimitiveUtils;
import org.proteosuite.utils.StringUtils;
import uk.ac.ebi.jmzidml.MzIdentMLElement;
import uk.ac.ebi.jmzidml.model.mzidml.CvParam;
import uk.ac.ebi.jmzidml.model.mzidml.SpectrumIdentificationItem;
import uk.ac.ebi.jmzidml.model.mzidml.SpectrumIdentificationProtocol;
import uk.ac.ebi.jmzidml.model.mzidml.SpectrumIdentificationResult;
import uk.ac.ebi.jmzidml.xml.io.MzIdentMLUnmarshaller;
import uk.ac.liv.mzqlib.idmapper.data.Tolerance;
import uk.ac.liv.mzqlib.idmapper.data.Tolerance.ToleranceUnit;
import uk.ac.liv.pgb.psi.utils.ident.MzIdentMLSingleStorageFactory;
import uk.ac.liv.pgb.psi.utils.ident.interfaces.MzIdentMLSingleStorage;

/**
 *
 * @author SPerkins
 */
public final class MzIdentMLFile extends IdentDataFile {

    private MzIdentMLUnmarshaller unmarshaller;
    private MzIdentMLSingleStorage singleStorage;
    private boolean hasRetentionTimes = true;

    public MzIdentMLFile(final File file, final RawDataFile parent) {
        super(file, parent);
    }

    @Override
    public String getFormat() {
        return "mzIdentML";
    }

    @Override
    public boolean isLoaded() {
        if (file == null) {
            return false;
        }

        return true;
    }

    @Override
    public int getPSMCountPassingThreshold() {
        return this.psmCountPassingThreshold;
    }

    @Override
    public int getPSMCountNotPassingThreshold() {
        return this.psmCountNotPassingThrehsold;
    }

    @Override
    public int getPeptideCountPassingThreshold() {
        return this.peptideCountPassingThreshold;
    }

    @Override
    public String getThresholdingUsed() {
        return this.thresholdingUsed;
    }

    @Override
    public Map<String, String> getThresholdables() {
        return this.thresholdables;
    }

    @Override
    public synchronized void computePSMStats() {
        final BackgroundTask<IdentDataFile> task = new BackgroundTask<>(this,
                "Compute PSM Stats");

        task.addAsynchronousProcessingAction(
                new ProteoSuiteAction<ProteoSuiteActionResult, IdentDataFile>() {
            @Override
            public ProteoSuiteActionResult<String[]> act(final IdentDataFile ignored) {
                SpectrumIdentificationProtocol protocol = unmarshaller.
                        unmarshal(
                                MzIdentMLElement.SpectrumIdentificationProtocol);
                List<CvParam> thresholdingParams = protocol.getThreshold().
                        getCvParam();
                List<String> thresholdTerms = new ArrayList<>();
                for (CvParam param : thresholdingParams) {
                    if (param.getValue() == null || param.getValue().isEmpty()) {
                        thresholdTerms.add(param.getName());
                    } else {
                        thresholdTerms.add(param.getName() + "(" + param.
                                getValue() + ")");
                    }
                }

                String thresholding = StringUtils.join(", ", thresholdTerms);

                int psmPassingThreshold = 0;
                int psmNotPassingThreshold = 0;
                Map<String, String> thresholdTypes = new HashMap<>();
                Set<String> peptidesPassingThreshold = new HashSet<>();

                Iterator<SpectrumIdentificationResult> spectrumIdentificationResultIterator
                        = unmarshaller.unmarshalCollectionFromXpath(
                                MzIdentMLElement.SpectrumIdentificationResult);
                while (spectrumIdentificationResultIterator.hasNext()) {
                    SpectrumIdentificationResult result
                            = spectrumIdentificationResultIterator.next();
                    for (SpectrumIdentificationItem item : result.
                            getSpectrumIdentificationItem()) {
                        if (item.isPassThreshold()) {
                            psmPassingThreshold++;
                            peptidesPassingThreshold.add(item.getPeptideRef());
                        } else {
                            psmNotPassingThreshold++;
                        }

                        for (CvParam param : item.getCvParam()) {
                            if (PrimitiveUtils.isDouble(param.getValue())) {
                                thresholdTypes.put(param.getName(), param.
                                        getAccession());
                            }
                        }
                    }
                }

                // Need to pack threshold types as a string.
                StringBuilder thresholdsBuilder = new StringBuilder();
                for (Entry<String, String> entry : thresholdTypes.entrySet()) {
                    thresholdsBuilder.append(entry.getKey());
                    thresholdsBuilder.append(",");
                    thresholdsBuilder.append(entry.getValue());
                    thresholdsBuilder.append(";");
                }

                return new ProteoSuiteActionResult<String[]>(new String[]{
                    String.valueOf(psmPassingThreshold), String.valueOf(
                    psmNotPassingThreshold),
                    String.valueOf(peptidesPassingThreshold.size()),
                    thresholding, thresholdsBuilder.toString()});
            }
        });

        task.addCompletionAction(
                new ProteoSuiteAction<ProteoSuiteActionResult, IdentDataFile>() {
            @Override
            public ProteoSuiteActionResult act(final IdentDataFile argument) {
                String[] computationResult = task.getResultOfClass(
                        String[].class);
                if (computationResult == null || computationResult[0] == null) {
                    return new ProteoSuiteActionResult(new ProteoSuiteException(
                            "Error - PSM Stats Calculator Did Not Run Properly", true));
                }

                psmCountPassingThreshold = Integer.
                        parseInt(computationResult[0]);
                psmCountNotPassingThrehsold = Integer.parseInt(
                        computationResult[1]);
                peptideCountPassingThreshold = Integer.parseInt(
                        computationResult[2]);
                thresholdingUsed = computationResult[3];
                String thresholdablesPacked = computationResult[4];
                String[] thresholdablesEntriesUnpacked = thresholdablesPacked.
                        split(";");
                for (String thresholdableEntry : thresholdablesEntriesUnpacked) {
                    String[] thresholdableEntryUnpacked = thresholdableEntry.
                            split(",");
                    thresholdables.put(thresholdableEntryUnpacked[0],
                            thresholdableEntryUnpacked[1]);
                }

                ((CleanIdentificationsStep) AnalyseDynamicTab.CLEAN_IDENTIFICATIONS_STEP).
                        refreshFromData();

                return ProteoSuiteActionResult.emptyResult();
            }
        });

        BackgroundTaskManager.getInstance().submit(task);
    }

    @Override
    protected void initiateLoading() {
        final BackgroundTask<IdentDataFile> task = new BackgroundTask<>(this,
                "Loading Identifications");

        task.addAsynchronousProcessingAction(
                new ProteoSuiteAction<ProteoSuiteActionResult, IdentDataFile>() {
            @Override
            public ProteoSuiteActionResult<MzIdentMLUnmarshaller> act(
                    final IdentDataFile argument) {
                if (MzIdentMLFile.this.getParent() != null) {
                    MzIdentMLFile.this.getParent().setIdentStatus("Loading...");
                    ((CreateOrLoadIdentificationsStep) (AnalyseDynamicTab.CREATE_OR_LOAD_IDENTIFICATIONS_STEP)).
                            refreshFromData();
                }

//                try {
//                    FileFormatUtils.fixEncoding(file);
//                    identManager.saveMzid(file);
//                }
//                catch (IOException ex) {
//                    return new ProteoSuiteActionResult(new ProteoSuiteException("Error reading mzid file to internal storage mechanism.", ex));
//                }
                MzIdentMLUnmarshaller source = new MzIdentMLUnmarshaller(file);
                singleStorage = MzIdentMLSingleStorageFactory.getInstance().
                        getSingleStorage(file);

                // Let's check if this file has retention times.
                Iterator<SpectrumIdentificationResult> iterator = source.unmarshalCollectionFromXpath(MzIdentMLElement.SpectrumIdentificationResult);
                if (iterator.hasNext()) {
                    SpectrumIdentificationResult result = iterator.next();
                    double rt = getRetentionTime(result);
                    MzIdentMLFile.this.hasRetentionTimes = !Double.isNaN(rt);
                }

                return new ProteoSuiteActionResult(source);
            }
        });

        task.addCompletionAction(
                new ProteoSuiteAction<ProteoSuiteActionResult, IdentDataFile>() {
            @Override
            public ProteoSuiteActionResult act(final IdentDataFile argument) {
                unmarshaller = task.
                        getResultOfClass(MzIdentMLUnmarshaller.class);
                return ProteoSuiteActionResult.emptyResult();
            }
        });

        task.addCompletionAction(new IdentFilePostLoadAction());

        BackgroundTaskManager.getInstance().submit(task);
    }

    public MzIdentMLUnmarshaller getUnmarshaller() {
        return unmarshaller;
    }

    public MzIdentMLSingleStorage getSingleStorage() {
        return singleStorage;
    }

    @Override
    public String getSubjectName() {
        return this.getFileName();
    }

    @Override
    public Tolerance getSearchPrecursorTolerance() {
        if (this.unmarshaller == null) {
            return null;
        }

        SpectrumIdentificationProtocol identificationProtocol = unmarshaller.unmarshal(MzIdentMLElement.SpectrumIdentificationProtocol);
        if (identificationProtocol == null) {
            return null;
        }

        uk.ac.ebi.jmzidml.model.mzidml.Tolerance tolerance = identificationProtocol.getParentTolerance();
        if (tolerance == null) {
            return null;
        }

        Optional<Tolerance> mzqTolerance = tolerance.getCvParam().stream().map(p -> {
            String value = p.getValue();
            String unit = p.getUnitName();
            double valueDouble;
            ToleranceUnit unitObj;
            if (value != null && !value.isEmpty()) {
                valueDouble = Double.parseDouble(value);
            } else {
                return null;
            }

            if (unit != null && !unit.isEmpty()) {
                unitObj = unit.equalsIgnoreCase("DALTON") ? ToleranceUnit.DALTON : ToleranceUnit.PPM;
            } else {
                return null;
            }

            return new Tolerance(valueDouble, unitObj);
        }).filter(p -> p != null).max((a, b) -> Double.compare(a.getTolerance(), b.getTolerance()));

        if (mzqTolerance.isPresent()) {
            return mzqTolerance.get();
        } else {
            return null;
        }
    }

    @Override
    public boolean hasRetentionTimes() {
        return this.hasRetentionTimes;
    }

    private static double getRetentionTime(final SpectrumIdentificationResult sir) {
        double rt = Double.NaN;

        List<CvParam> cvParams = sir.getCvParam();
        for (CvParam cp : cvParams) {
            if (cp.getAccession().equals("MS:1000016")) {
                String value = cp.getValue();
                String unit = cp.getUnitName();
                if (unit == null || unit.equals("")) {
                    return rt;
                }

                switch (unit.toLowerCase(Locale.ENGLISH)) {
                    case "second":
                        return Double.parseDouble(value) / 60;
                    case "minute":
                        return Double.parseDouble(value);
                    case "hour": // rare case?
                        return Double.parseDouble(value) * 60;
                    default:
                        return rt;
                }
            }
        }

        return rt;
    }
}
