package org.proteosuite.model;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 *
 * @author SPerkins
 */
public abstract class RawDataFile implements Iterable<Spectrum>,
        ProteoSuiteActionSubject {

    protected Map<String, Spectrum> cachedSpectra = null;
    protected boolean cachingComplete = false;
    protected File file;

    protected boolean[] peakPicking = {false, false};
    protected boolean peakPickingChecked;
    protected int spectraCount;
    protected boolean[] msLevelPresence = {false, false};
    protected boolean spectraCountChecked;
    private File peakListFile;
    private final Map<String, String> assayConditions = new HashMap<>();
    private IdentDataFile identFile = null;
    private String identStatus = "<None>";
    private boolean selectedUsingFolderMode = false;
    private static final double MEGABYTE = 1024.0 * 1024.0;

    @Override
    public abstract Iterator<Spectrum> iterator();

    public final Spectrum getSpectrumByID(final String id) {
        if (cachedSpectra.size() > 0) {
            return cachedSpectra.get(id);
        } else {
            for (Spectrum spectrum : this) {
                if (spectrum.getSpectrumID().equals(id)) {
                    return spectrum;
                }
            }
        }

        return null;
    }

    public RawDataFile(final File file) {
        this.file = file;
        initiateLoading();
    }

    public final Map<String, String> getConditions() {
        return assayConditions;
    }

    public final String getIdentStatus() {
        return identStatus;
    }

    public final File getPeakListFile() {
        return peakListFile;
    }

    public final void setPeakListFile(final File peakFile) {
        peakListFile = peakFile;
    }

    public final void setSelectedUsingFolderMode(final boolean selectionMode) {
        this.selectedUsingFolderMode = selectionMode;
    }

    public final boolean isSelectedUsingFolderMode() {
        return this.selectedUsingFolderMode;
    }

    public final void setIdentStatus(final String identStatus) {
        this.identStatus = identStatus;
    }

    public final void resetAssay() {
        assayConditions.clear();
    }

    public final void setAssays(final String[] assays) {
        for (String assay : assays) {
            if (!assayConditions.containsKey(assay)) {
                assayConditions.put(assay, "");
            }
        }
    }

    public final void setIdentificationDataFile(final IdentDataFile identFile) {
        this.identFile = identFile;
    }

    public final IdentDataFile getIdentificationDataFile() {
        return identFile;
    }

    public final File getFile() {
        return file;
    }

    public final int getFileSizeInMegaBytes() {
        long bytes = file.length();
        double megabytes = (double) bytes / MEGABYTE;
        return (int) megabytes;
    }

    public final boolean[] getMSLevelPresence() {
        if (!peakPickingChecked) {
            this.getPeakPicking();
        }

        return msLevelPresence.clone();
    }

    public abstract boolean isLoaded();

    public abstract String getFormat();

    public abstract int getSpectraCount();

    public abstract boolean[] getPeakPicking();

    public final String getFileName() {
        return file.getName();
    }

    public final String getAbsoluteFileName() {
        return file.getAbsolutePath();
    }

    protected abstract void initiateLoading();

}
