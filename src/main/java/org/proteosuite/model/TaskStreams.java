package org.proteosuite.model;

import java.io.InputStream;

/**
 *
 * @author SPerkins
 */
public class TaskStreams {

    private InputStream outputStream = null;
    private InputStream errorStream = null;

    public final InputStream getOutputStream() {
        return outputStream;
    }

    public final InputStream getErrorStream() {
        return errorStream;
    }

    public final void setOutputStream(final InputStream stream) {
        this.outputStream = stream;
    }

    public final void setErrorStream(final InputStream stream) {
        this.errorStream = stream;
    }
}
