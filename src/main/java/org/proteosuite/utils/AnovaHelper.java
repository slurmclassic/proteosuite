package org.proteosuite.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.xml.bind.JAXBException;
import org.proteosuite.ProteoSuiteException;
import org.proteosuite.actions.ProteoSuiteAction;
import org.proteosuite.config.GlobalConfig;
import org.proteosuite.gui.analyse.AnalyseDynamicTab;
import org.proteosuite.model.AnalyseData;
import org.proteosuite.model.BackgroundTask;
import org.proteosuite.model.BackgroundTaskManager;
import org.proteosuite.model.ProteoSuiteActionSubject;
import org.proteosuite.model.MzQuantMLFile;
import org.proteosuite.model.ProteoSuiteActionResult;
import uk.ac.cranfield.mzqlib.MzqLib;
import uk.ac.liv.mzqlib.stats.MzqQLAnova;

/**
 *
 * @author SPerkins
 */
public final class AnovaHelper {

    private static final GlobalConfig CONFIG = GlobalConfig.getInstance();

    private AnovaHelper() {
    }

    public static void anova(final File quantDataFile) {
        final BackgroundTask<ProteoSuiteActionSubject> task
                = new BackgroundTask<>(new ProteoSuiteActionSubject() {
                    @Override
                    public String getSubjectName() {
                        return quantDataFile.getName();
                    }
                }, "Calculating ANOVA Values");
        //task.setInvisibility(true);

        task.addAsynchronousProcessingAction(new ProteoSuiteAction<ProteoSuiteActionResult, ProteoSuiteActionSubject>() {
            @Override
            public ProteoSuiteActionResult<File> act(
                    final ProteoSuiteActionSubject argument) {
                AnalyseDynamicTab.getInstance().getAnalyseStatusPanel().
                        setAnovaProcessing();
                File outputFile = null;
                try {
                    outputFile = new File(quantDataFile.getCanonicalPath().
                            replaceFirst("\\.[Mm][Zz][Qq]$", "_anova.mzq"));
                } catch (IOException ex) {
                    if (outputFile == null) {
                        ProteoSuiteException pex = new ProteoSuiteException(
                                "Unable to generate output file for AnovaHelper.",
                                ex, true);
                        return new ProteoSuiteActionResult(pex);
                    }
                }

                if (CONFIG.isRequestionDebugSkipAnova()) {
                    return new ProteoSuiteActionResult<>(outputFile);
                }

                MzqQLAnova anova = new MzqQLAnova(quantDataFile.
                        getAbsolutePath(), "ProteinGroup", getGroupedAssays(),
                        "MS:1002518");
                try {
                    anova.writeMzQuantMLFile(outputFile.getAbsolutePath());
                } catch (JAXBException ex) {
                    Logger.getLogger(AnovaHelper.class.getName()).log(
                            Level.SEVERE, null, ex);
                    return new ProteoSuiteActionResult(new ProteoSuiteException(
                            "Error writing ANOVA values to mzq file.", ex, true));
                }

                MzqLib mzqlib = new MzqLib("csv", outputFile.getAbsolutePath(), outputFile.getAbsolutePath().replaceFirst("\\.[Mm][Zz][Qq]$", ".csv")); //NOPMD

                return new ProteoSuiteActionResult(outputFile);
            }
        });

        task.addCompletionAction(
                new ProteoSuiteAction<ProteoSuiteActionResult, ProteoSuiteActionSubject>() {
            @Override
            public ProteoSuiteActionResult act(
                    final ProteoSuiteActionSubject argument) {
                AnalyseDynamicTab.getInstance().getAnalyseStatusPanel().
                        setAnovaDone();
                AnalyseDynamicTab.getInstance().getAnalyseStatusPanel().
                        setResultsDone();
                File outputFile = task.getResultOfClass(File.class);

                AnalyseData.getInstance().getInspectModel().addQuantDataFile(
                        new MzQuantMLFile(outputFile));

                return ProteoSuiteActionResult.emptyResult();
            }
        });

//        task.addCompletionAction((ProteoSuiteActionSubject argument) -> {
//            File outputFile = task.getResultOfClass(File.class);
//            QualityHelper.createQualityPlots(outputFile);
//            return ProteoSuiteActionResult.emptyResult();
//        });
        BackgroundTaskManager.getInstance().submit(task);
    }

    private static List<List<String>> getGroupedAssays() {
        AnalyseData data = AnalyseData.getInstance();
        Map<String, List<String>> conditionToAssays = new HashMap<>();
        for (int i = 0; i < data.getRawDataCount(); i++) {
            String thisCondition = data.getRawDataFile(i).getConditions().
                    get("").toUpperCase();
            if (conditionToAssays.containsKey(thisCondition)) {
                conditionToAssays.get(thisCondition).add("ass_" + i);
            } else {
                conditionToAssays.put(thisCondition, new ArrayList<>(Arrays.
                        asList("ass_" + i)));
            }
        }

        System.out.println("Assay groups:");
        for (Entry<String, List<String>> entry : conditionToAssays.entrySet()) {
            System.out.println(entry.getKey() + ": " + prettifyList(entry.
                    getValue()));
        }

        return new ArrayList<>(conditionToAssays.values());
    }

    private static <T> String prettifyList(final List<T> list) {
        List<String> stringValues = list.stream().map(p -> p.toString()).
                collect(Collectors.toList());
        return "[" + String.join(",", stringValues) + "]";

    }
}
