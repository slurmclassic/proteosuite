package org.proteosuite.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import org.proteosuite.Launcher;
import org.proteosuite.ShutdownManager;
import org.proteosuite.executor.Executor;

/**
 * System utilities for ProteoSuite
 *
 * @author fgonzalez
 */
public final class SystemUtils {

    private static final int MEGABYTE = 1024 * 1024;
    private static final Runtime RUNTIME = Runtime.getRuntime();
    private static final Map<String, File> PREVIOUS_FIND_COMMAND_RESULTS
            = new HashMap<>();
    private static final Set<File> PREVIOUS_FIND_COMMAND_DIRECTORIES
            = new HashSet<>();
    private static final Map<String, File> PREVIOUS_FIND_FILE_RESULTS
            = new HashMap<>();
    private static SystemType TYPE = null;

    public String getTime() {
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        return dateFormat.format(new Date());
    }

    public long getUsedMemory() {
        return (RUNTIME.totalMemory() - RUNTIME.freeMemory()) / MEGABYTE;
    }

    public long getFreeMemory() {
        return RUNTIME.freeMemory() / MEGABYTE;
    }

    public long getTotalMemory() {
        return RUNTIME.totalMemory() / MEGABYTE;
    }

    public long getMaxMemory() {
        return RUNTIME.maxMemory() / MEGABYTE;
    }

    public boolean checkURL(final String urlName) {
        try {
            HttpURLConnection.setFollowRedirects(false);
            HttpURLConnection con = (HttpURLConnection) new URL(urlName)
                    .openConnection();
            con.setRequestMethod("HEAD");
            return con.getResponseCode() == HttpURLConnection.HTTP_OK;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public int getAvailableProcessors() {
        return RUNTIME.availableProcessors();
    }

    public String getRuntimeInfo() {
        return System.getProperty("java.version") + " " + System.getProperty(
                "os.arch");
    }

    public static boolean deleteRecursive(final File path) throws
            FileNotFoundException {
        if (!path.exists()) {
            throw new FileNotFoundException(path.getAbsolutePath());
        }

        boolean allDeleted = true;
        File[] children = path.listFiles();
        if (children == null) {
            return path.delete();
        }

        for (File child : children) {
            if (child.isFile()) {
                if (!child.delete()) {
                    allDeleted = false;
                }
            } else if (child.isDirectory() && !deleteRecursive(child)) {
                allDeleted = false;
            }
        }

        return allDeleted ? path.delete() : false;
    }

    public static File findExecutionCommand(final String command) {
        return findExecutionCommand(command, Collections.emptyList());
    }

    public static File findExecutionCommand(final String command,
            final Iterable<File> locationHints) {
        // First let's see if we have seen this command before in this session.
        if (PREVIOUS_FIND_COMMAND_RESULTS.containsKey(command)) {
            return PREVIOUS_FIND_COMMAND_RESULTS.get(command);
        }

        // We haven't seen it before, so let's see if the command is on the system path.
        // We use the 'where' command for windows, and the 'which' command for linux.
        SystemType type = getSystemType();
        Executor exec = null;
        switch (type) {
            case WIN32:
            case WIN64:
                exec = new Executor(findFileName("where.exe", Arrays.asList(
                        File.listRoots())));
                break;
            case LINUX32:
            case LINUX64:
                exec = new Executor(findFileName("which", Arrays.asList(File.
                        listRoots())));
                break;
            default:
                break;
        }

        if (exec != null) {
            try {
                exec.callExe(new String[]{command});
            } catch (IOException ex) {
                Logger.getLogger(SystemUtils.class.getName()).log(Level.SEVERE,
                        null, ex);
            }

            List<String> output = exec.getOutput();

            if (output != null) {
                for (String outputLine : output) {
                    if (!outputLine.startsWith("INFO") && !outputLine.
                            startsWith("JINKIES")) {
                        File file = new File(outputLine.trim());
                        PREVIOUS_FIND_COMMAND_RESULTS.put(command, file);
                        PREVIOUS_FIND_COMMAND_DIRECTORIES.add(file.getParentFile());
                        try {
                            return file.getCanonicalFile();
                        } catch (IOException ex) {
                            Logger.getLogger(SystemUtils.class.getName()).log(
                                    Level.SEVERE, null, ex);
                        }
                    }
                }
            }
        }

        // Command wasn't in the path, so let's check the location hints.
        File foundFile = findFileName(command, locationHints);
        if (foundFile != null) {
            PREVIOUS_FIND_COMMAND_RESULTS.put(command, foundFile);
            PREVIOUS_FIND_COMMAND_DIRECTORIES.add(foundFile.getParentFile());
            return foundFile;
        }

        // Command wasn't in the hinted locations, so we have to be more inventive.
        foundFile = findFileName(command, PREVIOUS_FIND_COMMAND_DIRECTORIES);
        if (foundFile != null) {
            PREVIOUS_FIND_COMMAND_RESULTS.put(command, foundFile);
            PREVIOUS_FIND_COMMAND_DIRECTORIES.add(foundFile.getParentFile());
            return foundFile;
        }

        // Command wasn't in the most recent directories that other commands where found in.
        // Let's look through the file system.
        foundFile = findFileName(command, Arrays.asList(File.listRoots()));
        if (foundFile != null) {
            PREVIOUS_FIND_COMMAND_RESULTS.put(command, foundFile);
            PREVIOUS_FIND_COMMAND_DIRECTORIES.add(foundFile.getParentFile());
            return foundFile;
        }

        // If we ever get to this point the command was nowhere to be found, so return null;
        return null;
    }

    private static File findFileName(final String fileName, final Iterable<File> directories) {
        return findFileName(fileName, directories, false);
    }

    private static File findFileName(final String fileName, final Iterable<File> directories,
            final boolean skipSystemLocationCheck) {
        for (File directory : directories) {
            File foundFile = findFileName(fileName, directory,
                    skipSystemLocationCheck);
            if (foundFile != null) {
                return foundFile;
            }
        }

        return null;
    }

    public static File findFileName(final String fileName, final File rootDirectory) {
        return findFileName(fileName, rootDirectory, false);
    }

    private static File findFileName(final String fileName, final File rootDirectory,
            final boolean skipSystemLocationCheck) {
        if (PREVIOUS_FIND_FILE_RESULTS.containsKey(fileName)) {
            return PREVIOUS_FIND_FILE_RESULTS.get(fileName);
        }

        if (!skipSystemLocationCheck) {
            File systemLocationsFile = findFileName(fileName,
                     Arrays.stream("c:\\Windows\\System32;/usr/bin".split(";")).map(p -> new File(p)).collect(Collectors.toList()),
                    true);
            if (systemLocationsFile != null) {
                return systemLocationsFile;
            }
        }

        if (rootDirectory == null || !rootDirectory.exists() || !rootDirectory.
                isDirectory()) {
            return null;
        }

        File[] children = rootDirectory.listFiles();
        if (children == null) {
            return null;
        }

        for (File child : children) {
            if (child.isDirectory()) {
                File foundFile = findFileName(fileName, child, true);
                if (foundFile != null) {
                    return foundFile;
                }
            } else if (child.isFile()) {
                if (getSystemType().equals(SystemType.WIN32) || getSystemType().
                        equals(SystemType.WIN64)) {
                    if (child.getName().toUpperCase().equals(fileName.
                            toUpperCase())) {
                        PREVIOUS_FIND_FILE_RESULTS.put(fileName, child);
                        return child;
                    }
                } else if (child.getName().equals(fileName)) {
                    PREVIOUS_FIND_FILE_RESULTS.put(fileName, child);
                    return child;
                }
            }
        }

        return null;
    }

    public static SystemType getSystemType() {
        if (TYPE != null) {
            return TYPE;
        }

        String operatingSystemType = System.getProperty("os.name");
        if (operatingSystemType.startsWith("Windows")) {
            if (System.getenv("ProgramFiles(x86)") != null) {
                TYPE = SystemType.WIN64;
            } else {
                TYPE = SystemType.WIN32;
            }
        } else {
            Executor exec = new Executor(findFileName("uname.sh", Arrays.asList(
                    File.listRoots())));
            try {
                exec.callExe(new String[]{"-a"});
            } catch (IOException ex) {
                Logger.getLogger(SystemUtils.class.getName()).log(Level.SEVERE,
                        null, ex);
                TYPE = SystemType.LINUX32;
            }

            List<String> output = exec.getOutput();
            for (String outputLine : output) {
                if (outputLine.contains("x86_64")
                        || outputLine.contains("ia64")) {
                    TYPE = SystemType.LINUX64;
                } else {
                    TYPE = SystemType.LINUX32;
                }
            }
        }

        return TYPE;
    }

    public static void spawnNewProteosuite(final int memoryMegabytes) {
        System.out.println("Starting new Proteosuite instance with "
                + memoryMegabytes + "M of memory.");
        File currentJar = null;
        try {
            currentJar = new File(Launcher.class.getProtectionDomain()
                    .getCodeSource().getLocation().toURI());
        } catch (URISyntaxException e) {
            System.out.println(
                    "Could not locate the current JAR file, so could not spawn new Proteosuite session.");
            ExceptionCatcher.reportException(e);
            return;

        }

        final String javaBin = System.getProperty("java.home") + File.separator
                + "bin" + File.separator + "java";

        final List<String> command = new ArrayList<String>();
        command.add(javaBin);
        command.add("-Xmx" + memoryMegabytes + "M");
        command.add("-jar");
        command.add(currentJar.getPath());

        ProcessBuilder pb = new ProcessBuilder(command);
        pb.inheritIO();
        try {
            pb.start();
            ShutdownManager.getInstance().allowShutdown();
        } catch (IOException e) {
            ExceptionCatcher.reportException(e);
            e.printStackTrace();
        }
    }

    public enum SystemType {

        WIN32, WIN64, LINUX32, LINUX64
    }
}
