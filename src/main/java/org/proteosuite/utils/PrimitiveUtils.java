package org.proteosuite.utils;

public final class PrimitiveUtils {

    /**
     * Private constructor to prevent instantiation.
     */
    private PrimitiveUtils() {
    }

    /**
     * Round a number to n decimals
     *
     * @param fValue Value
     * @param iDecimals Number of decimals
     *
     * @return Number rounded
     */
    public static float round(final float fValue, final int iDecimals) {
        float p = (float) Math.pow(10, iDecimals);
        float tmp = Math.round(fValue * p);
        return (float) tmp / p;
    }

    /**
     * Truncate a number to n decimals
     *
     * @param fValue Value
     * @param iDecimals Number of decimals
     *
     * @return Number truncated
     */
    public static double truncate(final double fValue, final int iDecimals) {
        double multiplier = Math.pow(10, iDecimals);

        return Math.floor(multiplier * fValue) / multiplier;
    }

    public static boolean isDouble(final String candidate) {
        try {
            double validatedDouble = Double.parseDouble(candidate);
            System.out.println("Parsed '" + validatedDouble
                    + "' as double from '" + candidate);
        } catch (NumberFormatException ex) {
            return false;
        }

        return true;
    }

    public static boolean equalsWithinMargin(final double val1, final double val2, final double margin) {
        double diff = val1 - val2;
        return Math.abs(diff) < margin;
    }

    public static boolean isInteger(final String candidate) {
        try {
            int validatedInt = Integer.parseInt(candidate);
            System.out.println("Parsed '" + validatedInt + "' as int from '"
                    + candidate);
        } catch (NumberFormatException ex) {
            return false;
        }

        return true;
    }
}
