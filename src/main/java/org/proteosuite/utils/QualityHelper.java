/*
 * ProteoSuite is released under the Apache 2 license.
 * This means that you are free to modify, use and distribute the software in all legislations provided you give credit to our project.
 */
package org.proteosuite.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import org.proteosuite.ProteoSuiteException;
import org.proteosuite.actions.ProteoSuiteAction;
import org.proteosuite.model.BackgroundTask;
import org.proteosuite.model.BackgroundTaskManager;
import org.proteosuite.model.ProteoSuiteActionResult;
import org.proteosuite.model.ProteoSuiteActionSubject;
import uk.ac.liv.mzqlib.openms.jaxb.ConsensusElement;
import uk.ac.liv.mzqlib.openms.jaxb.ConsensusXML;
import uk.ac.liv.mzqlib.openms.jaxb.Element;

/**
 *
 * @author SPerkins
 */
public final class QualityHelper {

    private QualityHelper() {
    }

    public static void createQualityPlots(final File inputFile) {
        final BackgroundTask<ProteoSuiteActionSubject> task
                = new BackgroundTask<>(new ProteoSuiteActionSubject() {
                    @Override
                    public String getSubjectName() {
                        return inputFile.getName();
                    }
                }, "Creating Quality Plots");

        task.addAsynchronousProcessingAction(new ProteoSuiteAction() {
            @Override
            public Object act(final Object argument) {
                // Do stuff!

                Path directoryPath;
                try {
                    directoryPath = Files.createTempDirectory(
                            "proteosuite_quality_");
                } catch (final IOException ex) {
                    return new ProteoSuiteActionResult(new ProteoSuiteException(
                            "Error: could not create temporary directory for quality files.",
                            ex, true));
                }

                Path rtFormatPath = directoryPath.resolve("rt_format.csv");

                Map<Integer, Integer> assaysPerFeatureCounts = new HashMap<>();

                // Need to find location of the consensus file.
                ConsensusXML consensus = null;
                try {
                    JAXBContext consensusContext = JAXBContext.newInstance(
                            new Class[]{ConsensusXML.class});
                    Unmarshaller consensusUnmarshaller = consensusContext.
                            createUnmarshaller();

                    String dataDirectory = inputFile.getParentFile().
                            getAbsolutePath();
                    consensus = (ConsensusXML) consensusUnmarshaller.unmarshal(
                            new File(dataDirectory + "\\"
                                    + "unlabeled_result_FLUQT.consensusXML"));
                } catch (final JAXBException ex) {
                    return new ProteoSuiteActionResult(new ProteoSuiteException(
                            "Error: could not open consensusXML file for reading.",
                            ex, true));
                }

                List<String> rtHeaderLine = new LinkedList<>();
                rtHeaderLine.add("Feature_ID");
                rtHeaderLine.add("RT_Centroid");
                for (int i = 0; i < consensus.getMapList().getCount(); i++) {
                    rtHeaderLine.add("Assay_" + i);
                }

                Map<Integer, List<Double>> assayCountsToRts = new HashMap<>();
                for (int i = 1; i <= consensus.getMapList().getCount(); i++) {
                    assayCountsToRts.put(i, new LinkedList<>());
                }

                try (BufferedWriter rtWriter = Files.newBufferedWriter(rtFormatPath)) {
                    rtWriter.write(String.join(",", rtHeaderLine));
                    rtWriter.newLine();
                    for (ConsensusElement element : consensus.
                            getConsensusElementList().getConsensusElement()) {
                        int assayCount = element.getGroupedElementList().
                                getElement().size();
                        Integer featuresWithThisNumberOfAssaysCount
                                = assaysPerFeatureCounts.get(assayCount);
                        if (featuresWithThisNumberOfAssaysCount == null) {
                            assaysPerFeatureCounts.put(assayCount, 1);
                        } else {
                            featuresWithThisNumberOfAssaysCount++;
                            assaysPerFeatureCounts.replace(assayCount,
                                    featuresWithThisNumberOfAssaysCount);
                        }

                        assayCountsToRts.get(assayCount).add(element.getCentroid().
                                getRt());

                        String[] rtValues = new String[(int) consensus.getMapList().
                                getCount()];
                        for (int i = 0; i < rtValues.length; i++) {
                            rtValues[i] = "";
                        }

                        for (Element groupedElement : element.
                                getGroupedElementList().getElement()) {
                            long mapNumber = groupedElement.getMap();
                            rtValues[(int) mapNumber] = String.valueOf(
                                    groupedElement.getRt());
                        }

                        rtWriter.write(element.getId() + "," + element.
                                getCentroid().getRt() + "," + String.join(",",
                                        rtValues));
                        rtWriter.newLine();
                    }

                } catch (final IOException ex) {
                    return new ProteoSuiteActionResult(new ProteoSuiteException(
                            "Error: could not write RT distribution quality file.",
                            ex, true));
                }

                Path assayPerFeatureFormatPath = directoryPath.resolve(
                        "assays_per_feature_format.csv");

                try {
                    BufferedWriter assayPerFeatureWriter = Files.
                            newBufferedWriter(assayPerFeatureFormatPath);
                    assayPerFeatureWriter.write("Assays_Per_Feature,Frequency");
                    assayPerFeatureWriter.newLine();
                    for (Entry<Integer, Integer> entry : assaysPerFeatureCounts.
                            entrySet()) {
                        assayPerFeatureWriter.write(entry.getKey() + ","
                                + entry.getValue());
                        assayPerFeatureWriter.newLine();
                    }

                    assayPerFeatureWriter.close();
                } catch (final IOException ex) {
                    return new ProteoSuiteActionResult(new ProteoSuiteException(
                            "Error: could write assays per feature count quality file.",
                            ex, true));
                }

                Path rtDistributionFormatPath = directoryPath.resolve(
                        "rt_distribution_format.csv");

                List<Integer> assayCountsToRtsKeys = new LinkedList<>(
                        assayCountsToRts.keySet());
                Collections.sort(assayCountsToRtsKeys);
                try {
                    BufferedWriter rtDistributionWriter = Files.
                            newBufferedWriter(rtDistributionFormatPath);
                    rtDistributionWriter.write(String.join(",",
                            assayCountsToRtsKeys.stream().map(p -> String.
                                    valueOf(p)).collect(Collectors.toList())));
                    rtDistributionWriter.newLine();
                    boolean listsExhausted = false;
                    Map<Integer, Iterator<Double>> map = assayCountsToRts.
                            entrySet().stream().collect(Collectors.toMap(p -> p.
                                    getKey(), p -> p.getValue().iterator()));
                    while (!listsExhausted) {
                        List<String> lineRts = new LinkedList<>();
                        for (Integer assayCount : assayCountsToRtsKeys) {
                            Iterator<Double> rtsIterator = map.get(assayCount);
                            lineRts.add(rtsIterator.hasNext() ? String.valueOf(
                                    rtsIterator.next()) : "");
                        }

                        rtDistributionWriter.write(String.join(",", lineRts));
                        rtDistributionWriter.newLine();

                        if (map.values().stream().allMatch(p -> !p.hasNext())) {
                            listsExhausted = true;
                        }

                    }

                    rtDistributionWriter.close();
                } catch (final IOException ex) {
                    return new ProteoSuiteActionResult(new ProteoSuiteException(
                            "Error: could write RT distribution per assay count quality file.",
                            ex, true));
                }

                // Created the quality files!
                File mzqJar = getMzqViewerJar();
                List<String> commandList = new LinkedList<>();
                commandList.add("java");
                commandList.add("-Xmx2G");
                commandList.add("-jar");
                commandList.add(mzqJar.getAbsolutePath());
                commandList.add("--dataForQualityCalculationLocation="
                        + directoryPath);

                try {
                    Process proc = getProcess(commandList);
                    proc.waitFor();
                } catch (final IOException ex) {
                    return new ProteoSuiteActionResult(new ProteoSuiteException(
                            "Error: could not start mzqLibrary to display quality plots.",
                            ex, true));
                } catch (final InterruptedException ex) {
                    return new ProteoSuiteActionResult(new ProteoSuiteException(
                            "Error: there was a problem while waiting for the mzqLibrary to close after displaying quality plots.",
                            ex, true));
                }

                return ProteoSuiteActionResult.emptyResult();
            }
        });

        BackgroundTaskManager.getInstance().submit(task);
    }

    private static Process getProcess(final List<String> commandList) throws
            IOException {
        ProcessBuilder builder = new ProcessBuilder(commandList);
        Process proc = builder.start();
        return proc;
    }

    private static File getMzqViewerJar() {
        try {
            File thisClassFile = new File(URLDecoder.decode(
                    QualityHelper.class.getProtectionDomain().
                    getCodeSource().getLocation().getPath(), "UTF-8"));
            if (thisClassFile.getAbsolutePath().endsWith("classes")) {
                File target = thisClassFile.getParentFile();
                Optional<File> proteosuiteBuildFolderOptional = Arrays.stream(
                        target.listFiles()).filter(p -> p.isDirectory() && p.
                                getName().startsWith("proteosuite")).findFirst();
                if (!proteosuiteBuildFolderOptional.isPresent()) {
                    throw new ProteosuiteBuildFolderException(
                            "Proteosuite build folder is not present: have you built the project?");
                }

                Optional<File> mzqViewerFolderOptional = Arrays.stream(
                        proteosuiteBuildFolderOptional.get().listFiles()).
                        filter(p -> p.isDirectory() && "mzqViewer".equals(p.getName())).findFirst();
                if (!mzqViewerFolderOptional.isPresent()) {
                    throw new ProteosuiteBuildFolderException(
                            "Proteosuite build mzqViewer folder is not present: have you built the project? If so check in the POM file for the correct mzqViewer link.");
                }

                File mzqViewerFolder = mzqViewerFolderOptional.get();
                Optional<File> mzqViewerJarOptional = Arrays.stream(mzqViewerFolder.
                        listFiles()).filter(p -> p.isFile() && p.getName().endsWith(".jar")).findFirst();
                if (!mzqViewerJarOptional.isPresent()) {
                    throw new ProteosuiteBuildFolderException(
                            "Proteosuite build mzqViewer JAR is not present: have you built the project? If so check in the POM file for the correct mzqViewer link.");
                }

                return mzqViewerJarOptional.get();
            }

            Optional<File> mzqViewerFolderOptional = Arrays.stream(thisClassFile.
                    getParentFile().listFiles()).filter(p -> p.isDirectory()
                            && "mzqViewer".equals(p.getName())).findFirst();
            if (!mzqViewerFolderOptional.isPresent()) {
                throw new ProteosuiteBuildFolderException(
                        "Proteosuite mzqViewer folder is not present.");
            }

            File mzqViewerFolder = mzqViewerFolderOptional.get();
            Optional<File> mzqViewerJarOptional = Arrays.stream(mzqViewerFolder.
                    listFiles()).filter(p -> p.isFile() && p.getName().
                            endsWith(".jar")).
                    findFirst();
            if (!mzqViewerJarOptional.isPresent()) {
                throw new ProteosuiteBuildFolderException(
                        "Proteosuite build mzqViewer JAR is not present: have you built the project? If so check in the POM file for the correct mzqViewer link.");
            }

            return mzqViewerJarOptional.get();
        } catch (final UnsupportedEncodingException ex) {
            throw new RuntimeException("UTF-8 not supported?!");
        }
    }

    private static class ProteosuiteBuildFolderException extends RuntimeException {
        ProteosuiteBuildFolderException(final String message) {
            super(message);
        }
    }
}
