package org.proteosuite.utils;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public final class ExceptionCatcher {
    /**
     * Private constructor to prevent instantiation of this class.
     */
    private ExceptionCatcher() { }

    private static final String UPDATE_CHECK_URL
            = "http://www.proteosuite.org/service/crash.php?%s";

    /**
     * Contacts update check service and returns location of latest version
     *
     * @param exception The exception to be reported.
     */
    public static void reportException(final Exception exception) {
        StringBuilder builder = new StringBuilder("exc=");
        builder.append(exception.getClass().toString());
        builder.append("&msg=");
        builder.append(exception.getMessage());
        for (StackTraceElement element : exception.getStackTrace()) {
            builder.append("&trace[]=");
            builder.append(element.toString());
        }

        String queryString = builder.toString().replace(" ", "%20");

        String query = String.format(UPDATE_CHECK_URL, queryString);
        try {
            URL url = new URL(query);

            try (InputStream urlStream = url.openStream()) {
                while (true) {
                    int read = urlStream.read();
                    if (read == -1) {
                        break;
                    }
                }
            }

        } catch (final IOException ignore) {
            // Most likely no Internet connection, do nothing!
            // Though we lost debug info :(
            System.out.println("Warning: could not report exception.");
            // Maybe we should write to file in case of no internet connection.
        }
    }
}
