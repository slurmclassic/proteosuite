package org.proteosuite.utils;

import java.util.Iterator;
import java.util.Objects;
import javax.swing.ListModel;

/**
 *
 * @author SPerkins
 */
public final class StringUtils {

    private static final String EMPTY_STRING = "";
    private static final String SPACE = " ";

    private StringUtils() {
    }

    public static String emptyString() {
        return EMPTY_STRING;
    }

    public static String space() {
        return SPACE;
    }

    public static String join(final String delimiter,
            final Iterable<String> elements) {
        Objects.requireNonNull(delimiter);
        Objects.requireNonNull(elements);
        StringBuilder builder = new StringBuilder();
        Iterator<String> iterator = elements.iterator();
        while (iterator.hasNext()) {
            builder.append(iterator.next());
            if (iterator.hasNext()) {
                builder.append(delimiter);
            }
        }

        return builder.toString();
    }

    public static <T> String join(final String delimiter,
            final ListModel<T> elements) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < elements.getSize(); i++) {
            builder.append(elements.getElementAt(i));

            if (i != elements.getSize() - 1) {
                builder.append(delimiter);
            }
        }

        return builder.toString();
    }
}
