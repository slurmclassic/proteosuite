package org.proteosuite.utils;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.JAXBException;
import org.proteosuite.ProteoSuiteException;
import org.proteosuite.actions.ProteoSuiteAction;
import org.proteosuite.config.GlobalConfig;
import org.proteosuite.gui.analyse.AnalyseDynamicTab;
import org.proteosuite.model.AnalyseData;
import org.proteosuite.model.BackgroundTask;
import org.proteosuite.model.BackgroundTaskManager;
import org.proteosuite.model.IdentDataFile;
import org.proteosuite.model.MzQuantMLFile;
import org.proteosuite.model.ProteoSuiteActionResult;
import org.proteosuite.model.QuantDataFile;
import org.proteosuite.model.RawDataFile;
import static org.proteosuite.utils.PrimitiveUtils.equalsWithinMargin;
import org.slf4j.LoggerFactory;
import uk.ac.liv.mzqlib.idmapper.MzqMzIdMapper;
import uk.ac.liv.mzqlib.idmapper.MzqMzIdMapperFactory;
import uk.ac.liv.mzqlib.idmapper.data.Tolerance;
import uk.ac.liv.mzqlib.idmapper.data.Tolerance.ToleranceUnit;
import uk.ac.liv.pgb.jmzqml.xml.io.MzQuantMLUnmarshaller;

/**
 *
 * @author SPerkins
 */
public final class MappingHelper {

    private static final GlobalConfig CONFIG = GlobalConfig.getInstance();

    private MappingHelper() {
    }

    public static void map(final QuantDataFile quantData,
            final boolean alignmentWasSkipped) {

        final BackgroundTask<QuantDataFile> task = new BackgroundTask<>(
                quantData, "Mapping Identifications");

        task.addAsynchronousProcessingAction((ProteoSuiteAction<ProteoSuiteActionResult, QuantDataFile>) (
                        final QuantDataFile ignored) -> {
                    AnalyseDynamicTab.getInstance().getAnalyseStatusPanel().
                    setMappingProcessing();
                    File outputFile = new File(quantData.getAbsoluteFileName().
                            replaceFirst("\\.[Mm][Zz][Qq]$", "_mapped.mzq"));

                    Map<String, String> rawToMzidMap = new HashMap<>();
                    for (int i = 0; i < AnalyseData.getInstance().getRawDataCount(); i++) {
                        RawDataFile rawData = AnalyseData.getInstance().
                        getRawDataFile(i);
                        IdentDataFile identData = rawData.getIdentificationDataFile();
                        if (identData != null) {
                            if (alignmentWasSkipped) {
                                rawToMzidMap.put(rawData.getAbsoluteFileName().
                                        replaceFirst(".[Mm][Zz][Mm][Ll]",
                                                "_FFC.featureXML"), identData.
                                        getAbsoluteFileName());
                            } else {
                                rawToMzidMap.put(rawData.getAbsoluteFileName().
                                        replaceFirst(".[Mm][Zz][Mm][Ll]",
                                                "_FFC_MAPC.featureXML"), identData.
                                        getAbsoluteFileName());
                            }
                        }
                    }

                    System.out.println(
                            "Map provided for mapper follows: (rawData -> identData)");
                    rawToMzidMap.entrySet().stream().forEach((entry) -> {
                        System.out.println(entry.getKey() + " -> " + entry.getValue());
                    });

                    try {
                        if (CONFIG.isRequestingDebugSkipMapper()) {
                            return new ProteoSuiteActionResult<>(outputFile);
                        }

                        MzQuantMLUnmarshaller umarsh = new MzQuantMLUnmarshaller(
                                quantData.getFile());

                        // Build our tolerance unit.
                        double toleranceValue = CONFIG.getPrecursorToleranceValue();
                        String toleranceUnit = CONFIG.getPrecursorToleranceUnit();


                        Tolerance tolerance = null;
                        if (!equalsWithinMargin(toleranceValue, 0.0, 0.00000001) && toleranceUnit != null) {
                            tolerance = new Tolerance(CONFIG.
                                    getPrecursorToleranceValue(), CONFIG.
                                    getPrecursorToleranceUnit().equalsIgnoreCase("ppm")
                                    ? ToleranceUnit.PPM : ToleranceUnit.DALTON);
                        } else {
                            // Need to get the tolerance from an mzid file, or else it will be set as below.

                            AnalyseData data = AnalyseData.getInstance();
                            for (int i = 0; i < data.getRawDataCount(); i++) {
                                RawDataFile rawDataFile = data.getRawDataFile(i);
                                if (rawDataFile == null) {
                                    continue;
                                }

                                IdentDataFile identDataFile = rawDataFile.getIdentificationDataFile();
                                if (identDataFile == null) {
                                    continue;
                                }

                                tolerance = identDataFile.getSearchPrecursorTolerance();
                                if (tolerance != null) {
                                    break;
                                }
                            }
                        }

                        if (tolerance == null) {
                            tolerance = new Tolerance(10, ToleranceUnit.PPM);
                        }

                        // Double tolerance used for MS1 seearch, for best performance.
                        tolerance.setTolerance(tolerance.getTolerance() * 2);

                        MzqMzIdMapper mapper = MzqMzIdMapperFactory.getInstance().
                        buildMzqMzIdMapper(umarsh, rawToMzidMap, tolerance);
                        mapper.createMappedFile(outputFile);
                    } catch (IOException | JAXBException ex) {
                        LoggerFactory.getLogger(MappingHelper.class).error("There was an error mapping mzid files to an mzq file.", ex);
                        return new ProteoSuiteActionResult(new ProteoSuiteException(
                                "Error mapping mzid files to mzq file.", ex, true));
                    } catch (IllegalStateException ex) {
                        LoggerFactory.getLogger(MappingHelper.class).error("There was an error in an mzid file.", ex);
                        return new ProteoSuiteActionResult(new ProteoSuiteException("Error in mzid file.", ex, true));
                    }

                    return new ProteoSuiteActionResult<>(outputFile);
                }
        );

        task.addCompletionAction(
                (ProteoSuiteAction<ProteoSuiteActionResult, QuantDataFile>) (
                        final QuantDataFile argument) -> {
                    AnalyseDynamicTab.getInstance().getAnalyseStatusPanel().
                    setMappingDone();
                    File outputFile = task.getResultOfClass(File.class);

                    NormalisationHelper.normalise(new MzQuantMLFile(outputFile));
                    return ProteoSuiteActionResult.emptyResult();
                }
        );

        BackgroundTaskManager.getInstance()
                .getTasksOfType(
                        "Create Identifications").stream().map(p -> p.getTaskLatch()).
                forEach((latch) -> {
                    task.addProcessingCondition(latch);
                }
                );

        BackgroundTaskManager.getInstance()
                .submit(task);
    }
}
