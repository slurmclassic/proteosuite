package org.proteosuite.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.proteosuite.ProteoSuiteException;
import org.proteosuite.actions.ProteoSuiteAction;
import org.proteosuite.config.GlobalConfig;
import org.proteosuite.gui.analyse.AnalyseDynamicTab;
import org.proteosuite.model.BackgroundTask;
import org.proteosuite.model.BackgroundTaskManager;
import org.proteosuite.model.ProteoSuiteActionResult;
import org.proteosuite.model.ProteoSuiteActionSubject;
import uk.ac.man.mzqlib.postprocessing.ProteinAbundanceInference;

/**
 *
 * @author SPerkins
 */
public final class ProteinInferenceHelper {

    public static final String REPORTER_ION_INTENSITY = "MS:1001847";
    public static final String LCMS_FEATURE_INTENSITY = "MS:1001840";
    public static final String LCMS_FEATURE_NORMALISED_INTENSITY = "MS:1001850";
    public static final String LABEL_FREE_PEPTIDE = "MS:1001891";

    private static final String LABEL_FREE_PROTEIN_GROUP_NORMALISED_ACC
            = "MS:1002518";
    private static final String LABEL_FREE_PROTEIN_GROUP_NORMALISED_NAME
            = "Progenesis: protein group normalised abundance";
    private static final String LABEL_FREE_PROTEIN_GROUP_RAW_ACC = "MS:1002519";
    private static final String LABEL_FREE_PROTEIN_GROUP_RAW_NAME
            = "Progenesis: protein group raw abundance";
    private static final GlobalConfig CONFIG = GlobalConfig.getInstance();

    private ProteinInferenceHelper() {
    }

    public static void infer(final File inputFile, final String quantMethod,
            final String quantDataType, final String mergeOperator) {

        BackgroundTask<ProteoSuiteActionSubject> task = new BackgroundTask<>(
                new ProteoSuiteActionSubject() {
            @Override
            public String getSubjectName() {
                return inputFile.getName();
            }
        }, "Inferring Proteins");

        task.addAsynchronousProcessingAction(
                new ProteoSuiteAction<ProteoSuiteActionResult, ProteoSuiteActionSubject>() {
            @Override
            public ProteoSuiteActionResult<File> act(
                    final ProteoSuiteActionSubject argument) {
                AnalyseDynamicTab.getInstance().getAnalyseStatusPanel()
                        .setProteinInferenceProcessing();

                File outputFile = null;
                try {
                    outputFile = new File(inputFile.getCanonicalPath().
                            replaceFirst("\\.[Mm][Zz][Qq]$",
                                    "_protein_inference.mzq"));
                } catch (IOException e) {
                    if (outputFile == null) {
                        ProteoSuiteException pex = new ProteoSuiteException(
                                "Unable to generate output file for ProteinInference.",
                                e, true);
                        return new ProteoSuiteActionResult(pex);
                    }
                }

                if (CONFIG.isRequestionDebugSkipProteinInference()) {
                    return new ProteoSuiteActionResult<>(outputFile);
                }

                ProteinAbundanceInference inference = null;
                try {
                    switch (quantDataType) {
                        case REPORTER_ION_INTENSITY:
                            //                            inference = new ProteinAbundanceInference(inputFile, outputFile,
                            //                                    quantMethod, mergeOperator, quantDataType,
                            //                                    "inputPeptideQuantLayerID",
                            //                                    LABEL_FREE_PROTEIN, LABEL_FREE_PROTEIN_DESC,
                            //                                    quantLayerType);
                            break;
                        case LCMS_FEATURE_INTENSITY:
                            //                            inference = new ProteinAbundanceInference(inputFile.getCanonicalPath(), outputFile, "AssayQuantLayer",
                            //                                    "MS:1001840", "MS:1001850", LABEL_FREE_PROTEIN_GROUP_NORMALISED_ACC, LABEL_FREE_PROTEIN_GROUP_NORMALISED_NAME,
                            //                                    LABEL_FREE_PROTEIN_GROUP_RAW_ACC, LABEL_FREE_PROTEIN_GROUP_RAW_NAME, "sum");
                            inference = new ProteinAbundanceInference(inputFile.
                                    getCanonicalPath(), outputFile.
                                    getCanonicalPath(), "sum",
                                    LCMS_FEATURE_NORMALISED_INTENSITY,
                                    LCMS_FEATURE_INTENSITY,
                                    LABEL_FREE_PROTEIN_GROUP_NORMALISED_ACC,
                                    LABEL_FREE_PROTEIN_GROUP_NORMALISED_NAME,
                                    LABEL_FREE_PROTEIN_GROUP_RAW_ACC,
                                    LABEL_FREE_PROTEIN_GROUP_RAW_NAME,
                                    "AssayQuantLayer");
                            break;
                        default:
                            break;
                    }

                    if (inference == null) {
                        return new ProteoSuiteActionResult(
                                new ProteoSuiteException(
                                        "Problem with protein inference object: cannot run inference routine.", true));
                    }

                    inference.proteinInference();
                } catch (final IllegalArgumentException | IOException ex) {
                    ProteoSuiteException pex = null;
                    if (ex instanceof IllegalArgumentException) {
                        pex = new ProteoSuiteException(
                                "Problem with passing correct arguments to normalisation module: "
                                + ex.getLocalizedMessage(), ex, true);
                    } else if (ex instanceof IOException) {
                        if (ex instanceof FileNotFoundException) {
                            pex = new ProteoSuiteException(
                                    "Problem with normalisation module reading mzq file.",
                                    ex, true);
                        } else {
                            pex = new ProteoSuiteException(
                                    "Problem reading path of input file in ProteinInferenceHelper.",
                                    ex, true);
                        }
                    }

                    return new ProteoSuiteActionResult(pex);
                }

                return new ProteoSuiteActionResult<>(outputFile);
            }
        });

        task.addCompletionAction(
                new ProteoSuiteAction<ProteoSuiteActionResult, ProteoSuiteActionSubject>() {
            @Override
            public ProteoSuiteActionResult act(
                    final ProteoSuiteActionSubject argument) {
                AnalyseDynamicTab.getInstance().getAnalyseStatusPanel()
                        .setProteinInferenceDone();

                File outputFile = task.getResultOfClass(File.class);

                if (outputFile == null) {
                    ProteoSuiteException pex = new ProteoSuiteException(
                            "Unable to retrieve File result of protein inference.", true);
                    return new ProteoSuiteActionResult(pex);
                }

                AnovaHelper.anova(outputFile);

                return ProteoSuiteActionResult.emptyResult();
            }
        });

        BackgroundTaskManager.getInstance().submit(task);
    }
}
