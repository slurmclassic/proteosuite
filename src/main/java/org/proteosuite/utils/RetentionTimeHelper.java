package org.proteosuite.utils;

import uk.ac.liv.mzidlib.AddRetentionTimeToMzid;

/**
 *
 * @author SPerkins
 */
public final class RetentionTimeHelper {

    private RetentionTimeHelper() {
    }

    public static String fill(final String rawFile, final String mzidFile) {
        String ftFilledIn = mzidFile.replaceAll(".mzid", "_rt_corrected.mzid");
        AddRetentionTimeToMzid.add(mzidFile, rawFile, mzidFile);
        return ftFilledIn;
    }
}
