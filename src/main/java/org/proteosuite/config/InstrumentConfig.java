package org.proteosuite.config;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.proteosuite.utils.PrimitiveUtils;

/**
 *
 * @author SPerkins
 */
public final class InstrumentConfig {

    private static Map<String, InstrumentConfig> configs = new HashMap<>();
    private String instrumentName;
    private double ms1tol = 10;
    private double ms2tol = 0.5;
    private String ms1tolunit = "ppm";
    private String ms2tolunit = "Da";

    private boolean successfulRead = true;

    private InstrumentConfig(final String filePath) {
        readConfig(filePath);
    }

    /**
     * Gets the name of the instrument.
     * @return The instrument name.
     */
    public String getInstrumentName() {
        return instrumentName;
    }

    /**
     * Gets the MS1 tolerance.
     * @return The MS1 tolerance.
     */
    public double getMs1tol() {
        return ms1tol;
    }

    /**
     * Gets the MS2 tolerance.
     * @return 
     */
    public double getMs2tol() {
        return ms2tol;
    }

    /**
     * Gets the MS1 tolerance unit.
     * @return The MS1 tolerance unit.
     */
    public String getMs1tolunit() {
        return ms1tolunit;
    }

    /**
     * Gets the MS2 tolerance unit.
     * @return The MS2 tolerance unit.
     */
    public String getMs2tolunit() {
        return ms2tolunit;
    }

    /**
     * Creates an instrument config given a file path.
     * @param filePath A file path.
     * @return An instrument config.
     */
    public static InstrumentConfig getConfig(final String filePath) {
        if (configs.containsKey(filePath)) {
            return configs.get(filePath);
        }

        InstrumentConfig config = new InstrumentConfig(filePath);
        if (config.isReadSuccessfully()) {
            configs.put(filePath, config);
        }

        return config;
    }

    /**
     * Checks whether the instrument config was read in correctly.
     * @return Whether the instrument config was read in correctly.
     */
    public boolean isReadSuccessfully() {
        return successfulRead;
    }

    /**
     * Reads an instrument config from file.
     * @param filePath Path to the config file.
     */
    private void readConfig(final String filePath) {
        BufferedReader reader = null;
        try {
            File file = new File(filePath);
            reader = new BufferedReader(new InputStreamReader(
                    new FileInputStream(file), StandardCharsets.UTF_8));
            String line = null;
            while ((line = reader.readLine()) != null) {
                if (line.contains("=")) {
                    String[] split = line.split("=");
                    readKeyValue(split[0], split[1]);
                }
            }

            reader.close();
        } catch (final IOException ex) {
            Logger.getLogger(InstrumentConfig.class.getName()).log(Level.SEVERE,
                    null, ex);
            successfulRead = false;
        }
    }

    /**
     * Reads key-value pairs and places them into the appropriate variables.
     * @param key The key.
     * @param value The value.
     */
    private void readKeyValue(final String key, final String value) {
        switch (key) {
            case "INSTRUMENT_NAME":
                instrumentName = value;
                break;
            case "MS1_TOL":
                if (PrimitiveUtils.isDouble(value)) {
                    ms1tol = Double.parseDouble(value);
                }

                break;
            case "MS2_TOL":
                if (PrimitiveUtils.isDouble(value)) {
                    ms2tol = Double.parseDouble(value);
                }

                break;
            case "MS1_TOL_UNIT":
                ms1tolunit = value;
                break;
            case "MS2_TOL_UNIT":
                ms2tolunit = value;
                break;
            default:
                break;
        }
    }
}
