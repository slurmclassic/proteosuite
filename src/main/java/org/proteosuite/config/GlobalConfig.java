package org.proteosuite.config;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.proteosuite.utils.PrimitiveUtils;

/**
 *
 * @author SPerkins
 */
public final class GlobalConfig {

    private static GlobalConfig INSTANCE = null;
    private String rememberedDirectory = null;
    private boolean instrumentSpecificConfig = false;
//    private final String searchDatabaseFile = "";
//    private final double searchPrecursorMsTolerance = 5;
//    private final String searchPrecursorMsToleranceUnit = "ppm";
//    private final double searchFragmentMsMsTolerance = 0.5;
//    private final char searchFragmentTypesABC = 'b';
//    private final char searchFragmentTypesXYZ = 'y';
//    private final String searchEnzyme = "Trypsin";
//    private final int searchMaxMissedCleavages = 1;
//    private final int searchPrecursorChargeRangeLower = 2;
//    private final int searchPrecursorChargeRangeUpper = 4;
//    private final String searchPeptideLevelThresholding = "0.01 (1%)";
//    private final String searchProteinLevelThresholding = "0.01 (1%)";
//    private final List<String> searchFixedModifications = Collections.singletonList("carbamidomethyl c");
//    private final List<String> searchVariableModifications = Collections.singletonList("oxidation of m");
    private int maxMemorySetting = 512;
    private boolean checkForPreexistingFeatureXML = false;

    private boolean skipInternalAligner = false;

    private double featureFinderMassTraceMzTolerance = 0.03;
    private int featureFinderMassTraceMinSpectra = 5;
    private int featureFinderMassTraceMaxMissing = 2;
    private int featureFinderMassTraceSlopeBound = 1;
    private int featureFinderIsotopicPatternChargeLow = 2;
    private int featureFinderIsotopicPatternChargeHigh = 4;
    private double featureFinderIsotopicPatternMzTolerance = 0.03;
    private double featureFinderSeedMinScore = 0.1;
    private double featureFinderFeatureMinScore = 0.3;
    private double featureFinderFeatureMinIsotopeFit = 0.1;
    private double featureFinderFeatureMinTraceScore = 0.1;
    private double featureFinderFeatureMaxRtSpan = 3;

    private int identAlignerMinRunOccur = 2;
    private double identAlignerMaxRtShift = 200;

    private double clusterAlignerPeptideScoreThreshold = 0;
    private int clusterAlignerMinRunOccur = 2;
    private double clusterAlignerMaxRtShift = 200;
    private boolean clusterAlignerUseUnassignedPeptides = true;
    private boolean clusterAlignerUseFeatureRt = false;
    private boolean featureLinkerUseIdents = true;
    private int featureLinkerDistanceRtMaxDifference = 60;
    private double featureLinkerDistanceMzMaxDifference = 0.02;
//    private Set<String> instruments = new HashSet<>();

    private boolean isDirty = false;

    private boolean debugSkipCleaningIdents = false;
    private boolean debugSkipFeatureFinder = false;
    private boolean debugSkipFeatureAligner = false;
    private boolean debugSkipFeatureLinker = false;
    private boolean debugSkipMzqConverter = false;
    private boolean debugSkipMapper = false;
    private boolean debugSkipNormalisation = false;
    private boolean debugSkipProteinInference = false;
    private boolean debugSkipAnova = false;

    private double precursorToleranceValue;
    private String precursorToleranceUnit;

    private GlobalConfig() {
        if (!readConfig()) {
            writeFile();
        }
    }

    public static GlobalConfig getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new GlobalConfig();
        }

        return INSTANCE;
    }

    private boolean readConfig() {
        try {
            File globalConfigFile = new File("global.pc");
            if (!globalConfigFile.exists()) {
                return false;
            }

            System.out.println(
                    "Looking for ProteoSuite global config file in folder: "
                    + globalConfigFile.getAbsolutePath());
            try (BufferedReader globalReader = new BufferedReader(
                    new InputStreamReader(new FileInputStream(globalConfigFile),
                            StandardCharsets.UTF_8))) {
                String line = null;
                while ((line = globalReader.readLine()) != null) {
                    if (line.contains("=")) {
                        String[] split = line.split("=");
                        if (split.length == 2) {
                            readKeyValue(split[0], split[1]);
                        }
                    }
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(GlobalConfig.class.getName()).log(Level.SEVERE,
                    null, ex);
            return false;
        }

        return true;
    }

    private void readKeyValue(final String key, final String value) {
        switch (key.toUpperCase()) {
            case "REMEMBERED_DIRECTORY":
                rememberedDirectory = value;
                break;
            case "INSTRUMENT_CONFIG":
                instrumentSpecificConfig = Boolean.parseBoolean(value);
                break;
            case "SKIP_INTERNAL_ALIGNER":
                skipInternalAligner = Boolean.parseBoolean(value);
                break;
            case "CHECK_PREEXISTING_FEATURE_XML":
                checkForPreexistingFeatureXML = Boolean.parseBoolean(value);
                break;
            case "FEATURE_FINDER_MASS_TRACE_MZ_TOLERANCE":
                if (PrimitiveUtils.isDouble(value)) {
                    featureFinderMassTraceMzTolerance = Double.
                            parseDouble(value);
                } else {
                    System.out.println(
                            "Could not read FEATURE_FINDER_MASS_TRACE_MZ_TOLERANCE option from config. Using default: "
                            + featureFinderMassTraceMzTolerance);
                    isDirty = true;
                }

                break;
            case "FEATURE_FINDER_MASS_TRACE_MIN_SPECTRA":
                if (PrimitiveUtils.isInteger(value)) {
                    featureFinderMassTraceMinSpectra = Integer.parseInt(value);
                } else {
                    System.out.println(
                            "Could not read FEATURE_FINDER_MASS_TRACE_MIN_SPECTRA option from config. Using default: "
                            + featureFinderMassTraceMinSpectra);
                    isDirty = true;
                }

                break;
            case "FEATURE_FINDER_MASS_TRACE_MAX_MISSING":
                if (PrimitiveUtils.isInteger(value)) {
                    featureFinderMassTraceMaxMissing = Integer.parseInt(value);
                } else {
                    System.out.println(
                            "Could not read FEATURE_FINDER_MASS_TRACE_MAX_MISSING option from config. Using default: "
                            + featureFinderMassTraceMaxMissing);
                    isDirty = true;
                }

                break;
            case "FEATURE_FINDER_MASS_TRACE_SLOPE_BOUND":
                if (PrimitiveUtils.isInteger(value)) {
                    featureFinderMassTraceSlopeBound = Integer.parseInt(value);
                } else {
                    System.out.println(
                            "Could not read FEATURE_FINDER_MASS_TRACE_SLOPE_BOUND option from config. Using default: "
                            + featureFinderMassTraceSlopeBound);
                    isDirty = true;
                }

                break;
            case "FEATURE_FINDER_ISOTOPIC_PATTERN_CHARGE_LOW":
                if (PrimitiveUtils.isInteger(value)) {
                    featureFinderIsotopicPatternChargeLow = Integer.parseInt(
                            value);
                } else {
                    System.out.println(
                            "Could not read FEATURE_FINDER_ISOTOPIC_PATTERN_CHARGE_LOW option from config. Using default: "
                            + featureFinderIsotopicPatternChargeLow);
                    isDirty = true;
                }

                break;
            case "FEATURE_FINDER_ISOTOPIC_PATTERN_CHARGE_HIGH":
                if (PrimitiveUtils.isInteger(value)) {
                    featureFinderIsotopicPatternChargeHigh = Integer.parseInt(
                            value);
                } else {
                    System.out.println(
                            "Could not read FEATURE_FINDER_ISOTOPIC_PATTERN_CHARGE_HIGH option from config. Using default: "
                            + featureFinderIsotopicPatternChargeHigh);
                    isDirty = true;
                }

                break;
            case "FEATURE_FINDER_ISOTOPIC_PATTERN_MZ_TOLERANCE":
                if (PrimitiveUtils.isDouble(value)) {
                    featureFinderIsotopicPatternMzTolerance = Double.
                            parseDouble(value);
                } else {
                    System.out.println(
                            "Could not read FEATURE_FINDER_ISOTOPIC_PATTERN_MZ_TOLERANCE option from config. Using default: "
                            + featureFinderIsotopicPatternMzTolerance);
                    isDirty = true;
                }

                break;
            case "FEATURE_FINDER_SEED_MIN_SCORE":
                if (PrimitiveUtils.isDouble(value)) {
                    featureFinderSeedMinScore = Double.parseDouble(value);
                } else {
                    System.out.println(
                            "Could not read FEATURE_FINDER_SEED_MIN_SCORE option from config. Using default: "
                            + featureFinderSeedMinScore);
                    isDirty = true;
                }

                break;
            case "FEATURE_FINDER_FEATURE_MIN_SCORE":
                if (PrimitiveUtils.isDouble(value)) {
                    featureFinderFeatureMinScore = Double.parseDouble(value);
                } else {
                    System.out.println(
                            "Could not read FEATURE_FINDER_FEATURE_MIN_SCORE option from config. Using default: "
                            + featureFinderFeatureMinScore);
                    isDirty = true;
                }

                break;
            case "FEATURE_FINDER_FEATURE_MIN_ISOTOPE_FIT":
                if (PrimitiveUtils.isDouble(value)) {
                    featureFinderFeatureMinIsotopeFit = Double.
                            parseDouble(value);
                } else {
                    System.out.println(
                            "Could not read FEATURE_FINDER_FEATURE_MIN_ISOTOPE_FIT option from config. Using default: "
                            + featureFinderFeatureMinIsotopeFit);
                    isDirty = true;
                }

                break;
            case "FEATURE_FINDER_FEATURE_MIN_TRACE_SCORE":
                if (PrimitiveUtils.isDouble(value)) {
                    featureFinderFeatureMinTraceScore = Double.
                            parseDouble(value);
                } else {
                    System.out.println(
                            "Could not read FEATURE_FINDER_FEATURE_MIN_TRACE_SCORE option from config. Using default: "
                            + featureFinderFeatureMinTraceScore);
                    isDirty = true;
                }

                break;
            case "FEATURE_FINDER_FEATURE_MAX_RT_SPAN":
                if (PrimitiveUtils.isDouble(value)) {
                    featureFinderFeatureMaxRtSpan = Double.parseDouble(value);
                } else {
                    System.out.println(
                            "Could not read FEATURE_FINDER_FEATURE_MAX_RT_SPAN option from config. Using default: "
                            + featureFinderFeatureMaxRtSpan);
                    isDirty = true;
                }

                break;
            case "IDENT_ALIGNER_MIN_RUNS":
                if (PrimitiveUtils.isInteger(value)) {
                    identAlignerMinRunOccur = Integer.parseInt(value);
                } else {
                    System.out.println(
                            "Could not read IDENT_ALIGNER_MIN_RUNS option from config. Using default: "
                            + identAlignerMinRunOccur);
                    isDirty = true;
                }

                break;
            case "IDENT_ALIGNER_MAX_RT_SHIFT":
                if (PrimitiveUtils.isDouble(value)) {
                    identAlignerMaxRtShift = Double.parseDouble(value);
                } else {
                    System.out.println(
                            "Could not read IDENT_ALIGNER_MAX_RT_SHIFT option from config. Using default: "
                            + identAlignerMaxRtShift);
                    isDirty = true;
                }

                break;
            case "CLUSTER_ALIGNER_PEPTIDE_SCORE_THRESHOLD":
                if (PrimitiveUtils.isDouble(value)) {
                    clusterAlignerPeptideScoreThreshold = Double.parseDouble(
                            value);
                } else {
                    System.out.println(
                            "Could not read CLUSTER_ALIGNER_PEPTIDE_SCORE_THRESHOLD option from config. Using default: "
                            + clusterAlignerPeptideScoreThreshold);
                    isDirty = true;
                }

                break;
            case "CLUSTER_ALIGNER_MIN_RUN_OCCUR":
                if (PrimitiveUtils.isInteger(value)) {
                    clusterAlignerMinRunOccur = Integer.parseInt(value);
                } else {
                    System.out.println(
                            "Could not read CLUSTER_ALIGNER_MIN_RUN_OCCUR option from config. Using default: "
                            + clusterAlignerMinRunOccur);
                    isDirty = true;
                }

                break;
            case "CLUSTER_ALIGNER_MAX_RT_SHIFT":
                if (PrimitiveUtils.isDouble(value)) {
                    clusterAlignerMaxRtShift = Double.parseDouble(value);
                } else {
                    System.out.println(
                            "Could not read CLUSTER_ALIGNER_MAX_RT_SHIFT option from config. Using default: "
                            + clusterAlignerMaxRtShift);
                    isDirty = true;
                }

                break;
            case "CLUSTER_ALIGNER_USE_UNASSIGNED_PEPTIDES":
                clusterAlignerUseUnassignedPeptides = Boolean.
                        parseBoolean(value);
                break;
            case "CLUSTER_ALIGNER_USE_FEATURE_RT":
                clusterAlignerUseFeatureRt = Boolean.parseBoolean(value);
                break;
            case "FEATURE_LINKER_USE_IDENTIFICATIONS":
                featureLinkerUseIdents = Boolean.parseBoolean(value);
                break;
            case "FEATURE_LINKER_DISTANCE_RT_MAX_DIFFERENCE":
                if (PrimitiveUtils.isInteger(value)) {
                    featureLinkerDistanceRtMaxDifference = Integer.parseInt(
                            value);
                } else {
                    System.out.println(
                            "Could not read FEATURE_LINKER_DISTANCE_RT_MAX_DIFFERENCE option from config. Using default: "
                            + featureLinkerDistanceRtMaxDifference);
                    isDirty = true;
                }

                break;
            case "FEATURE_LINKER_DISTANCE_MZ_MAX_DIFFERENCE":
                if (PrimitiveUtils.isDouble(value)) {
                    featureLinkerDistanceMzMaxDifference = Double.parseDouble(
                            value);
                } else {
                    System.out.println(
                            "Could not read FEATURE_LINKER_DISTANCE_MZ_MAX_DIFFERENCE option from config. Using default: "
                            + featureLinkerDistanceMzMaxDifference);
                    isDirty = true;
                }

                break;
            case "MAX_MEMORY":
                if (PrimitiveUtils.isInteger(value)) {
                    maxMemorySetting = Integer.parseInt(value);
                } else {
                    System.out.println(
                            "Could not read MAX_MEMORY option from config. Using default: "
                            + maxMemorySetting);
                }

                break;
            default:
                break;

//            case "INSTRUMENTS":
//                String[] instrumentStrings = value.split(";");
//                instruments.addAll(Arrays.asList(instrumentStrings));
//                break;
        }
    }

    private void writeFile() {
        try {
            File out = new File("global.pc");
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(out), StandardCharsets.UTF_8));
            writer.write("REMEMBERED_DIRECTORY=" + (rememberedDirectory != null
                    ? rememberedDirectory : ""));
            writer.newLine();
            writer.write("INSTRUMENT_CONFIG=" + instrumentSpecificConfig);
            writer.newLine();
            writer.write("SKIP_INTERNAL_ALIGNER=" + skipInternalAligner);
            writer.newLine();
            writer.write("CHECK_PREEXISTING_FEATURE_XML="
                    + checkForPreexistingFeatureXML);
            writer.newLine();
            writer.write("FEATURE_FINDER_MASS_TRACE_MZ_TOLERANCE="
                    + featureFinderMassTraceMzTolerance);
            writer.newLine();
            writer.write("FEATURE_FINDER_MASS_TRACE_MIN_SPECTRA="
                    + featureFinderMassTraceMinSpectra);
            writer.newLine();
            writer.write("FEATURE_FINDER_MASS_TRACE_MAX_MISSING="
                    + featureFinderMassTraceMaxMissing);
            writer.newLine();
            writer.write("FEATURE_FINDER_MASS_TRACE_SLOPE_BOUND="
                    + featureFinderMassTraceSlopeBound);
            writer.newLine();
            writer.write("FEATURE_FINDER_ISOTOPIC_PATTERN_CHARGE_LOW="
                    + featureFinderIsotopicPatternChargeLow);
            writer.newLine();
            writer.write("FEATURE_FINDER_ISOTOPIC_PATTERN_CHARGE_HIGH="
                    + featureFinderIsotopicPatternChargeHigh);
            writer.newLine();
            writer.write("FEATURE_FINDER_ISOTOPIC_PATTERN_MZ_TOLERANCE="
                    + featureFinderIsotopicPatternMzTolerance);
            writer.newLine();
            writer.write("FEATURE_FINDER_SEED_MIN_SCORE="
                    + featureFinderSeedMinScore);
            writer.newLine();
            writer.write("FEATURE_FINDER_FEATURE_MIN_SCORE="
                    + featureFinderFeatureMinScore);
            writer.newLine();
            writer.write("FEATURE_FINDER_FEATURE_MIN_ISOTOPE_FIT="
                    + featureFinderFeatureMinIsotopeFit);
            writer.newLine();
            writer.write("FEATURE_FINDER_FEATURE_MIN_TRACE_SCORE="
                    + featureFinderFeatureMinTraceScore);
            writer.newLine();
            writer.write("FEATURE_FINDER_FEATURE_MAX_RT_SPAN="
                    + featureFinderFeatureMaxRtSpan);
            writer.newLine();

            writer.write("IDENT_ALIGNER_MIN_RUNS=" + identAlignerMinRunOccur);
            writer.newLine();
            writer.write("IDENT_ALIGNER_MAX_RT_SHIFT=" + identAlignerMaxRtShift);
            writer.newLine();

            writer.write("CLUSTER_ALIGNER_PEPTIDE_SCORE_THRESHOLD="
                    + clusterAlignerPeptideScoreThreshold);
            writer.newLine();
            writer.write("CLUSTER_ALIGNER_MIN_RUN_OCCUR="
                    + clusterAlignerMinRunOccur);
            writer.newLine();
            writer.write("CLUSTER_ALIGNER_MAX_RT_SHIFT="
                    + clusterAlignerMaxRtShift);
            writer.newLine();
            writer.write("CLUSTER_ALIGNER_USE_UNASSIGNED_PEPTIDES="
                    + clusterAlignerUseUnassignedPeptides);
            writer.newLine();
            writer.write("CLUSTER_ALIGNER_USE_FEATURE_RT="
                    + clusterAlignerUseFeatureRt);
            writer.newLine();
            writer.write("FEATURE_LINKER_USE_IDENTIFICATIONS="
                    + featureLinkerUseIdents);
            writer.newLine();
            writer.write("FEATURE_LINKER_DISTANCE_RT_MAX_DIFFERENCE="
                    + featureLinkerDistanceRtMaxDifference);
            writer.newLine();
            writer.write("FEATURE_LINKER_DISTANCE_MZ_MAX_DIFFERENCE="
                    + featureLinkerDistanceMzMaxDifference);
            writer.newLine();

            writer.write("MAX_MEMORY=" + maxMemorySetting);
            writer.newLine();

            writer.close();

        } catch (IOException ex) {
            System.out.println(
                    "Error: Problem writing global configuration file.");
            Logger.getLogger(GlobalConfig.class.getName()).log(Level.SEVERE,
                    null, ex);
        }
    }

    public void saveConfig() {
        if (!isDirty) {
            return;
        }

        System.out.println("Global config has changed - saving!");
        writeFile();
    }

    public boolean isRequestingSkipInternalAlignment() {
        return this.skipInternalAligner;
    }

    public boolean isRequestingCheckForPreexistingFeatureXML() {
        return this.checkForPreexistingFeatureXML;
    }

    public boolean isRequestingDebugSkipCleaningIdentifications() {
        return this.debugSkipCleaningIdents;
    }

    public boolean isRequestingDebugSkipFeatureFinder() {
        return this.debugSkipFeatureFinder;
    }

    public boolean isRequestingDebugSkipFeatureAligner() {
        return this.debugSkipFeatureAligner;
    }

    public boolean isRequestingDebugSkipFeatureLinker() {
        return this.debugSkipFeatureLinker;
    }

    public boolean isRequestingDebugSkipMzqConverter() {
        return this.debugSkipMzqConverter;
    }

    public boolean isRequestingDebugSkipMapper() {
        return this.debugSkipMapper;
    }

    public boolean isRequestionDebugSkipNormalisation() {
        return this.debugSkipNormalisation;
    }

    public boolean isRequestionDebugSkipProteinInference() {
        return this.debugSkipProteinInference;
    }

    public boolean isRequestionDebugSkipAnova() {
        return this.debugSkipAnova;
    }

    public double getFeatureFinderMassTraceMzTolerance() {
        return this.featureFinderMassTraceMzTolerance;
    }

    public int getFeatureFinderMassTraceMinSpectra() {
        return this.featureFinderMassTraceMinSpectra;
    }

    public int getFeatureFinderMassTraceMaxMissing() {
        return this.featureFinderMassTraceMaxMissing;
    }

    public int getFeatureFinderMassTraceSlopeBound() {
        return this.featureFinderMassTraceSlopeBound;
    }

    public int getFeatureFinderIsotopicPatternChargeLow() {
        return this.featureFinderIsotopicPatternChargeLow;
    }

    public int getFeatureFinderIsotopicPatternChargeHigh() {
        return this.featureFinderIsotopicPatternChargeHigh;
    }

    public double getFeatureFinderIsotopicPatternMzTolerance() {
        return this.featureFinderIsotopicPatternMzTolerance;
    }

    public double getFeatureFinderSeedMinScore() {
        return this.featureFinderSeedMinScore;
    }

    public double getFeatureFinderFeatureMinScore() {
        return this.featureFinderFeatureMinScore;
    }

    public double getFeatureFinderFeatureMinIsotopeFit() {
        return this.featureFinderFeatureMinIsotopeFit;
    }

    public double getFeatureFinderFeatureMinTraceScore() {
        return this.featureFinderFeatureMinTraceScore;
    }

    public double getFeatureFinderFeatureMaxRtSpan() {
        return this.featureFinderFeatureMaxRtSpan;
    }

    public int getIdentAlignerMinRuns() {
        return this.identAlignerMinRunOccur;
    }

    public double getIdentAlignerMaxRtShift() {
        return this.identAlignerMaxRtShift;
    }

    public double getClusterAlignerPeptideScoreThreshold() {
        return this.clusterAlignerPeptideScoreThreshold;
    }

    public int getClusterAlignerMinRunOccur() {
        return this.clusterAlignerMinRunOccur;
    }

    public double getClusterAlignerMaxRtShift() {
        return this.clusterAlignerMaxRtShift;
    }

    public boolean getClusterAlignerUseUnassignedPeptides() {
        return this.clusterAlignerUseUnassignedPeptides;
    }

    public boolean getClusterAlignerUseFeatureRt() {
        return this.clusterAlignerUseFeatureRt;
    }

    public boolean getFeatureLinkerUseIdentifications() {
        return this.featureLinkerUseIdents;
    }

    public int getFeatureLinkerDistanceRtMaxDifference() {
        return this.featureLinkerDistanceRtMaxDifference;
    }

    public double getFeatureLinkerDistanceMzMaxDifference() {
        return this.featureLinkerDistanceMzMaxDifference;
    }

    public String getRememberedDirectory() {
        return rememberedDirectory;
    }

    public void setRememberedDirectory(final String dir) {
        this.rememberedDirectory = dir;
        this.isDirty = true;
    }

    public void setMaxMemory(final int maxMemory) {
        this.maxMemorySetting = maxMemory;
        this.isDirty = true;
    }

    public int getMaxMemory() {
        return this.maxMemorySetting;
    }

//    public Set<String> getInstrumentNames() {
//        return instruments;
//    }
    public boolean allowsInstrumentConfig() {
        return instrumentSpecificConfig;
    }

    public void setPrecursorToleranceValue(final double toleranceValue) {
        this.precursorToleranceValue = toleranceValue;
    }

    public void setPrecursorToleranceUnit(final String toleranceUnit) {
        this.precursorToleranceUnit = toleranceUnit;
    }

    public double getPrecursorToleranceValue() {
        return this.precursorToleranceValue;
    }

    public String getPrecursorToleranceUnit() {
        return this.precursorToleranceUnit;
    }
}
