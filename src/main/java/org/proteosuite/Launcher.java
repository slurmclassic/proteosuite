package org.proteosuite;

import java.awt.EventQueue;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.UnsupportedLookAndFeelException;
import org.proteosuite.actions.ProteoSuiteAction;
import org.proteosuite.actions.TaskPostCompleteAction;
import org.proteosuite.config.GlobalConfig;
import org.proteosuite.gui.IdentParamsView;
import org.proteosuite.gui.ProteoSuite;
import org.proteosuite.model.BackgroundTask;
import org.proteosuite.model.BackgroundTaskManager;
import org.proteosuite.model.ProteoSuiteActionResult;
import org.proteosuite.model.ProteoSuiteActionSubject;
import org.proteosuite.quantitation.OpenMSLabelFreeWrapper;
import org.proteosuite.utils.ExceptionCatcher;
import org.proteosuite.utils.OpenURL;
import org.proteosuite.utils.SystemUtils;
import static org.proteosuite.utils.SystemUtils.spawnNewProteosuite;
import org.proteosuite.utils.UpdateCheck;
import org.slf4j.LoggerFactory;

/**
 * Class that does some start up configuration and then launches the Proteosuite
 * window.
 *
 * @author SPerkins
 */
public final class Launcher {

    /**
     * The global config variable.
     */
    private static GlobalConfig config = GlobalConfig.getInstance();

    /**
     * The variable for accessing system utility methods.
     */
    private static SystemUtils sys = new SystemUtils();

    /**
     * Private constructor to prevent instantiation.
     */
    private Launcher() {
    }

    /**
     * Main entry point to application.
     *
     * @param args the command line arguments (leave empty)
     * @throws java.lang.InterruptedException Thrown if there is an error shutting down the application.
     */
    public static void main(final String[] args) throws InterruptedException {
        // Setting standard look and feel
        setLookAndFeel();
        installCheckOpenMS();

        checkMemory();

        // Pre-load the searchGUI modifications.
        IdentParamsView.readInPossibleMods();

        BackgroundTaskManager.getInstance().setTasksRefreshAction(
                new TaskPostCompleteAction());

        BackgroundTask checkVersion = new UpdateWorker();
        BackgroundTaskManager.getInstance().submit(checkVersion);

        // Create and display the form
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {
                    new ProteoSuite().setVisible(true);
                } catch (Exception e) {
                    handleException(new ProteoSuiteException(
                            "ProteoSuite error.", e, true));
                }
            }
        });

        // Exit the application when the above lock has expired.
        ShutdownManager.getInstance().getShutdownLatch().await();
        System.exit(0);
    }

    /**
     * Handles the provided exception.
     * @param pex The exception.
     */
    public static synchronized void handleException(
            final ProteoSuiteException pex) {
        ExceptionCatcher.reportException(pex);
        int result = JOptionPane.showConfirmDialog(null,
                "ProteoSuite has encountered a fatal problem: " + pex.
                getLocalizedMessage() + "\n"
                + "You can close the software now, in which case any work currently being done may be lost, or you"
                + " can allow tasks to complete (if possible) and close it later.\n"
                + "Do you wish to close the software immediately?",
                "He's dead, Jim!", JOptionPane.YES_NO_OPTION);

        if (result == JOptionPane.YES_OPTION) {
           ShutdownManager.getInstance().allowShutdown();
        }
    }

    /**
     * Checks whether the currently allocated memory is acceptable.
     * If not allocated memory is increased in a fresh Proteosuite instance.
     */
    private static void checkMemory() {
        int requiredMemory = config.getMaxMemory();
        int actualMemory = (int) sys.getMaxMemory();
        System.out.println(actualMemory);

        if (actualMemory < requiredMemory) {
            spawnNewProteosuite((int) (requiredMemory * 1.25));
        }

        if (actualMemory < 4096) {
            JOptionPane
                    .showConfirmDialog(
                            null,
                            "Proteosuite is currently configured to use less than 4GB of system memory.\n"
                            + "We recommend using more than this.\n"
                            + "The more memory you are able to allocate to Proteosuite, the less instability and errors may occur.\n"
                            + "You may use the \"Memory\" menu to see the current memory allocation, and choose a new memory allocation.",
                            "Low Memory Setting Detected",
                            JOptionPane.PLAIN_MESSAGE,
                            JOptionPane.INFORMATION_MESSAGE);
        }
    }

    /**
     * Checks whether openMS is installed.
     * If not the message is displayed indicating that label-free capability will be disabled.
     * Threads are also reallocated for non-openMS tasks.
     */
    private static void installCheckOpenMS() {
        String openMSUrl = "http://open-ms.sourceforge.net";

        if (!OpenMSLabelFreeWrapper.checkIsInstalled()) {
            int result = JOptionPane
                    .showConfirmDialog(
                            null,
                            "You do not appear to have openMS installed.\n"
                            + "You need to install openMS in order to use the label-free quantitation feature.\n"
                            + "openMS features will be disabled for now.\n"
                            + "OpenMS is available at:\n" + openMSUrl
                            + "\nTo install now, click \"Yes\" to be directed to the openMS web site.\n"
                            + "Once installed you will need to restart Proteosuite to use openMS features.",
                            "openMS Not Installed!", JOptionPane.YES_NO_OPTION);

            BackgroundTaskManager.getInstance().
                    freeMoreThreadsForGenericExecution();
            if (result == JOptionPane.YES_OPTION) {
                OpenURL.open(openMSUrl);
                return;
            }
        }

    }

    /**
     * Sets the look-and-feel of the application to 'Nimbus'.
     */
    private static void setLookAndFeel() {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());

            for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException exception) {
            LoggerFactory.getLogger(Launcher.class).error("Error setting look and feel.", exception);
        }
    }

    /**
     * Class facilitating the checking of an available update to Proteosuite.
     */
    private static class UpdateWorker extends BackgroundTask<ProteoSuiteActionSubject> {

        /**
         * Creates an instance of the UpdateWorker class.
         */
        UpdateWorker() {
            super(new ProteoSuiteActionSubject() {
                @Override
                public final String getSubjectName() {
                    return "ProteoSuite";
                }
            }, "Checking For Update");

            super.addAsynchronousProcessingAction(
                    new ProteoSuiteAction<ProteoSuiteActionResult, ProteoSuiteActionSubject>() {
                @Override
                public ProteoSuiteActionResult act(
                        final ProteoSuiteActionSubject argument) {
                    try {
                        String updateCheckString = UpdateCheck.hasUpdate(
                                ProteoSuite.PROTEOSUITE_VERSION);
                        ProteoSuiteActionResult<String> result
                                = new ProteoSuiteActionResult(updateCheckString,
                                        null);
                        return result;
                    } catch (final IOException ex) {
                        Logger.getLogger(Launcher.class.getName()).log(
                                Level.SEVERE, null, ex);
                        ProteoSuiteException exception
                                = new ProteoSuiteException(
                                        "Error checking for new version.", ex, false);
                        return new ProteoSuiteActionResult(exception);
                    }
                }
            });

            super.addCompletionAction(
                    new ProteoSuiteAction<ProteoSuiteActionResult, ProteoSuiteActionSubject>() {
                @Override
                public ProteoSuiteActionResult act(final
                        ProteoSuiteActionSubject subject) {
                    String newVersion = UpdateWorker.super.getResultOfClass(
                            String.class);
                    if (newVersion != null) {
                        int result = JOptionPane
                                .showConfirmDialog(
                                        null,
                                        "There is a new version of ProteoSuite available\n Click OK to visit the download page.",
                                        "Information",
                                        JOptionPane.OK_CANCEL_OPTION,
                                        JOptionPane.INFORMATION_MESSAGE);

                        if (result == JOptionPane.OK_OPTION) {
                            OpenURL.open(newVersion);
                        }
                    }

                    return ProteoSuiteActionResult.emptyResult();
                }
            });
        }
    }
}
