package org.proteosuite.gui.chart;

import com.compomics.util.gui.spectrum.ChromatogramPanel;
import java.awt.Dimension;
import java.util.HashMap;
import java.util.Map;
import uk.ac.ebi.jmzml.model.mzml.BinaryDataArray;
import uk.ac.ebi.jmzml.model.mzml.CVParam;
import uk.ac.ebi.jmzml.model.mzml.Chromatogram;
import uk.ac.ebi.jmzml.xml.io.MzMLObjectIterator;
import uk.ac.ebi.jmzml.xml.io.MzMLUnmarshaller;

/**
 *
 * @author SPerkins
 */
public class ChartChromatogram extends AbstractChart {

    private static final Map<MzMLUnmarshaller, ChromatogramPanel> CHROMATOGRAM_CACHE
            = new HashMap<>();

    /**
     * Display total ion chromatogram
     *
     * @param unmarshaller The unmarshaller from which to create the
     * chromatogram.
     *
     * @return The created chromatogram as a panel.
     */
    public static final ChromatogramPanel getChromatogram(
            final MzMLUnmarshaller unmarshaller) {
        if (CHROMATOGRAM_CACHE.containsKey(unmarshaller)) {
            return CHROMATOGRAM_CACHE.get(unmarshaller);
        }

        ChromatogramPanel panel = null;

        MzMLObjectIterator<Chromatogram> iterator = unmarshaller
                .unmarshalCollectionFromXpath(
                        "/run/chromatogramList/chromatogram",
                        Chromatogram.class);
        while (iterator.hasNext()) {
            Chromatogram chrom = iterator.next();

            // Check if mzML contains MS1 data
            if (chrom.getId().toUpperCase().equals("TIC")) {
                panel = createChromatogramPanel(chrom);
                break;
            }

            for (CVParam param : chrom.getCvParam()) {
                if (param.getName().toUpperCase()
                        .equals("TOTAL ION CURRENT CHROMATOGRAM")) {
                    panel = createChromatogramPanel(chrom);
                    break;
                }
            }
        }

        CHROMATOGRAM_CACHE.put(unmarshaller, panel);
        return panel;
    }

    private static ChromatogramPanel createChromatogramPanel(
            final Chromatogram chromatogram) {
        double[] retentionTimes = null;
        double[] intensities = null;

        for (BinaryDataArray binaryData : chromatogram.getBinaryDataArrayList()
                .getBinaryDataArray()) {
            if (retentionTimes == null || retentionTimes.length == 0) {
                retentionTimes = getDouble(getRetentionTimes(binaryData));
            }

            if (intensities == null || intensities.length == 0) {
                intensities = getDouble(getIntensity(binaryData));
            }
        }

        // Class chromatogram from compomics.org
        ChromatogramPanel chromatogramPanel = new ChromatogramPanel(
                retentionTimes,
                intensities, "RT (mins)", "Intensity (counts)");
        chromatogramPanel.setSize(new Dimension(600, 400));
        chromatogramPanel.setPreferredSize(new Dimension(600, 400));

        return chromatogramPanel;
    }
}
