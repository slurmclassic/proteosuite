package org.proteosuite.gui.chart;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import uk.ac.ebi.jmzml.model.mzml.BinaryDataArray;
import uk.ac.ebi.jmzml.model.mzml.CVParam;

public class AbstractChart {

    private static final String SCAN_START_TIME = "MS:1000016";
    private static final String TIME_MINUTE = "UO:0000031";
    private static final String MS_LEVEL = "MS:1000511";
    private static final String MZ = "MS:1000514";
    private static final String ITENSITY = "MS:1000515";
    private static final String RETENTION_TIME = "MS:1000595";

    /**
     * Protected constructor to prevent instantiation.
     */
    protected AbstractChart() { }

    protected static float getRetentionTime(final List<CVParam> cvParams) {
        // Refactored this method to use Streams instead of a loop.
        // It still suffers from the same lack of error handling - what if the value is not floatable?
        Optional<CVParam> startTimeParamOptional = cvParams.stream().filter(
                p -> p.getAccession().equals(SCAN_START_TIME)
                        && p.getUnitAccession().trim().equals(TIME_MINUTE)
        ).findFirst();

        if (startTimeParamOptional.isPresent()) {
            return Float.parseFloat(startTimeParamOptional.get().getValue().trim());
        } else {
            return 0f;
        }
    }

    protected static byte getMSLevel(final List<CVParam> cvParams) {
        Optional<CVParam> msLevelParamOptional = cvParams.stream().filter(p -> p.getAccession().equals(MS_LEVEL)).findFirst();
        if (msLevelParamOptional.isPresent()) {
            return Byte.parseByte(msLevelParamOptional.get().getValue().trim());
        } else {
            return -1;
        }
    }

    protected static Number[] getMz(final BinaryDataArray binaryData) {
        return get(binaryData, MZ);
    }

    protected static Number[] getIntensity(final BinaryDataArray binaryData) {
        return get(binaryData, ITENSITY);
    }

    protected static Number[] getRetentionTimes(final BinaryDataArray binaryData) {
        return get(binaryData, RETENTION_TIME);
    }

    protected static Number[] get(final BinaryDataArray binaryData,
            final String cvParam) {
        Optional<CVParam> paramOptional = binaryData.getCvParam().stream().filter(p -> p.getAccession().equals(cvParam)).findFirst();
        if (paramOptional.isPresent()) {
            return binaryData.getBinaryDataAsNumberArray();
        } else {
            return new Number[0];
        }
    }

    protected static double[] getDouble(final Number[] numbers) {
        if (numbers == null) {
            return null;
        }

        return Stream.of(numbers).mapToDouble(Number::doubleValue).toArray();
    }

    protected static float[] getFloat(final Number[] numbers) {
        if (numbers == null) {
            return null;
        }

        float[] floats = new float[numbers.length];

        for (int i = 0; i < numbers.length; i++) {
            floats[i] = numbers[i].floatValue();
        }

        return floats;
    }
}
