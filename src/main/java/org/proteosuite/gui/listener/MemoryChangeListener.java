package org.proteosuite.gui.listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import org.proteosuite.config.GlobalConfig;
import static org.proteosuite.utils.SystemUtils.spawnNewProteosuite;

public final class MemoryChangeListener implements ActionListener {

    private static GlobalConfig config = GlobalConfig.getInstance();
    private final short memory;

    public MemoryChangeListener(final short memory) {
        this.memory = memory;
    }

    @Override
    public void actionPerformed(final ActionEvent event) {
        config.setMaxMemory(memory * 1024);
        config.saveConfig();
        if (!showWarning()) {
            return;
        }

        spawnNewProteosuite(memory * 1024);
    }

    private boolean showWarning() {
        int returnValue = JOptionPane
                .showConfirmDialog(
                        null,
                        "Modifying memory will require ProteoSuite to restart and all unsaved data maybe lost. \n"
                        + "Ensure the value selected is not greater than the available system memory!",
                        "Are you sure?", JOptionPane.OK_CANCEL_OPTION,
                        JOptionPane.WARNING_MESSAGE);

        if (returnValue == JOptionPane.OK_OPTION) {
            return true;
        }

        return false;
    }

}
