package org.proteosuite.gui.listener;

import java.awt.Component;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import org.proteosuite.gui.analyse.AbstractThresholdingPopup;
import org.proteosuite.gui.analyse.AnalyseDynamicTab;
import org.proteosuite.gui.analyse.CleanIdentificationsStep;
import org.proteosuite.gui.tables.CleanIdentificationsTable;
import org.proteosuite.identification.AbstractThresholdWrapper;
import org.proteosuite.model.AnalyseData;
import org.proteosuite.model.IdentDataFile;

/**
 *
 * @author SPerkins
 */
public abstract class AbstractPerformThresholdingForSelectedListener implements ActionListener {
    private final CleanIdentificationsStep step;
    private final AnalyseData data = AnalyseData.getInstance();
    public AbstractPerformThresholdingForSelectedListener(final CleanIdentificationsStep step) {
        this.step = step;
    }

    @Override
    public final void actionPerformed(final ActionEvent e) {
        Map<Entry<String, String>, Integer> thresholdables = new HashMap<>();
        CleanIdentificationsTable table = step.getCleanIdentificationsTable();

        int[] selectedRows = table.getSelectedRows();
        Set<IdentDataFile> identDataFiles = new HashSet<>();
        for (int selectedRow : selectedRows) {
            String fileName = (String) table.getModel().getValueAt(selectedRow,
                    0);
            for (int i = 0; i < data.getRawDataCount(); i++) {
                IdentDataFile identDataFile = data.getRawDataFile(i).
                        getIdentificationDataFile();
                identDataFiles.add(identDataFile);
                if (identDataFile != null && identDataFile.getFileName().equals(
                        fileName)) {
                    Map<String, String> thresholdablesLocal = identDataFile.
                            getThresholdables();
                    for (Entry<String, String> entry : thresholdablesLocal.
                            entrySet()) {
                        Optional<Entry<Entry<String, String>, Integer>> optional
                                = thresholdables.entrySet().stream().filter(p
                                        -> p.getKey().getKey().equals(entry.
                                                getKey()) && p.getKey().
                                        getValue().equals(entry.getValue())).
                                findAny();
                        if (optional.isPresent()) {
                            Entry<Entry<String, String>, Integer> entryToIncrement
                                    = optional.get();
                            entryToIncrement.setValue(entryToIncrement.
                                    getValue() + 1);
                        } else {
                            thresholdables.put(entry, 1);
                        }
                    }

                    break;
                }
            }
        }

        List<Entry<String, String>> usableThresholdables = thresholdables.
                entrySet().stream().filter(p -> p.getValue() == identDataFiles.
                        size()).map(r -> r.getKey()).
                collect(Collectors.toList());

        if (usableThresholdables.isEmpty()) {
            JOptionPane
                    .showConfirmDialog(
                            step,
                            "The files you have chosen to threshold together do not share a common thresholding term.\n"
                            + "The chosen files must share at least 1 thresholding term in order to be thresholded by the same term.\n"
                            + "Please choose a different selection of files to threshold.\n"
                            + "You may also choose to threshold files one at a a time, individually.\n",
                            "Invalid Selection For Thresholding",
                            JOptionPane.PLAIN_MESSAGE,
                            JOptionPane.ERROR_MESSAGE);
            return;
        }

        Map<String, String> usableThresholdablesMap = new HashMap<>();
        for (Entry<String, String> entry : usableThresholdables) {
            usableThresholdablesMap.put(entry.getKey(), entry.getValue());
        }

        Component parent = step.getParent();
        while (!(parent instanceof JFrame)) {
            parent = parent.getParent();
        }

        AbstractThresholdingPopup popup = getThresholdingPopup((Window) parent, usableThresholdablesMap);
        popup.setVisible(true);

        if (popup.hasBeenClosed()) {
            return;
        }

        AnalyseDynamicTab.getInstance().getAnalyseStatusPanel().
                setCleanIdentificationsProcessing();

        for (IdentDataFile identFile : identDataFiles) {
            if (identFile == null || !identFile.isCleanable() || identFile.
                    getThresholdStatus().equals("Thresholding...") || identFile.
                    getPSMCountPassingThreshold() == -1) {
                // Issue with this file!
                continue;
            }

            AbstractThresholdWrapper wrapper = getWrapperFromPopupContent(popup, identFile);
            wrapper.doThresholding();
        }
    }

    protected abstract AbstractThresholdingPopup getThresholdingPopup(final Window owner, final Map<String, String> usableThresholdablesMap);
    protected abstract AbstractThresholdWrapper getWrapperFromPopupContent(AbstractThresholdingPopup popup, IdentDataFile identFile);
}
