package org.proteosuite.gui.listener;

import java.awt.Window;
import java.util.Map;
import org.proteosuite.gui.analyse.AbstractThresholdingPopup;
import org.proteosuite.gui.analyse.CleanIdentificationsStep;
import org.proteosuite.gui.analyse.ThresholdingPopup;
import org.proteosuite.identification.AbstractThresholdWrapper;
import org.proteosuite.identification.ThresholdWrapper;
import org.proteosuite.model.IdentDataFile;

/**
 *
 * @author SPerkins
 */
public final class PerformThresholdingForSelectedListener extends AbstractPerformThresholdingForSelectedListener {
    public PerformThresholdingForSelectedListener(
            final CleanIdentificationsStep step) {
        super(step);
    }

    @Override
    protected AbstractThresholdingPopup getThresholdingPopup(final Window owner, final Map<String, String> usableThresholdablesMap) {
        return new ThresholdingPopup(owner, usableThresholdablesMap);
    }

    @Override
    protected AbstractThresholdWrapper getWrapperFromPopupContent(final AbstractThresholdingPopup popup, final IdentDataFile identFile) {
        ThresholdingPopup concretePopup = (ThresholdingPopup) popup;
        String[] thresholdToPerform = concretePopup.getThresholdingChosen();

        return new ThresholdWrapper(identFile,
                    thresholdToPerform[0], Double.valueOf(thresholdToPerform[2]),
                    thresholdToPerform[1].equals(">"));
    }
}
