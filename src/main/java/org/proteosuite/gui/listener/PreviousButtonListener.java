package org.proteosuite.gui.listener;

import javax.swing.JPanel;
import org.proteosuite.gui.analyse.AnalyseDynamicTab;
import org.proteosuite.gui.analyse.AnalyseStatusPanel;
import org.proteosuite.gui.analyse.DefineConditionsStep;
import org.proteosuite.gui.analyse.QuantitationStep;
import org.proteosuite.gui.tables.DefineConditionsTable;

/**
 *
 * @author SPerkins
 */
public final class PreviousButtonListener extends AbstractMoveButtonListener {

    public PreviousButtonListener(final JPanel panel) {
        super(panel);
    }

    @Override
    protected void moveFromRawDataAndMultiplexingStep() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void moveFromDefineConditionsStep() {
        DefineConditionsTable conditionsTable
                = ((DefineConditionsStep) panel).getConditionsTable();

    super.readConditionsTableIntoModel(conditionsTable);

        parent.moveToStep(AnalyseDynamicTab.RAW_DATA_AND_MULTIPLEXING_STEP);
        AnalyseStatusPanel.getInstance().setRawDataAsCurrentStep();
    }

    @Override
    protected void moveFromIdentificationsStep() {
        if (!DATA.doingGenomeAnnotation()) {
            parent.moveToStep(AnalyseDynamicTab.DEFINE_CONDITIONS_STEP);
            AnalyseStatusPanel.getInstance().setConditionsAsCurrentStep();
        } else {
            parent.moveToStep(
                    AnalyseDynamicTab.RAW_DATA_AND_MULTIPLEXING_STEP);
            AnalyseStatusPanel.getInstance().setRawDataAsCurrentStep();
        }
    }

    @Override
    protected void moveFromCleanIdentificationsStep() {
        parent.moveToStep(
                AnalyseDynamicTab.CREATE_OR_LOAD_IDENTIFICATIONS_STEP);
        AnalyseStatusPanel.getInstance().setIdentificationsAsCurrentStep();
    }

    @Override
    protected void moveFromQuantitationStep() {
        if ("TMT".equals(((QuantitationStep) panel).getQuantitationTypeName().toUpperCase())) {
            // Experiment is TMT. For now, do nothing.
            return;
        }

        parent.moveToStep(
                AnalyseDynamicTab.CREATE_OR_LOAD_IDENTIFICATIONS_STEP);
        AnalyseStatusPanel.getInstance().setIdentificationsAsCurrentStep();
    }
}
