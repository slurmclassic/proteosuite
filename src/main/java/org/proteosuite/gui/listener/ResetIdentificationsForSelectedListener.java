package org.proteosuite.gui.listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.proteosuite.gui.analyse.AnalyseDynamicTab;
import org.proteosuite.gui.analyse.CreateOrLoadIdentificationsStep;
import org.proteosuite.gui.inspect.InspectTab;
import org.proteosuite.gui.tables.CreateOrLoadIdentificationsTable;
import org.proteosuite.model.AnalyseData;
import org.proteosuite.model.RawDataFile;

/**
 *
 * @author SPerkins
 */
public class ResetIdentificationsForSelectedListener implements ActionListener {

    private static AnalyseData data = AnalyseData.getInstance();
    private CreateOrLoadIdentificationsStep step;

    public ResetIdentificationsForSelectedListener(
            final CreateOrLoadIdentificationsStep step) {
        this.step = step;
    }

    @Override
    public final void actionPerformed(final ActionEvent e) {
        CreateOrLoadIdentificationsTable identificationsTable = step.
                getIdentificationsTable();
        int[] selectedRawFiles = identificationsTable.getSelectedRows();
        if (selectedRawFiles.length == 0) {
            return;
        }

        for (int selectedRow : selectedRawFiles) {
            RawDataFile dataFile = data.getRawDataFile(selectedRow);
            dataFile.setIdentStatus("<None>");
            if (dataFile.getIdentificationDataFile() != null) {
                data.getInspectModel().removeIdentDataFile(dataFile.
                        getIdentificationDataFile());
            }

            dataFile.setIdentificationDataFile(null);
        }

        InspectTab.getInstance().refreshComboBox();
        step.refreshFromData();
        AnalyseDynamicTab.getInstance().getAnalyseStatusPanel().
                setIdentificationsNotDone();
    }
}
