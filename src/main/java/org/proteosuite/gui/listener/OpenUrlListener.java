package org.proteosuite.gui.listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import org.proteosuite.utils.OpenURL;

public final class OpenUrlListener implements ActionListener {

    private final String url;

    public OpenUrlListener(final String url) {
        this.url = url;
    }

    @Override
    public void actionPerformed(final ActionEvent actionEvent) {
        OpenURL.open(url);
    }
}
