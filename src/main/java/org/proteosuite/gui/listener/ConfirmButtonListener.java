package org.proteosuite.gui.listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JPanel;
import org.proteosuite.gui.analyse.AnalyseDynamicTab;
import org.proteosuite.gui.analyse.QuantitationStep;
import org.proteosuite.model.AnalyseData;
import org.proteosuite.model.RawDataFile;
import org.proteosuite.quantitation.OpenMSLabelFreeWrapper;
import org.proteosuite.quantitation.XTrackerITRAQWrapper;
import org.proteosuite.quantitation.XTrackerTMTWrapper;

/**
 *
 * @author SPerkins
 */
public class ConfirmButtonListener implements ActionListener {

    private JPanel panel;

    public ConfirmButtonListener(final JPanel panel) {
        this.panel = panel;
    }

    @Override
    public final void actionPerformed(final ActionEvent event) {
        AnalyseDynamicTab parent = (AnalyseDynamicTab) panel.getParent();

        List<RawDataFile> dataFiles = new ArrayList<>();
        for (int i = 0; i < AnalyseData.getInstance().getRawDataCount(); i++) {
            dataFiles.add(AnalyseData.getInstance().getRawDataFile(i));
        }

        AnalyseDynamicTab.getInstance().getAnalyseStatusPanel().
                setQuantitationProcessing();

        if (!(panel instanceof QuantitationStep)) {
            return;
        }

        switch (((QuantitationStep) panel).getQuantitationTypeName().toUpperCase()) {
            case "LABEL-FREE":
                OpenMSLabelFreeWrapper labelFree = new OpenMSLabelFreeWrapper(
                    dataFiles);
                labelFree.compute();
                break;
            case "ITRAQ":
                XTrackerITRAQWrapper itraq = new XTrackerITRAQWrapper(dataFiles);
                itraq.compute();
                break;
            case "TMT":
                XTrackerTMTWrapper tmt = new XTrackerTMTWrapper(dataFiles);
                tmt.compute();
                break;
            default:
                break;
        }

        parent.moveToStep(AnalyseDynamicTab.DONE_STEP);
    }
}
