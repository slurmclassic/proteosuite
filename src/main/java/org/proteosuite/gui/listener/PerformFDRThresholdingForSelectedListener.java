package org.proteosuite.gui.listener;

import java.awt.Window;
import java.util.Map;
import org.proteosuite.gui.analyse.AbstractThresholdingPopup;
import org.proteosuite.gui.analyse.CleanIdentificationsStep;
import org.proteosuite.gui.analyse.FalseDiscoveryRateThresholdingPopup;
import org.proteosuite.identification.AbstractThresholdWrapper;
import org.proteosuite.identification.FDRThresholdWrapper;
import org.proteosuite.model.IdentDataFile;

/**
 *
 * @author SPerkins
 */
public final class PerformFDRThresholdingForSelectedListener extends AbstractPerformThresholdingForSelectedListener {
    public PerformFDRThresholdingForSelectedListener(
            final CleanIdentificationsStep step) {
        super(step);
    }

    @Override
    protected AbstractThresholdingPopup getThresholdingPopup(final Window owner, final Map<String, String> usableThresholdablesMap) {
        return new FalseDiscoveryRateThresholdingPopup(owner, usableThresholdablesMap);
    }

    @Override
    protected AbstractThresholdWrapper getWrapperFromPopupContent(final AbstractThresholdingPopup popup, final IdentDataFile identFile) {
        FalseDiscoveryRateThresholdingPopup concretePopup = (FalseDiscoveryRateThresholdingPopup) popup;
        String thresholdTerm = concretePopup.getThresholdTermChosen();
        double fdrThresholdValue = concretePopup.getFDRThresholdValueChosen();
        boolean lowerValuesBetter = concretePopup.areLowerValuesBetter();
        String fdrLevel = concretePopup.getFDRLevel();
        String decoyHitTag = concretePopup.getDecoyHitTag();

        return new FDRThresholdWrapper(identFile,
                    thresholdTerm, fdrThresholdValue, lowerValuesBetter,
                    fdrLevel, decoyHitTag);
    }
}
