package org.proteosuite.gui.listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JPanel;
import org.proteosuite.gui.analyse.AnalyseDynamicTab;
import org.proteosuite.gui.analyse.CleanIdentificationsStep;
import org.proteosuite.gui.analyse.CreateOrLoadIdentificationsStep;
import org.proteosuite.gui.analyse.DefineConditionsStep;
import org.proteosuite.gui.analyse.QuantitationStep;
import org.proteosuite.gui.analyse.RawDataAndMultiplexingStep;
import org.proteosuite.gui.tables.DefineConditionsTable;
import org.proteosuite.model.AnalyseData;
import org.proteosuite.model.RawDataFile;

/**
 *
 * @author SPerkins
 */
public abstract class AbstractMoveButtonListener implements ActionListener {

    protected final JPanel panel;
    protected AnalyseDynamicTab parent;
    protected static final AnalyseData DATA = AnalyseData.getInstance();

    public AbstractMoveButtonListener(final JPanel panel) {
        this.panel = panel;
    }

    @Override
    public final void actionPerformed(final ActionEvent e) {
        parent = (AnalyseDynamicTab) panel.getParent();

        if (panel instanceof RawDataAndMultiplexingStep) {
            moveFromRawDataAndMultiplexingStep();
        } else if (panel instanceof DefineConditionsStep) {
            moveFromDefineConditionsStep();
        } else if (panel instanceof CreateOrLoadIdentificationsStep) {
            moveFromIdentificationsStep();
        } else if (panel instanceof CleanIdentificationsStep) {
            moveFromCleanIdentificationsStep();
        } else if (panel instanceof QuantitationStep) {
            moveFromQuantitationStep();
        }
    }

    protected final void readConditionsTableIntoModel(final DefineConditionsTable conditionsTable) {
        for (int i = 0; i < conditionsTable.getRowCount(); i++) {
            String condition = (String) conditionsTable.getModel().
                    getValueAt(i, 0);

            String fileName = (String) conditionsTable.getModel().
                    getValueAt(i, 1);
            String assay = (String) conditionsTable.getModel().getValueAt(i,
                    2);
            for (int j = 0; j < DATA.getRawDataCount(); j++) {
                RawDataFile dataFile = DATA.getRawDataFile(j);
                if (dataFile.getFileName().equals(fileName) && dataFile.
                        getConditions().containsKey(assay)) {
                    dataFile.getConditions().put(assay, condition);
                }
            }
        }
    }

    protected abstract void moveFromRawDataAndMultiplexingStep();

    protected abstract void moveFromDefineConditionsStep();

    protected abstract void moveFromIdentificationsStep();

    protected abstract void moveFromCleanIdentificationsStep();

    protected abstract void moveFromQuantitationStep();
}
