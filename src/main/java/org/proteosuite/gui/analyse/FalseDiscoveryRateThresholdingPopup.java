package org.proteosuite.gui.analyse;

import java.awt.Component;
import java.awt.GridLayout;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;
import javax.swing.Box;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author SPerkins
 */
public final class FalseDiscoveryRateThresholdingPopup extends AbstractThresholdingPopup {
    private final JCheckBox lowerScoresBetter = new JCheckBox(
            "Lower Scores Better?");
    private final JComboBox<String> thresholdingValue = new JComboBox<>(
            new String[]{"0.00 (0%)", "0.001 (0.1%)", "0.005 (0.5%)",
                "0.01 (1%)", "0.02 (2%)", "0.03 (3%)", "0.04 (4%)", "0.05 (5%)"});
    private final JTextField decoyHitTag = new JTextField();
    private final JComboBox<String> fdrLevel = new JComboBox<>(new String[]{
        "PSM", "Peptide"});

    public FalseDiscoveryRateThresholdingPopup(final Window owner,
            final Map<String, String> thresholdables) {
        super(owner, thresholdables, "FDR Thresholding For Selected Files");
    }

    @Override
    protected JLabel getChooseThresholdingJLabel() {
        return new JLabel(
                "Choose a metric on which to apply FDR thresholding:");
    }

    @Override
    protected String getThresholdButtonText() {
        return "Threshold!";
    }

    @Override
    protected ActionListener getThresholdButtonListener() {
        return new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent event) {
                setVisible(false);
                dispose();

            }
        };
    }

    @Override
    protected JPanel getGrid() {
        JPanel theGrid = new JPanel();
        theGrid.setLayout(new GridLayout(5, 2));

        JLabel thresholdUsingLabel = new JLabel("FDR Threshold Using:");
        thresholdUsingLabel.setAlignmentX(Component.RIGHT_ALIGNMENT);
        theGrid.add(thresholdUsingLabel);

        theGrid.add(thresholdingChoice);

        theGrid.add(Box.createGlue());
        theGrid.add(lowerScoresBetter);
        JLabel valueLabel = new JLabel("FDR Threshold:");
        valueLabel.setAlignmentX(Component.RIGHT_ALIGNMENT);
        theGrid.add(valueLabel);
        theGrid.add(thresholdingValue);
        JLabel levelLabel = new JLabel("FDR Level:");
        levelLabel.setAlignmentX(Component.RIGHT_ALIGNMENT);
        theGrid.add(levelLabel);
        theGrid.add(fdrLevel);
        JLabel decoyFieldLabel = new JLabel("Decoy Hit Tag:");
        decoyFieldLabel.setToolTipText(
                "Tag may occur anywhere in the identifier.");
        decoyFieldLabel.setAlignmentX(Component.RIGHT_ALIGNMENT);
        theGrid.add(decoyFieldLabel);
        theGrid.add(decoyHitTag);

        return theGrid;
    }

    public String getThresholdTermChosen() {
        return thresholdables.get((String) thresholdingChoice.getSelectedItem());
    }

    public double getFDRThresholdValueChosen() {
        String thresholdingValueString = ((String) thresholdingValue.
                getSelectedItem()).split(" ")[0];
        return Double.parseDouble(thresholdingValueString);
    }

    public boolean areLowerValuesBetter() {
        return this.lowerScoresBetter.isSelected();
    }

    public String getFDRLevel() {
        return (String) this.fdrLevel.getSelectedItem();
    }

    public String getDecoyHitTag() {
        return this.decoyHitTag.getText();
    }
}
