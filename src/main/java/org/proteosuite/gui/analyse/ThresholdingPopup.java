package org.proteosuite.gui.analyse;

import java.awt.Component;
import java.awt.GridLayout;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;
import javax.swing.Box;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import org.proteosuite.utils.PrimitiveUtils;

/**
 *
 * @author SPerkins
 */
public final class ThresholdingPopup extends AbstractThresholdingPopup {
    private static final long serialVersionUID = 1L;
    private final JComboBox<String> thresholdingOperator = new JComboBox<>(new String[]{"<", ">"});;
    private final JTextField thresholdingValue = new JTextField("0.0");

    public ThresholdingPopup(final Window owner, final Map<String, String> thresholdables) {
        super(owner, thresholdables, "Choose Thresholding For Selected Files");
    }

    @Override
    protected JLabel getChooseThresholdingJLabel() {
        return new JLabel(
                "Choose a metric on which to apply thresholding:");
    }

    @Override
    protected String getThresholdButtonText() {
        return "Threshold!";
    }

    @Override
    protected ActionListener getThresholdButtonListener() {
        return new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent event) {
                if (PrimitiveUtils.isDouble(thresholdingValue.getText())) {
                    setVisible(false);
                    dispose();
                } else {
                    JOptionPane.showConfirmDialog(
                            ThresholdingPopup.this,
                            "You have entered an invalid number for the thresholding value.\n"
                            + "Please enter a valid number to proceed.",
                            "Invalid Thresholding Value",
                            JOptionPane.PLAIN_MESSAGE,
                            JOptionPane.ERROR_MESSAGE);
                }
            }
        };
    }

    @Override
    protected JPanel getGrid() {
        JPanel theGrid = new JPanel();
        theGrid.setLayout(new GridLayout(3, 2));
        JLabel thresholdOnLabel = new JLabel("Threshold On:");
        thresholdOnLabel.setAlignmentX(Component.RIGHT_ALIGNMENT);
        theGrid.add(thresholdOnLabel);
        theGrid.add(thresholdingChoice);
        theGrid.add(Box.createGlue());
        theGrid.add(thresholdingOperator);
        JLabel valueLabel = new JLabel("Value:");
        valueLabel.setAlignmentX(Component.RIGHT_ALIGNMENT);
        theGrid.add(valueLabel);
        theGrid.add(thresholdingValue);

        return theGrid;
    }

    public String[] getThresholdingChosen() {
        return new String[]{thresholdables.get((String) thresholdingChoice.
            getSelectedItem()), (String) thresholdingOperator.getSelectedItem(),
            thresholdingValue.getText()};
    }
}
