package org.proteosuite.gui.analyse;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Window;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.proteosuite.gui.IdentParamsView;
import org.proteosuite.gui.listener.ContinueButtonListener;
import org.proteosuite.gui.listener.CreateIdentificationsForSelectedListener;
import org.proteosuite.gui.listener.LoadIdentificationsForSelectedListener;
import org.proteosuite.gui.listener.PreviousButtonListener;
import org.proteosuite.gui.listener.ResetIdentificationsForSelectedListener;
import org.proteosuite.gui.listener.TableButtonToggleListener;
import org.proteosuite.gui.tables.CreateOrLoadIdentificationsTable;
import org.proteosuite.model.AnalyseData;
import org.proteosuite.model.RawDataFile;

/**
 *
 * @author SPerkins
 */
public final class CreateOrLoadIdentificationsStep extends JPanel {

    private static final long serialVersionUID = 1L;
    private final CreateOrLoadIdentificationsTable identificationsTable
            = new CreateOrLoadIdentificationsTable();
    private IdentParamsView identParamsView = null;
    private final JButton createIdentifications;
    private final JButton loadIdentifications;
    private static final AnalyseData DATA = AnalyseData.getInstance();
    private boolean enableCreateIdentificationsButton = true;

    public CreateOrLoadIdentificationsStep() {
        super(new BorderLayout());

        JLabel stepTitle = new JLabel("Create or load your identifications:");
        stepTitle.setFont(new Font(stepTitle.getFont().getFontName(), stepTitle
                .getFont().getStyle(), 72));

        JPanel buttonsPanel = new JPanel(new GridLayout(2, 3));

        loadIdentifications = new JButton(
                "Load identifications for selected...");
        createIdentifications = new JButton(
                "Create identifications for selected...");
        JButton resetIdentifications = new JButton(
                "Reset status for selected...");

        loadIdentifications.setEnabled(false);
        createIdentifications.setEnabled(false);
        resetIdentifications.setEnabled(false);

        loadIdentifications
                .addActionListener(new LoadIdentificationsForSelectedListener(
                        this));
        createIdentifications
                .addActionListener(new CreateIdentificationsForSelectedListener(
                        this));
        resetIdentifications
                .addActionListener(new ResetIdentificationsForSelectedListener(
                        this));

        identificationsTable.getSelectionModel().addListSelectionListener(
                new TableButtonToggleListener(identificationsTable,
                        loadIdentifications, createIdentifications,
                        resetIdentifications));

        JButton continueButton = new JButton("Continue");
        JButton previousButton = new JButton("Previous");

        previousButton.addActionListener(new PreviousButtonListener(this));
        continueButton.addActionListener(new ContinueButtonListener(this));

        buttonsPanel.add(loadIdentifications);
        buttonsPanel.add(createIdentifications);
        buttonsPanel.add(resetIdentifications);

        buttonsPanel.add(previousButton);
        buttonsPanel.add(Box.createGlue());
        buttonsPanel.add(continueButton);

        add(stepTitle, BorderLayout.PAGE_START);
        add(new JScrollPane(identificationsTable), BorderLayout.CENTER);
        add(buttonsPanel, BorderLayout.PAGE_END);
    }

    public CreateOrLoadIdentificationsTable getIdentificationsTable() {
        return identificationsTable;
    }

    public IdentParamsView getIdentParamsView() {
        if (identParamsView == null) {
            Component parent = this.getParent();
            while (!(parent instanceof JFrame)) {
                parent = parent.getParent();
            }

            identParamsView = new IdentParamsView(
                    (Window) parent, DATA.doingGenomeAnnotation());
        }
        return identParamsView;
    }

    public synchronized void setCreateButtonEnabled(final boolean state) {
        this.enableCreateIdentificationsButton = state;
        this.createIdentifications.setEnabled(state);
    }

    public synchronized void refreshFromData() {
        identificationsTable.clear();
        for (int i = 0; i < DATA.getRawDataCount(); i++) {
            RawDataFile dataFile = DATA.getRawDataFile(i);
            identificationsTable.addRawFileRow(dataFile);
        }

        if (DATA.doingGenomeAnnotation()) {
            identificationsTable.getSelectionModel().addListSelectionListener(
                    new ListSelectionListener() {
                @Override
                public void valueChanged(final ListSelectionEvent e) {
                    if (identificationsTable.getSelectedRows().length > 0) {
                        identificationsTable.clearSelection();
                    }
                }
            });

            createIdentifications.setText("Create identifications for all...");
            if (this.enableCreateIdentificationsButton) {
                createIdentifications.setEnabled(true);
            }

            loadIdentifications.setText("Load identifications for all...");

        }
    }
}
