package org.proteosuite.gui.analyse;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import org.proteosuite.gui.listener.RawDataAndMultiplexingSectionListener;
import org.proteosuite.model.AnalyseData;

/**
 *
 * @author SPerkins
 */
public final class AnalyseStatusPanel extends JPanel {

    private static final long serialVersionUID = 1L;
    private static final JPanel RAW_DATA_SECTION = new JPanel();
    private static final JLabel RAW_DATA = new JLabel("Raw Data");
    private static final JLabel RAW_DATA_STATUS = new JLabel();
    private static final JPanel CONDITIONS_SECTION = new JPanel();
    private static final JLabel CONDITIONS = new JLabel("Conditions");
    private static final JLabel CONDITIONS_STATUS = new JLabel();
    private static final JPanel IDENTIFICATIONS_SECTION = new JPanel();
    private static final JLabel IDENTIFICATIONS = new JLabel("Identifications");
    private static final JLabel IDENTIFICATIONS_STATUS = new JLabel();
    private static final JPanel CLEAN_IDENTIFICATIONS_SECTION = new JPanel();
    private static final JLabel CLEAN_IDENTIFICATIONS = new JLabel(
            "Clean Identifications");
    private static final JLabel CLEAN_IDENTIFICATIONS_STATUS = new JLabel();
    private static final JPanel QUANTITATION_SECTION = new JPanel();
    private static final JLabel QUANTITATION = new JLabel("Quantitation");
    private static final JLabel QUANTITATION_STATUS = new JLabel();
    private static final JPanel MAPPING_SECTION = new JPanel();
    private static final JLabel MAPPING = new JLabel("Mapping");
    private static final JLabel MAPPING_STATUS = new JLabel();
    private static final JPanel NORMALISATION_SECTION = new JPanel();
    private static final JLabel NORMALISATION = new JLabel("Normalisation");
    private static final JLabel NORMALISATION_STATUS = new JLabel();
    private static final JPanel PROTEIN_INFERENCE_SECTION = new JPanel();
    private static final JLabel PROTEIN_INFERENCE = new JLabel(
            "Protein Inference");
    private static final JLabel PROTEIN_INFERENCE_STATUS = new JLabel();
    private static final JPanel ANOVA_SECTION = new JPanel();
    private static final JLabel ANOVA = new JLabel("ANOVA");
    private static final JLabel ANOVA_STATUS = new JLabel();
    private static final JPanel RESULTS_SECTION = new JPanel();
    private static final JLabel RESULTS = new JLabel("Results");
    private static final JLabel RESULTS_STATUS = new JLabel();
    private static final ImageIcon NOT_DONE = new ImageIcon(AnalyseStatusPanel.class.
            getClassLoader().getResource(
                    "images/empty.gif"));
    private static final ImageIcon DONE = new ImageIcon(AnalyseStatusPanel.class.
            getClassLoader().getResource(
                    "images/fill.gif"));
    private static final ImageIcon PROCESSING = new ImageIcon(
            AnalyseStatusPanel.class.getClassLoader().getResource(
                    "images/loading.gif"));
    private static AnalyseStatusPanel INSTANCE = null;

    public static AnalyseStatusPanel getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new AnalyseStatusPanel();
        }

        return INSTANCE;
    }

    private AnalyseStatusPanel() {
        super(new FlowLayout(FlowLayout.CENTER, 30, 4));

        RAW_DATA.setFont(new Font(RAW_DATA.getFont().getFamily(), RAW_DATA
                .getFont().getStyle(), 20));
        CONDITIONS.setFont(new Font(CONDITIONS.getFont().getFamily(),
                CONDITIONS.getFont().getStyle(), 20));
        IDENTIFICATIONS.setFont(new Font(IDENTIFICATIONS.getFont()
                .getFamily(), IDENTIFICATIONS.getFont().getStyle(), 20));
        CLEAN_IDENTIFICATIONS.setFont(new Font(CLEAN_IDENTIFICATIONS.getFont()
                .getName(), CLEAN_IDENTIFICATIONS.getFont().getStyle(), 20));
        QUANTITATION.setFont(new Font(QUANTITATION.getFont().getFamily(),
                QUANTITATION.getFont().getStyle(), 20));
        MAPPING.setFont(new Font(MAPPING.getFont().getFamily(), MAPPING
                .getFont().getStyle(), 20));
        NORMALISATION.setFont(new Font(NORMALISATION.getFont()
                .getFamily(), NORMALISATION.getFont().getStyle(), 20));
        PROTEIN_INFERENCE.setFont(new Font(PROTEIN_INFERENCE.getFont()
                .getFamily(), PROTEIN_INFERENCE.getFont().getStyle(), 20));
        ANOVA.setFont(new Font(ANOVA.getFont()
                .getFamily(), ANOVA.getFont().getStyle(), 20));
        RESULTS.setFont(new Font(RESULTS.getFont()
                .getFamily(), RESULTS.getFont().getStyle(), 20));

        RAW_DATA_STATUS.setIcon(NOT_DONE);
        CONDITIONS_STATUS.setIcon(NOT_DONE);
        IDENTIFICATIONS_STATUS.setIcon(NOT_DONE);
        CLEAN_IDENTIFICATIONS_STATUS.setIcon(NOT_DONE);
        QUANTITATION_STATUS.setIcon(NOT_DONE);
        MAPPING_STATUS.setIcon(NOT_DONE);
        NORMALISATION_STATUS.setIcon(NOT_DONE);
        PROTEIN_INFERENCE_STATUS.setIcon(NOT_DONE);
        ANOVA_STATUS.setIcon(NOT_DONE);
        RESULTS_STATUS.setIcon(NOT_DONE);

        RAW_DATA_SECTION.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 4));
        CONDITIONS_SECTION.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 4));
        IDENTIFICATIONS_SECTION
                .setLayout(new FlowLayout(FlowLayout.CENTER, 5, 4));
        CLEAN_IDENTIFICATIONS_SECTION.setLayout(new FlowLayout(FlowLayout.CENTER,
                5, 4));
        QUANTITATION_SECTION.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 4));
        MAPPING_SECTION.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 4));
        NORMALISATION_SECTION.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 4));
        PROTEIN_INFERENCE_SECTION.setLayout(new FlowLayout(FlowLayout.CENTER, 5,
                4));
        ANOVA_SECTION.setLayout(new FlowLayout(FlowLayout.CENTER, 5,
                4));
        RESULTS_SECTION.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 4));

        RAW_DATA_SECTION.add(RAW_DATA_STATUS);
        RAW_DATA_SECTION.add(RAW_DATA);
        CONDITIONS_SECTION.add(CONDITIONS_STATUS);
        CONDITIONS_SECTION.add(CONDITIONS);
        IDENTIFICATIONS_SECTION.add(IDENTIFICATIONS_STATUS);
        IDENTIFICATIONS_SECTION.add(IDENTIFICATIONS);
        CLEAN_IDENTIFICATIONS_SECTION.add(CLEAN_IDENTIFICATIONS_STATUS);
        CLEAN_IDENTIFICATIONS_SECTION.add(CLEAN_IDENTIFICATIONS);
        QUANTITATION_SECTION.add(QUANTITATION_STATUS);
        QUANTITATION_SECTION.add(QUANTITATION);
        MAPPING_SECTION.add(MAPPING_STATUS);
        MAPPING_SECTION.add(MAPPING);
        NORMALISATION_SECTION.add(NORMALISATION_STATUS);
        NORMALISATION_SECTION.add(NORMALISATION);
        PROTEIN_INFERENCE_SECTION.add(PROTEIN_INFERENCE_STATUS);
        PROTEIN_INFERENCE_SECTION.add(PROTEIN_INFERENCE);
        ANOVA_SECTION.add(ANOVA_STATUS);
        ANOVA_SECTION.add(ANOVA);
        RESULTS_SECTION.add(RESULTS_STATUS);
        RESULTS_SECTION.add(RESULTS);

        add(RAW_DATA_SECTION);
        add(CONDITIONS_SECTION);
        add(IDENTIFICATIONS_SECTION);
        add(CLEAN_IDENTIFICATIONS_SECTION);
        add(QUANTITATION_SECTION);
        add(MAPPING_SECTION);
        add(NORMALISATION_SECTION);
        add(PROTEIN_INFERENCE_SECTION);
        add(ANOVA_SECTION);
        add(RESULTS_SECTION);

        RAW_DATA.addMouseListener(new RawDataAndMultiplexingSectionListener());
    }

    public void setGenomeAnnotationMode() {
        CONDITIONS_SECTION.remove(CONDITIONS_STATUS);
        CLEAN_IDENTIFICATIONS_SECTION.remove(CLEAN_IDENTIFICATIONS_STATUS);
        QUANTITATION_SECTION.remove(QUANTITATION_STATUS);
        MAPPING_SECTION.remove(MAPPING_STATUS);
        PROTEIN_INFERENCE_SECTION.remove(PROTEIN_INFERENCE_STATUS);
        ANOVA_SECTION.remove(ANOVA_STATUS);

        CONDITIONS.setForeground(Color.LIGHT_GRAY);
        CLEAN_IDENTIFICATIONS.setForeground(Color.LIGHT_GRAY);
        QUANTITATION.setForeground(Color.LIGHT_GRAY);
        MAPPING.setForeground(Color.LIGHT_GRAY);
        NORMALISATION.setForeground(Color.LIGHT_GRAY);
        PROTEIN_INFERENCE.setForeground(Color.LIGHT_GRAY);
        ANOVA_SECTION.setForeground(Color.LIGHT_GRAY);

        this.revalidate();
    }

    public void reset() {
        setRawDataNotDone();
        setConditionsNotDone();
        setIdentificationsNotDone();
        setCleanIdentificationsNotDone();
        setQuantitationNotDone();
        setMappingNotDone();
        setNormalisationNotDone();
        setProteinInferenceNotDone();
        setAnovaNotDone();
        setResultsNotDone();
    }

    private void setAllAsPlainText() {
        RAW_DATA.setFont(new Font(RAW_DATA.getFont()
                .getFamily(), Font.PLAIN, 20));

        CONDITIONS.setFont(new Font(CONDITIONS.getFont()
                .getFamily(), Font.PLAIN, 20));

        IDENTIFICATIONS.setFont(new Font(IDENTIFICATIONS.getFont()
                .getFamily(), Font.PLAIN, 20));

        CLEAN_IDENTIFICATIONS.setFont(new Font(CLEAN_IDENTIFICATIONS.getFont()
                .getFamily(), Font.PLAIN, 20));

        QUANTITATION.setFont(new Font(QUANTITATION.getFont()
                .getFamily(), Font.PLAIN, 20));

        MAPPING.setFont(new Font(MAPPING.getFont()
                .getFamily(), Font.PLAIN, 20));

        NORMALISATION.setFont(new Font(NORMALISATION.getFont()
                .getFamily(), Font.PLAIN, 20));

        PROTEIN_INFERENCE.setFont(new Font(PROTEIN_INFERENCE.getFont()
                .getFamily(), Font.PLAIN, 20));

        ANOVA.setFont(new Font(ANOVA.getFont()
                .getFamily(), Font.PLAIN, 20));

        RESULTS.setFont(new Font(RESULTS.getFont()
                .getFamily(), Font.PLAIN, 20));
    }

    public void setRawDataAsCurrentStep() {
        setAllAsPlainText();
        RAW_DATA.setFont(new Font(RAW_DATA.getFont()
                .getFamily(), Font.BOLD, 20));
    }

    public void setConditionsAsCurrentStep() {
        setAllAsPlainText();
        CONDITIONS.setFont(new Font(CONDITIONS.getFont()
                .getFamily(), Font.BOLD, 20));
    }

    public void setIdentificationsAsCurrentStep() {
        setAllAsPlainText();
        IDENTIFICATIONS.setFont(new Font(IDENTIFICATIONS.getFont()
                .getFamily(), Font.BOLD, 20));
    }

    public void setCleanConditionsAsCurrentStep() {
        setAllAsPlainText();
        CLEAN_IDENTIFICATIONS.setFont(new Font(CLEAN_IDENTIFICATIONS.getFont()
                .getFamily(), Font.BOLD, 20));
    }

    public void setQuantitationAsCurrentStep() {
        setAllAsPlainText();
        QUANTITATION.setFont(new Font(QUANTITATION.getFont()
                .getFamily(), Font.BOLD, 20));
    }

    public void setNormalisationAsCurrentStep() {
        setAllAsPlainText();
        NORMALISATION.setFont(new Font(NORMALISATION.getFont()
                .getFamily(), Font.BOLD, 20));
    }

    public void setAnovaAsCurrentStep() {
        setAllAsPlainText();
        ANOVA.setFont(new Font(ANOVA.getFont()
                .getFamily(), Font.BOLD, 20));
    }

    public void setResultsAsCurrentStep() {
        setAllAsPlainText();
        RESULTS.setFont(new Font(RESULTS.getFont()
                .getFamily(), Font.BOLD, 20));
    }

    public void setRawDataDone() {
        RAW_DATA_STATUS.setIcon(DONE);
    }

    public void setRawDataNotDone() {
        RAW_DATA_STATUS.setIcon(NOT_DONE);
    }

    public void setRawDataProcessing() {
        RAW_DATA_STATUS.setIcon(PROCESSING);
    }

    public void setConditionsDone() {
        CONDITIONS_STATUS.setIcon(DONE);
    }

    public void setConditionsNotDone() {
        CONDITIONS_STATUS.setIcon(NOT_DONE);
    }

    public void setConditionsProcessing() {
        CONDITIONS_STATUS.setIcon(PROCESSING);
    }

    public void setIdentificationsDone() {
        IDENTIFICATIONS_STATUS.setIcon(DONE);
    }

    public void setIdentificationsNotDone() {
        IDENTIFICATIONS_STATUS.setIcon(NOT_DONE);
    }

    public void setIdentificationsProcessing() {
        IDENTIFICATIONS_STATUS.setIcon(PROCESSING);
    }

    public void setCleanIdentificationsDone() {
        CLEAN_IDENTIFICATIONS_STATUS.setIcon(DONE);
    }

    public void setCleanIdentificationsNotDone() {
        CLEAN_IDENTIFICATIONS_STATUS.setIcon(NOT_DONE);
    }

    public void setCleanIdentificationsProcessing() {
        CLEAN_IDENTIFICATIONS_STATUS.setIcon(PROCESSING);
    }

    public void setQuantitationDone() {
        QUANTITATION_STATUS.setIcon(DONE);
    }

    public void setQuantitationNotDone() {
        QUANTITATION_STATUS.setIcon(NOT_DONE);
    }

    public void setQuantitationProcessing() {
        QUANTITATION_STATUS.setIcon(PROCESSING);
    }

    public void setMappingDone() {
        MAPPING_STATUS.setIcon(DONE);
    }

    public void setMappingNotDone() {
        MAPPING_STATUS.setIcon(NOT_DONE);
    }

    public void setMappingProcessing() {
        MAPPING_STATUS.setIcon(PROCESSING);
    }

    public void setNormalisationDone() {
        NORMALISATION_STATUS.setIcon(DONE);
    }

    public void setNormalisationNotDone() {
        NORMALISATION_STATUS.setIcon(NOT_DONE);
    }

    public void setNormalisationProcessing() {
        NORMALISATION_STATUS.setIcon(PROCESSING);
    }

    public void setProteinInferenceDone() {
        PROTEIN_INFERENCE_STATUS.setIcon(DONE);
    }

    public void setProteinInferenceNotDone() {
        PROTEIN_INFERENCE_STATUS.setIcon(NOT_DONE);
    }

    public void setProteinInferenceProcessing() {
        PROTEIN_INFERENCE_STATUS.setIcon(PROCESSING);
    }

    public void setAnovaDone() {
        ANOVA_STATUS.setIcon(DONE);
    }

    public void setAnovaNotDone() {
        ANOVA_STATUS.setIcon(NOT_DONE);
    }

    public void setAnovaProcessing() {
        ANOVA_STATUS.setIcon(PROCESSING);
    }

    public void setResultsDone() {
        RESULTS_STATUS.setIcon(DONE);
    }

    public void setResultsNotDone() {
        RESULTS_STATUS.setIcon(NOT_DONE);
    }

    public void setResultsProcessing() {
        RESULTS_STATUS.setIcon(PROCESSING);
    }

    public synchronized void checkAndUpdateRawDataStatus() {
        AnalyseData data = AnalyseData.getInstance();
        if (data.getRawDataCount() == 0) {
            setRawDataNotDone();
            return;
        }

        boolean allDataLoaded = true;
        for (int i = 0; i < data.getRawDataCount(); i++) {
            if (!data.getRawDataFile(i).isLoaded()) {
                allDataLoaded = false;
                break;
            }
        }

        if (allDataLoaded) {
            setRawDataDone();
        }
    }

    public synchronized boolean checkAndUpdateIdentificationsStatus() {
        AnalyseData data = AnalyseData.getInstance();
        boolean allDataLoaded = true;
        for (int i = 0; i < data.getRawDataCount(); i++) {
            if (data.getRawDataFile(i).getIdentificationDataFile() == null
                    || !data.getRawDataFile(i).getIdentificationDataFile()
                    .isLoaded()) {
                allDataLoaded = false;
                break;
            }
        }

        if (allDataLoaded) {
            setIdentificationsDone();
        }

        return allDataLoaded;
    }
}
