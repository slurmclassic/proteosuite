
package org.proteosuite.gui.analyse;

import java.awt.Component;
import java.awt.Dialog;
import java.awt.Window;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Map;
import java.util.Map.Entry;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

/**
 *
 * @author SPerkins
 */
public abstract class AbstractThresholdingPopup extends JDialog {
    protected final transient Map<String, String> thresholdables;
    protected JComboBox<String> thresholdingChoice;
    protected boolean hasBeenClosed = false;

    public AbstractThresholdingPopup(final Window owner, final Map<String, String> thresholdables, final String popupTitle) {
        super(owner, popupTitle, Dialog.ModalityType.APPLICATION_MODAL);

        this.thresholdables = thresholdables;

        super.setIconImage(new ImageIcon(getClass().getClassLoader().getResource(
                "images/icon.gif")).getImage());

        super.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(final WindowEvent e) {
                hasBeenClosed = true;
                dispose();
            }
        });

        super.getContentPane().add(generateJPanel());
        super.pack();
    }

    public final boolean hasBeenClosed() {
        return this.hasBeenClosed;
    }

    private JPanel generateJPanel() {
        JPanel panel = new JPanel();
        panel.setBorder(new EmptyBorder(5, 5, 5, 5));
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

        JLabel message = getChooseThresholdingJLabel();
        message.setAlignmentX(Component.CENTER_ALIGNMENT);

        thresholdingChoice = new JComboBox<>();
        for (Entry<String, String> entry : thresholdables.entrySet()) {
            thresholdingChoice.addItem(entry.getKey());
        }

        JButton thresholdButton = new JButton(getThresholdButtonText());
        thresholdButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        thresholdButton.addActionListener(getThresholdButtonListener());

        panel.add(message);

        panel.add(getGrid());
        panel.add(thresholdButton);

        return panel;
    }

    protected abstract JLabel getChooseThresholdingJLabel();
    protected abstract String getThresholdButtonText();
    protected abstract ActionListener getThresholdButtonListener();
    protected abstract JPanel getGrid();
}
