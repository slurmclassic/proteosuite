package org.proteosuite.gui.tables;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 * Class representing conditions in the experiment, allowing the user to describe how they map to assays.
 * @author SPerkins
 */
public final class DefineConditionsTable extends JTable {

    private static final long serialVersionUID = 1L;
    private DefaultTableModel model;

    /**
     * Creates an instance of this class.
     */
    public DefineConditionsTable() {
        model = new DefaultTableModel() {
            private static final long serialVersionUID = 1L;

            @Override
            public boolean isCellEditable(final int row, final int col) {
                return col == 0;
            }
        };

        model.addColumn("Condition");
        model.addColumn("File Name");
        model.addColumn("Multiplex Assay");
        setModel(model);
    }

    /**
     * Adds a new row to the conditions table.
     * @param condition Name of the condition (editable by the user).
     * @param fileName Name of file that contains the assay(s).
     * @param assay The technical name of the assay.
     */
    public void addConditionRow(final String condition, final String fileName, final String assay) {
        model.addRow(new Object[]{condition, fileName, assay});
    }

    /**
     * Clears the conditions table.
     */
    public void clear() {
        model.setRowCount(0);
    }

    /**
     * Checks whether the data in the conditions table (with user input) is valid.
     * @return Whether the data in the conditions table is valid.
     */
    public boolean isConditionsValid() {
        for (int i = 0; i < getRowCount(); i++) {
            String condition = (String) model.getValueAt(i, 0);
            if (condition.isEmpty()) {
                return false;
            }
        }
        return true;
    }
}
