package org.proteosuite.gui.tables;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

import org.proteosuite.model.MzQuantMLFile;

import uk.ac.liv.pgb.jmzqml.MzQuantMLElement;
import uk.ac.liv.pgb.jmzqml.model.mzqml.Assay;
import uk.ac.liv.pgb.jmzqml.model.mzqml.AssayList;
import uk.ac.liv.pgb.jmzqml.model.mzqml.MzQuantML;
import uk.ac.liv.pgb.jmzqml.model.mzqml.Protein;
import uk.ac.liv.pgb.jmzqml.model.mzqml.ProteinList;
import uk.ac.liv.pgb.jmzqml.model.mzqml.Row;
import uk.ac.liv.pgb.jmzqml.model.mzqml.StudyVariable;
import uk.ac.liv.pgb.jmzqml.model.mzqml.StudyVariableList;
import uk.ac.liv.pgb.jmzqml.xml.io.MzQuantMLUnmarshaller;

/**
 *
 * @author Andrew Collins
 */
public final class JTableProteinQuant extends JTableDefault {

    private static final long serialVersionUID = 1L;

    public void showData(final MzQuantMLFile dataFile) {
        DefaultTableModel proteinModel = new DefaultTableModel() {
            private static final long serialVersionUID = 1L;

            @Override
            public boolean isCellEditable(final int row, final int col) {
                return false;
            }

            @Override
            public Class<?> getColumnClass(final int columnIndex) {
                if (columnIndex == 0) {
                    return String.class;
                } else {
                    return Double.class;
                }
            }
        };
        setModel(proteinModel);
        proteinModel.addColumn("Protein");

        MzQuantMLUnmarshaller unmarshaller = dataFile.getUnmarshaller();
        MzQuantML mzq = unmarshaller.unmarshal(MzQuantML.class);

        // Protein Quantitation
        // Based on the the assay list and study variables
        // we will include the different columns
        AssayList assayList = mzq.getAssayList();
        List<Assay> listAssay = assayList.getAssay();
        int iAssays = listAssay.size();
        for (Assay assay : listAssay) {
            proteinModel.addColumn(assay.getName());
        }

        StudyVariableList studyList = unmarshaller
                .unmarshal(MzQuantMLElement.StudyVariableList);

        int iStudyVars = 0;
        if (studyList != null) {
            List<StudyVariable> listStudy = studyList.getStudyVariable();
            iStudyVars = listStudy.size();
            for (StudyVariable study : listStudy) {
                proteinModel.addColumn(study.getName());
            }
        }

        // Fill rows
        // Check if mzq file contains protein list
        ProteinList proteinList = unmarshaller
                .unmarshal(MzQuantMLElement.ProteinList);

        if (proteinList == null) {
            return;
        }

        Map<String, Protein> mapIDToProt = new HashMap<>();
        for (Protein protein : proteinList.getProtein()) {
            mapIDToProt.put(protein.getId(), protein);
        }

        Map<String, List<String>> hmProtein
                = new HashMap<>();
        // Getting DataMatrix from AssayQuantLayer
        if (proteinList.getAssayQuantLayer().size() > 0) {
            List<Row> dataMatrix = proteinList.getAssayQuantLayer().get(0)
                    .getDataMatrix().getRow();
            for (Row row : dataMatrix) {
                String protID = row.getObjectRef();
                List<String> al = row.getValue();
                hmProtein.put(mapIDToProt.get(protID).getAccession(), al);
            }
        }
        // Getting DataMatrix from StudyVariableQuantLayer
        if (proteinList.getStudyVariableQuantLayer().size() > 0) {
            List<Row> dataMatrix2 = proteinList.getStudyVariableQuantLayer()
                    .get(0).getDataMatrix().getRow();
            for (Row row : dataMatrix2) {
                // Protein prot = (Protein)
                // row.getObjectRef();
                String protID = row.getObjectRef();
                List<String> al2 = hmProtein.get(mapIDToProt.get(protID)
                        .getAccession());
                for (String obj : row.getValue()) {
                    al2.add(obj);
                }
                hmProtein.put(mapIDToProt.get(protID).getAccession(), al2);
            }
        }
        for (Entry<String, List<String>> entry : hmProtein.entrySet()) {
            Object[] aObj = new Object[iAssays + iStudyVars + 1];
            aObj[0] = entry.getKey();

            int iI = 1;
            for (String s : entry.getValue()) {
                if (iI >= iAssays + iStudyVars + 1) {
                    JOptionPane
                            .showMessageDialog(
                                    null,
                                    "Invalid file. The mzq file contains duplications in the DataMatrix on "
                                    + "the AssayQuantLayer or StudyVariableQuantLayer",
                                    "Information",
                                    JOptionPane.INFORMATION_MESSAGE);
                    break;
                } else {
                    aObj[iI] = Double.parseDouble(s);
                    iI++;
                }
            }
            proteinModel.insertRow(proteinModel.getRowCount(), aObj);
        }

        setupTooltipsForHeaders();
    }

    @Override
    protected String getTableType() {
        return "Protein";
    }
}
