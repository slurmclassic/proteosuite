package org.proteosuite.gui.tables;

import javax.swing.table.DefaultTableModel;

public abstract class JTableDefault extends AbstractJTable {
    private static final long serialVersionUID = 1L;

    @Override
    public final void reset() {
        DefaultTableModel model = new DefaultTableModel();
        model.addColumn(getTableType());
        setModel(model);
    }
}
