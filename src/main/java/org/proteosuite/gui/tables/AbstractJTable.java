package org.proteosuite.gui.tables;

import java.awt.Component;
import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

/**
 *
 * @author SPerkins
 */
public abstract class AbstractJTable extends JTable {
    private static final long serialVersionUID = 1L;
    protected final void setupTooltipsForHeaders() {
        getTableHeader().setDefaultRenderer(new TableCellRenderer() {
            final TableCellRenderer defaultRenderer = getTableHeader().getDefaultRenderer();

            @Override
            public Component getTableCellRendererComponent(final JTable table,
                    final Object value, final boolean isSelected, final boolean hasFocus,
                    final int row, final int column) {
                JComponent component = (JComponent) defaultRenderer
                        .getTableCellRendererComponent(table, value,
                                isSelected, hasFocus, row, column);
                component.setToolTipText(""
                        + getColumnName(column));
                return component;
            }
        });

        setAutoCreateRowSorter(true);
    }

    @Override
    public final boolean isCellEditable(final int rowIndex, final int colIndex) {
        return false;
    }

    protected abstract void reset();
    protected abstract String getTableType();
}
