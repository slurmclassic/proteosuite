package org.proteosuite.gui.tables;

import java.util.List;

import javax.swing.table.DefaultTableModel;

import org.proteosuite.model.MzQuantMLFile;
import uk.ac.liv.pgb.jmzqml.MzQuantMLElement;
import uk.ac.liv.pgb.jmzqml.model.mzqml.Assay;
import uk.ac.liv.pgb.jmzqml.model.mzqml.AssayList;
import uk.ac.liv.pgb.jmzqml.model.mzqml.MzQuantML;
import uk.ac.liv.pgb.jmzqml.model.mzqml.PeptideConsensusList;
import uk.ac.liv.pgb.jmzqml.model.mzqml.Row;
import uk.ac.liv.pgb.jmzqml.xml.io.MzQuantMLUnmarshaller;

public final class JTablePeptideQuant extends JTableDefault {
    private static final long serialVersionUID = 1L;

    public void showData(final MzQuantMLFile dataFile) {
        DefaultTableModel peptideModel = new DefaultTableModel() {
            private static final long serialVersionUID = 1L;

            @Override
            public boolean isCellEditable(final int row, final int col) {
                return false;
            }

            @Override
            public Class<?> getColumnClass(final int columnIndex) {
                if (columnIndex == 0) {
                    return String.class;
                } else {
                    return Double.class;
                }
            }
        };
        setModel(peptideModel);
        peptideModel.addColumn("Peptide");

        MzQuantMLUnmarshaller unmarshaller = dataFile.getUnmarshaller();
        MzQuantML mzq = unmarshaller.unmarshal(MzQuantML.class);

        // Protein Quantitation
        // Based on the the assay list and study variables
        // we will include the different columns
        AssayList assayList = mzq.getAssayList();
        List<Assay> listAssay = assayList.getAssay();
        final int iAssays = listAssay.size();

        // Peptide Quantitation
        // Based on the the assay list and study
        // variables we will include the different columns
        for (Assay assay : listAssay) {
            peptideModel.addColumn(assay.getName());
        }

        // Getting DataMatrix from AssayQuantLayer
        PeptideConsensusList pepConsList = unmarshaller
                .unmarshal(MzQuantMLElement.PeptideConsensusList);

        if (pepConsList.getAssayQuantLayer().size() > 0) {
            List<Row> dataMatrix3 = pepConsList.getAssayQuantLayer().get(0)
                    .getDataMatrix().getRow();
            for (Row row : dataMatrix3) {
                Object[] aObj = new Object[iAssays + 1];

                aObj[0] = row.getObjectRef();

                int iI = 1;
                for (String s : row.getValue()) {
                    if (s.equals("null")) {
                        aObj[iI] = Double.NaN;
                    } else {
                        aObj[iI] = Double.parseDouble(s);
                    }
                    iI++;
                }
                peptideModel.insertRow(peptideModel.getRowCount(), aObj);
            }
        }

        super.setupTooltipsForHeaders();
    }

    @Override
    protected String getTableType() {
        return "Peptide";
    }
}
