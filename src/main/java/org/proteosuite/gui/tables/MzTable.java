package org.proteosuite.gui.tables;

import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import org.proteosuite.model.FragmentSpectrum;
import org.proteosuite.model.MzIntensityPair;
import org.proteosuite.model.PrecursorSpectrum;
import org.proteosuite.model.RawDataFile;
import org.proteosuite.model.Spectrum;

/**
 *
 * @author Andrew collins
 */
public final class MzTable extends AbstractJTable {
    private static final long serialVersionUID = 1L;

    private DefaultTableModel model;

    public MzTable() {
        super();
        setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }

    public void showData(final RawDataFile dataFile,
            final byte msLevelThreshold, final double lowIntensityThreshold) {
        for (Spectrum spectrum : dataFile) {
            if (msLevelThreshold == 1 && spectrum instanceof FragmentSpectrum) {
                continue;
            } else if (msLevelThreshold == 2
                    && spectrum instanceof PrecursorSpectrum) {
                continue;
            }

            MzIntensityPair basePeak = spectrum.getBasePeak();
            if (basePeak.getIntensity() == 0.0 && basePeak.getMz() == 0.0) {
                calculateBasePeak(basePeak, spectrum);
            }

            if (basePeak.getIntensity() < lowIntensityThreshold) {
                continue;
            }

            double precursorMz = spectrum instanceof FragmentSpectrum
                    ? ((FragmentSpectrum) spectrum).getPrecursor().getMz() : 0.0;

            if (precursorMz > 0.0) {
                model.insertRow(
                        model.getRowCount(),
                        new Object[]{
                            spectrum.getSpectrumIndex(),
                            spectrum.getSpectrumID(),
                            spectrum instanceof PrecursorSpectrum ? 1 : 2,
                            basePeak.getMz(),
                            basePeak.getIntensity(),
                            spectrum.getRetentionTimeInSeconds(),
                            precursorMz
                        });
            } else {
                model.insertRow(
                        model.getRowCount(),
                        new Object[]{
                            spectrum.getSpectrumIndex(),
                            spectrum.getSpectrumID(),
                            spectrum instanceof PrecursorSpectrum ? 1 : 2,
                            basePeak.getMz(),
                            basePeak.getIntensity(),
                            spectrum.getRetentionTimeInSeconds(),
                            ""
                        });
            }
        }

        setupTooltipsForHeaders();
    }

    @Override
    public void reset() {
        model = new DefaultTableModel() {
            private static final long serialVersionUID = 1L;
            Class<?>[] types = new Class[]{Integer.class, String.class,
                Byte.class, Float.class, Float.class, Float.class,
                String.class};

            @Override
            public Class<?> getColumnClass(final int columnIndex) {
                return types[columnIndex];
            }

            @Override
            public boolean isCellEditable(final int row, final int col) {
                return false;
            }
        };

        model.addColumn("Index");
        model.addColumn("ID");
        model.addColumn("MS");
        model.addColumn("Base peak m/z");
        model.addColumn("Base peak int");
        model.addColumn("RT (sec)");
        model.addColumn("Precurs m/z");
        setModel(model);
    }

    private static void calculateBasePeak(final MzIntensityPair basePeak,
            final Spectrum spectrum) {
        double intensity = 0.0;
        double mz = 0.0;
        for (MzIntensityPair dataPoint : spectrum) {
            if (dataPoint.getIntensity() > intensity) {
                intensity = dataPoint.getIntensity();
                mz = dataPoint.getMz();
            }
        }

        basePeak.setMz(mz);
        basePeak.setIntensity(intensity);
    }

    @Override
    protected String getTableType() {
        return "M/Z";
    }
}
