package org.proteosuite.gui.inspect;

import com.compomics.util.gui.spectrum.ChromatogramPanel;
import com.compomics.util.gui.spectrum.SpectrumPanel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

/**
 *
 * @author SPerkins
 */
public class InspectChartPanel extends JTabbedPane {

    private static final long serialVersionUID = 1L;
    private ChromatogramPanel chromatogramPanel;
    private SpectrumPanel spectrumPanel;
    private JPanel chart2dPanel;

    public final void setChromatogramPanel(final ChromatogramPanel chromatogramPanel) {
        this.chromatogramPanel = chromatogramPanel;
        refreshTabView(0);
        setTabPlacement(JTabbedPane.BOTTOM);
    }

    public final void setSpectrum(final SpectrumPanel spectrumPanel) {
        this.spectrumPanel = spectrumPanel;
        refreshTabView(1);
    }

    public final void setChart2dPanel(final JPanel chart2dPanel) {
        this.chart2dPanel = chart2dPanel;
        refreshTabView(2);
    }

    public final void refreshTabView(final int indexOrig) {
        int index = indexOrig;
        removeAll();
        if (chromatogramPanel != null) {
            addTab("Chromatogram", chromatogramPanel);
        } else {
            index--;
        }

        if (spectrumPanel != null) {
            addTab("Spectrum", spectrumPanel);
        } else {
            index--;
        }

        if (chart2dPanel != null) {
            addTab("2D", chart2dPanel);
        }

        if (index > 0 && index < this.getTabCount()) {
            setSelectedIndex(index);
        }

        repaint();
    }
}
