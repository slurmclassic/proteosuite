package org.proteosuite.gui.renderer;

import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import org.proteosuite.model.ModificationViewModel;
import org.proteosuite.model.PeptideViewModel;

public final class PeptideCellRenderer extends DefaultTableCellRenderer {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Override
    public Component getTableCellRendererComponent(final JTable table,
            final Object value,
            final boolean isSelected, final boolean hasFocus, final int row,
            final int column) {

        JLabel tableCellRendererComponent = (JLabel) super
                .getTableCellRendererComponent(table, value, isSelected,
                        hasFocus, row, column);

        if (value instanceof PeptideViewModel) {
            return getRenderedPeptide(tableCellRendererComponent,
                    (PeptideViewModel) value);
        }

        if (value instanceof Boolean) {
            return getRenderedBoolean(tableCellRendererComponent,
                    (boolean) value);
        }

        return tableCellRendererComponent;
    }

    private Component getRenderedBoolean(
            final JLabel tableCellRendererComponent,
            final boolean value) {

        // Turn true and false to tick/cross
        // If you get a compile error here, this is a UTF8 source file!
        if (value) {
            setText("✔");
        } else {
            setText("✘");
        }

        return tableCellRendererComponent;
    }

    private JLabel getRenderedPeptide(
            final JLabel tableCellRendererComponent,
            final PeptideViewModel peptide) {

        // Firstly identify mod locations
        StringBuilder html = new StringBuilder("<html>");
        boolean[] modLocations = new boolean[peptide.getSequence().length()];

        StringBuilder modList = new StringBuilder();
        int mods = 0;
        for (ModificationViewModel m : peptide.getModifications()) {
            if (m == null) {
                continue;
            }
            int location = m.getLocation() == 0 ? 0 : m.getLocation() - 1;
            modLocations[location] = true;
            if (mods < peptide.getModifications().length) {
                modList.append(m.getName()).append(", ");
            }
            mods++;
        }

        for (int i = 0; i < peptide.getSequence().length(); i++) {
            char c = peptide.getSequence().charAt(i);
            if (modLocations[i]) {
                html.append("<span style=\"color: red;\"><b><u>").append(c).
                        append("</u></b></span>");
            } else {
                html.append(c);
            }
        }

        html.append("</html");
        setText(html.toString());

        tableCellRendererComponent.setToolTipText(modList.toString());

        return tableCellRendererComponent;

    }
}
