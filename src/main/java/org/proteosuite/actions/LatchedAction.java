/*
 * ProteoSuite is releases under the Apache 2 license.
 * This means that you are free to modify, use and distribute the software in all legislations provided you give credit to our project.
 */
package org.proteosuite.actions;

import java.util.concurrent.CountDownLatch;
import org.proteosuite.model.ProteoSuiteActionResult;
import org.proteosuite.model.ProteoSuiteActionSubject;
import org.slf4j.LoggerFactory;

/**
 *
 * @author SPerkins
 */
public class LatchedAction implements
        ProteoSuiteAction<ProteoSuiteActionResult, ProteoSuiteActionSubject> {

    private CountDownLatch latch = null;

    protected LatchedAction(final CountDownLatch latch) {
        this.latch = latch;
    }

    @Override
    public final ProteoSuiteActionResult act(
            final ProteoSuiteActionSubject argument) {
        try {
            latch.await();
            System.out.println("Latched action complete.");
        } catch (InterruptedException ex) {
            LoggerFactory.getLogger(LatchedAction.class).error("Error waiting for LatchedAction to complete.", ex);
            Thread.currentThread().interrupt();
        }

        return ProteoSuiteActionResult.emptyResult();
    }
}
