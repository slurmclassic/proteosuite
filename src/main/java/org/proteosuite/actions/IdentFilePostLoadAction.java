package org.proteosuite.actions;

import org.proteosuite.gui.analyse.AnalyseDynamicTab;
import org.proteosuite.gui.analyse.CreateOrLoadIdentificationsStep;
import org.proteosuite.gui.inspect.InspectTab;
import org.proteosuite.model.AnalyseData;
import org.proteosuite.model.IdentDataFile;
import org.proteosuite.model.ProteoSuiteActionResult;

/**
 *
 * @author SPerkins
 */
public class IdentFilePostLoadAction implements
        ProteoSuiteAction<ProteoSuiteActionResult, IdentDataFile> {

    private static final AnalyseData DATA = AnalyseData.getInstance();

    @Override
    public final ProteoSuiteActionResult act(final IdentDataFile identData) {
        AnalyseData.getInstance().getInspectModel().addIdentDataFile(identData);
        InspectTab.getInstance().refreshComboBox();

        if (identData.getParent() != null) {
            if (identData.getParent().getIdentificationDataFile() == null) {
                identData.getParent().setIdentificationDataFile(identData);
            }

            identData.getParent().setIdentStatus("Done");
            if (identData.isFromExternal()) {
                identData.computePSMStats();
            }
        } else if (DATA.doingGenomeAnnotation()) {
            for (int i = 0; i < DATA.getRawDataCount(); i++) {
                DATA.getRawDataFile(i).setIdentStatus("Done");
            }
        }

        boolean identsAllDone = AnalyseDynamicTab.getInstance().
                getAnalyseStatusPanel().checkAndUpdateIdentificationsStatus();
        if (DATA.doingIdentificationOnly() && identsAllDone) {
            AnalyseDynamicTab.getInstance().getAnalyseStatusPanel().
                    setResultsDone();
        }

        ((CreateOrLoadIdentificationsStep) (AnalyseDynamicTab.CREATE_OR_LOAD_IDENTIFICATIONS_STEP)).
                refreshFromData();

        return ProteoSuiteActionResult.emptyResult();
    }
}
