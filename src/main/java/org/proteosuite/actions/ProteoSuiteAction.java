package org.proteosuite.actions;

/**
 *
 * @author SPerkins
 */
@FunctionalInterface
public interface ProteoSuiteAction<T, U> {

    T act(U argument);
}
