package org.proteosuite.executor;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/**
 * Calls an executable file and returns the output from StdOut/StdError
 *
 * @author Andrew Collins
 *
 */
public class Executor {

    private final File executable;
    private String errorMessage;
    private List<String> outputMessage = new ArrayList<>();

    public Executor(final File exe) {
        this.executable = exe;
    }

    /**
     * Calls the executable and returns true or false depending on occurrence of
     * an error
     *
     * @param args Arguments for the command called.
     *
     * @return true if success, false if failure
     *
     * @throws java.io.IOException Thrown if issue with reading output/error
     * streams.
     */
    public final boolean callExe(final String[] args) throws IOException {
        String[] exeArgs = new String[args.length + 1];
        exeArgs[0] = executable.getCanonicalPath();
        System.arraycopy(args, 0, exeArgs, 1, args.length);

        ProcessBuilder processBuilder = new ProcessBuilder(exeArgs);
        Process process = null;

        process = processBuilder.start();
        InputStream is = process.getInputStream();
        try (BufferedReader reader = new BufferedReader(
                new InputStreamReader(is, StandardCharsets.UTF_8))) {
            String line;
            while ((line = reader.readLine()) != null) {
                outputMessage.add(line);
            }
        }

        InputStream errIs = process.getErrorStream();
        int value;

        StringBuilder error = new StringBuilder();
        while ((value = errIs.read()) != -1) {
            error.append((char) value);
        }

        errorMessage = error.toString();

        // destroy the process
        process.destroy();

        int exitValue = process.exitValue();

        return exitValue == 0;
    }

    public final boolean callExe() throws IOException {
        return callExe(new String[0]);

    }

    /**
     * Gets an error message if any occurred
     *
     * @return error Message
     */
    public final String getError() {
        return errorMessage;
    }

    /**
     * Gets any output generated
     *
     * @return error Message
     */
    public final List<String> getOutput() {
        return outputMessage;
    }
}
