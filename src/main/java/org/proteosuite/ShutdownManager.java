package org.proteosuite;

import java.util.concurrent.CountDownLatch;

/**
 *
 * @author SPerkins
 */
public final class ShutdownManager {
    private static final ShutdownManager INSTANCE = new ShutdownManager();
    private final CountDownLatch shutdownLatch = new CountDownLatch(1);

    private ShutdownManager() { }

    public static ShutdownManager getInstance() {
        return INSTANCE;
    }

    public CountDownLatch getShutdownLatch() {
        return this.shutdownLatch;
    }

    public void allowShutdown() {
        this.shutdownLatch.countDown();
    }
}
