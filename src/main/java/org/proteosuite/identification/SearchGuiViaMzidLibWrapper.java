package org.proteosuite.identification;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import org.proteosuite.ProteoSuiteException;
import org.proteosuite.actions.Actions;
import org.proteosuite.actions.ProteoSuiteAction;
import org.proteosuite.gui.analyse.AnalyseDynamicTab;
import org.proteosuite.model.AnalyseData;
import org.proteosuite.model.BackgroundTask;
import org.proteosuite.model.BackgroundTaskManager;
import org.proteosuite.model.ProteoSuiteActionSubject;
import org.proteosuite.model.TaskStreams;
import org.proteosuite.model.MzIdentMLFile;
import org.proteosuite.model.MzMLFile;
import org.proteosuite.model.ProteoSuiteActionResult;
import org.proteosuite.model.RawDataFile;
import org.proteosuite.utils.FileFormatUtils;
import org.proteosuite.utils.FileFormatUtils.SpectraConversionResult;
import org.proteosuite.utils.StringUtils;
import uk.ac.liv.mzidlib.AddRetentionTimeToMzid;

/**
 *
 * @author SPerkins
 */
public class SearchGuiViaMzidLibWrapper implements SearchEngine {

    private static final boolean DEBUGGING_AND_OUTPUT_EXISTS = false;
    private static final BackgroundTaskManager TASK_MANAGER
            = BackgroundTaskManager.getInstance();
    private static final AnalyseData DATA = AnalyseData.getInstance();
    private static final String JAR_LOCATION = getMzIdLibJar().getAbsolutePath();
    private final boolean genomeAnnotation;
    private final Set<RawDataFile> rawData;
    private String prefix = null;
    private final List<String> commandList;

    {
        commandList = new LinkedList<>();
        commandList.add("java");
        commandList.add("-Xmx10G");
        commandList.add("-Djava.util.Arrays.useLegacyMergeSort=true");
        commandList.add("-jar");
        commandList.add(JAR_LOCATION);
    }

    public SearchGuiViaMzidLibWrapper(final Set<RawDataFile> inputSpectra,
            final String databasePath,
            final Map<String, String> searchParameters,
            final String peptideLevelThresholding,
            final String proteinLevelThresholding) throws ProteoSuiteException {
        this.genomeAnnotation = false;
        this.rawData = inputSpectra;
        commandList.add("GenericSearch");
        if (databasePath != null && !databasePath.isEmpty()) {
            commandList.add("-inputFasta");
            commandList.add(databasePath);
        }

        if (!searchParameters.isEmpty()) {
            commandList.add("-searchParameters");
            File searchParameterFile = createSearchParameterFile(
                    searchParameters);
            commandList.add(searchParameterFile.getAbsolutePath());
        }

        commandList.add("-peptideThreshValue");
        commandList.add(peptideLevelThresholding);

        commandList.add("-proteinThreshValue");
        commandList.add(proteinLevelThresholding);

    }

    public SearchGuiViaMzidLibWrapper(final Set<RawDataFile> inputSpectra,
            final String[] geneModel, final Map<String, String> otherModels,
            final Map<String, String> searchParameters, final String prefix,
            final String peptideLevelThresholding,
            final String proteinLevelThresholding) throws ProteoSuiteException {
        this.genomeAnnotation = true;
        this.rawData = inputSpectra;
        this.prefix = prefix;

        commandList.add("ProteoAnnotator");
        commandList.add("-inputGFF");
        commandList.add(geneModel[0]);
        if (geneModel[1] != null && !geneModel[1].isEmpty()) {
            commandList.add("-inputFasta");
            commandList.add(geneModel[1]);
        }

        if (!otherModels.isEmpty()) {
            commandList.add("-inputPredicted");
            StringBuilder otherModelBuilder = new StringBuilder();
            //otherModelBuilder.append("\"");
            Iterator<Entry<String, String>> iterator = otherModels.entrySet().
                    iterator();
            while (iterator.hasNext()) {
                Entry<String, String> model = iterator.next();
                otherModelBuilder.append(model.getKey());
                otherModelBuilder.append(";");
                if (model.getValue() != null && !model.getValue().isEmpty()) {
                    otherModelBuilder.append(model.getValue());
                }

                if (iterator.hasNext()) {
                    otherModelBuilder.append("##");
                }
            }

            //otherModelBuilder.append("\"");
            commandList.add(otherModelBuilder.toString());
        }

        if (this.prefix != null && !this.prefix.isEmpty()) {
            commandList.add("-prefix");
            commandList.add(this.prefix);
        }

        if (!searchParameters.isEmpty()) {
            commandList.add("-searchParameters");
            File searchParameterFile = createSearchParameterFile(
                    searchParameters);
            commandList.add(searchParameterFile.getAbsolutePath());
        }

        commandList.add("-peptideThreshValue");
        commandList.add(peptideLevelThresholding);

        commandList.add("-proteinThreshValue");
        commandList.add(proteinLevelThresholding);

        commandList.add("-compress");
        commandList.add("false");
    }

    public final void compute() {
        if (this.genomeAnnotation) {
            this.computeGenomeAnnotation();
        } else {
            this.computeGenericSearch();
        }
    }

    private File createSearchParameterFile(
            final Map<String, String> searchParameters) throws
            ProteoSuiteException {
        StringBuilder parametersBuilder = new StringBuilder();
        Iterator<Entry<String, String>> iterator = searchParameters.entrySet().
                iterator();
        while (iterator.hasNext()) {
            Entry<String, String> searchEntry = iterator.next();
            if (searchEntry.getKey().toUpperCase().contains("_MODS")) {
                parametersBuilder.append("-").append(searchEntry.getKey()).
                        append(StringUtils.space()).append("\"").append(
                        searchEntry.getValue()).append("\"");
            } else {
                parametersBuilder.append("-").append(searchEntry.getKey()).
                        append(StringUtils.space()).append(searchEntry.
                        getValue());
            }

            if (iterator.hasNext()) {
                parametersBuilder.append(StringUtils.space());
            }
        }

        File temporaryFile = null;
        try {
            temporaryFile = File.createTempFile(
                    "proteosuite_searchGUI_settings_", null);
            try (BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(new FileOutputStream(temporaryFile),
                            StandardCharsets.UTF_8))) {
                writer.write(parametersBuilder.toString() + "\n");
            }
        } catch (IOException ex) {
            System.out.println(ex.getLocalizedMessage());
            temporaryFile = null;
            throw new ProteoSuiteException(
                    "Problem writing out search settings file for mzidLib.", ex, true);
        }

        return temporaryFile;
    }

    private void computeGenericSearch() {
        Iterator<RawDataFile> iterator = rawData.iterator();
        final RawDataFile primaryFile = iterator.next();
        BackgroundTask<RawDataFile> task = new BackgroundTask<>(primaryFile,
                "Create Identifications");
        task.addAsynchronousProcessingAction(
                new ProteoSuiteAction<ProteoSuiteActionResult, RawDataFile>() {
            @Override
            public final ProteoSuiteActionResult<File> act(
                    final RawDataFile argument) {
                File mgfFileOrLocation = null;
                if (rawData.size() > 1) {
                    for (RawDataFile rawDataFile : rawData) {
                        if (rawDataFile instanceof MzMLFile) {
                            File file = new File(rawDataFile.
                                    getAbsoluteFileName().replaceAll("\\.mzML$",
                                            ".mgf").replace("\\.mzml$", ".mgf"));
                            if (DEBUGGING_AND_OUTPUT_EXISTS) {
                                rawDataFile.setPeakListFile(file);
                                mgfFileOrLocation = file;
                                continue;
                            }

                            SpectraConversionResult conversionResult
                                    = FileFormatUtils.mzMLToMGF(
                                            ((MzMLFile) rawDataFile).
                                            getUnmarshaller(), file.
                                            getAbsolutePath());
                            if (conversionResult.isSuccessfulConversion()
                                    && file.exists()) {
                                rawDataFile.setPeakListFile(file);
                                mgfFileOrLocation = file;
                            } else {
                                throw new RuntimeException(
                                        "Conversion to MGF failed, or MGF file does not exist.");
                            }

                            try {
                                mgfFileOrLocation = FileFormatUtils.
                                        splitMGFsOrReturnSame(mgfFileOrLocation,
                                                (int) Math.pow(1024, 3), 25000);
                            } catch (IOException ex) {
                                throw new RuntimeException(
                                        "Splitting of MGF file failed: " + ex.
                                        getLocalizedMessage());
                            }
                        } else {
                            mgfFileOrLocation = rawDataFile.getFile();
                            rawDataFile.setPeakListFile(rawDataFile.getFile());
                        }
                    }

                    if (mgfFileOrLocation != null) {
                        commandList.add("-spectrum_files");
                        commandList.add(mgfFileOrLocation.getParent());
                    }
                } else if (primaryFile instanceof MzMLFile) {
                    mgfFileOrLocation = new File(primaryFile.
                            getAbsoluteFileName().replaceAll("\\.mzML$", ".mgf").
                            replace("\\.mzml$", ".mgf"));
                    if (!DEBUGGING_AND_OUTPUT_EXISTS) {
                        SpectraConversionResult result = FileFormatUtils.
                                mzMLToMGF(((MzMLFile) primaryFile).
                                        getUnmarshaller(), mgfFileOrLocation.
                                        getAbsolutePath());
                        if (result.isSuccessfulConversion()
                                && mgfFileOrLocation.exists()) {
                            primaryFile.setPeakListFile(mgfFileOrLocation);

                            try {
                                mgfFileOrLocation = FileFormatUtils.
                                        splitMGFsOrReturnSame(mgfFileOrLocation,
                                                (int) Math.pow(1024, 3), 25000);
                            } catch (IOException ex) {
                                throw new RuntimeException(
                                        "Splitting of MGF file failed: " + ex.
                                        getLocalizedMessage());
                            }

                            commandList.add("-spectrum_files");
                            commandList.add(mgfFileOrLocation.getAbsolutePath());
                        }
                    }
                } else {
                    commandList.add("-spectrum_files");
                    primaryFile.setPeakListFile(primaryFile.getFile());
                    commandList.add(primaryFile.getAbsoluteFileName());
                }

                commandList.add("-outputFolder");
                if (primaryFile.getFileName().toUpperCase().endsWith("MGF")) {
                    commandList.add(primaryFile.getFile().getParentFile().
                            getAbsolutePath() + File.separator + primaryFile.
                            getFileName().replaceAll("\\.[Mm][Gg][Ff]$",
                                    "_ident"));
                } else if (primaryFile.getFileName().toUpperCase().endsWith(
                        "MZML")) {
                    commandList.add(primaryFile.getFile().getParentFile().
                            getAbsolutePath() + File.separator + primaryFile.
                            getFileName().replaceAll("\\.[Mm][Zz][Mm][Ll]$",
                                    "_ident"));
                }

                String outputMzid = null;
                if (primaryFile.getFileName().toUpperCase().endsWith("MGF")) {
                    outputMzid = primaryFile.getFile().getParentFile().
                            getAbsolutePath()
                            + File.separator + primaryFile.getFileName().
                            replaceFirst("\\.[Mm][Gg][Ff]$", "_ident")
                            + File.separator
                            + "combined_fdr_peptide_threshold_proteoGrouper_fdr_Threshold.mzid";
                } else if (primaryFile.getFileName().toUpperCase().endsWith(
                        "MZML")) {
                    outputMzid = primaryFile.getFile().getParentFile().
                            getAbsolutePath()
                            + File.separator + primaryFile.getFileName().
                            replaceFirst("\\.[Mm][Zz][Mm][Ll]$", "_ident")
                            + File.separator
                            + "combined_fdr_peptide_threshold_proteoGrouper_fdr_Threshold.mzid";
                }

                if (outputMzid == null) {
                    return new ProteoSuiteActionResult(null,
                            new ProteoSuiteException(
                                    "Could not generate output mzid file (path).", true));
                }

                File mzidNonRtFile = new File(outputMzid);

                if (DEBUGGING_AND_OUTPUT_EXISTS) {
                    return new ProteoSuiteActionResult(mzidNonRtFile);
                }

                System.out.println("Execution String: " + StringUtils.join(" ",
                        commandList));

                try {
                    Process proc = getProcess();
                    TaskStreams streams = getStreams(proc);
                    task.setStreams(streams);
                    int returnVal = awaitProcessTermination(proc);
                    if (returnVal != 0) {
                        return new ProteoSuiteActionResult(
                                new ProteoSuiteException(
                                        "Generic identification search produced exceptional return value.", true));
                    }

                    // If we are in 'folder' mode, then don't add retention times - it is impossible at this stage.
                    if (rawData.size() > 1) {
                        return new ProteoSuiteActionResult(mzidNonRtFile);
                    }

                    // Need to add in retention times now.
                    if (mzidNonRtFile.exists()) {
                        File mzidRtFile = new File(mzidNonRtFile.
                                getCanonicalPath() + ".tmp");
                        AddRetentionTimeToMzid.add(mzidNonRtFile.
                                getCanonicalPath(), (String) null, mzidRtFile.
                                getCanonicalPath());

                        // If the mzid file exists with retention times, delete the original and rename it to the original name.
                        // Even if the delete fails, the file should be overwritten.
                        if (mzidRtFile.exists()) {
                            File mzidFile = new File(mzidNonRtFile.
                                    getCanonicalPath());
                            if (!mzidNonRtFile.delete()) {
                                System.out.println(
                                        "Failed to delete original file with no retention times.");
                            }

                            Files.move(mzidRtFile.toPath(), mzidFile.toPath(),
                                    StandardCopyOption.REPLACE_EXISTING);
                            if (mzidRtFile.exists() && !mzidRtFile.delete()) {
                                System.out.println(
                                        "Failed to delete temporary mzid file.");
                            }

                            return new ProteoSuiteActionResult(mzidFile);
                        }
                    }
                } catch (IOException | InterruptedException ex) {
                    Logger.getLogger(SearchGuiViaMzidLibWrapper.class.getName()).
                            log(Level.SEVERE, null, ex);
                    return new ProteoSuiteActionResult(new ProteoSuiteException(
                            "Error running mzidLib/searchGUI for identifications.",
                            ex, true));
                }

                return ProteoSuiteActionResult.emptyResult();
            }
        });

        task.addCompletionAction(new ProteoSuiteAction<ProteoSuiteActionResult, RawDataFile>() {
            @Override
            public final ProteoSuiteActionResult act(final RawDataFile argument) {
                File mzidFile = task.getResultOfClass(File.class);
                if (mzidFile != null && mzidFile.exists()) {
                    // Let's copy this file to the main directory.
                    File finalFile = new File(mzidFile.getParentFile().
                            getParentFile().getAbsolutePath() + File.separator
                            + mzidFile.getParentFile().getName().replaceFirst(
                                    "_ident", "") + ".mzid");
                    try {
                        // Copy instead of move and do not delete folder, while we need the untouched folder.
                        Files.copy(mzidFile.toPath(), finalFile.toPath(),
                                StandardCopyOption.REPLACE_EXISTING);
                        //Files.move(mzidFile.toPath(), finalFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
                        //SystemUtils.deleteRecursive(mzidFile.getParentFile());
                    } catch (IOException ex) {
                        Logger.getLogger(SearchGuiViaMzidLibWrapper.class.
                                getName()).log(Level.SEVERE, null, ex);
                        System.out.println("Problem copying mzid file to main directory. Using mzid file: " + mzidFile.getAbsolutePath());
                        finalFile = mzidFile;
                    }

                    MzIdentMLFile mzid = new MzIdentMLFile(finalFile,
                            primaryFile);
                    primaryFile.setIdentificationDataFile(mzid);

                    DATA.getInspectModel().addIdentDataFile(mzid);
                }

                return ProteoSuiteActionResult.emptyResult();
            }
        });

        task.addCompletionAction(
                new ProteoSuiteAction<ProteoSuiteActionResult, RawDataFile>() {
            @Override
            public final ProteoSuiteActionResult act(final RawDataFile argument) {
                System.out.println("Identification search done.");
                return ProteoSuiteActionResult.emptyResult();
            }
        });

        while (iterator.hasNext()) {
            RawDataFile dataFile = iterator.next();
            BackgroundTask slaveTask = new BackgroundTask(dataFile,
                    "Create Identifications");
            slaveTask.setSlaveStatus(true);
            slaveTask.addAsynchronousProcessingAction(Actions.latchedAction(
                    task.getTaskLatch()));

            TASK_MANAGER.submit(slaveTask);
        }

        TASK_MANAGER.submit(task);
    }

    private void computeGenomeAnnotation() {

        Iterator<RawDataFile> iterator = rawData.iterator();
        final RawDataFile primaryFile = iterator.next();
        BackgroundTask task = new BackgroundTask(primaryFile,
                "Run Genome Annotation");
        final CountDownLatch masterTaskLatch = new CountDownLatch(1);

        task.addAsynchronousProcessingAction(
                new ProteoSuiteAction<ProteoSuiteActionResult, ProteoSuiteActionSubject>() {
            @Override
            public final ProteoSuiteActionResult<Integer> act(
                    final ProteoSuiteActionSubject ignored) {
                if (rawData.size() > 1) {
                    File mgfFile = null;
                    for (RawDataFile rawDataFile : rawData) {
                        if (rawDataFile instanceof MzMLFile) {
                            File file = new File(rawDataFile.
                                    getAbsoluteFileName().replaceAll("\\.mzML$",
                                            ".mgf").replace("\\.mzml$", ".mgf"));
                            SpectraConversionResult conversionResult
                                    = FileFormatUtils.mzMLToMGF(
                                            ((MzMLFile) rawDataFile).
                                            getUnmarshaller(), file.
                                            getAbsolutePath());
                            if (conversionResult.isSuccessfulConversion()
                                    && file.exists()) {
                                mgfFile = file;
                            }
                        } else {
                            mgfFile = rawDataFile.getFile();
                        }
                    }

                    if (mgfFile != null) {
                        commandList.add("-spectrum_files");
                        commandList.add(mgfFile.getParent());
                    }
                } else if (primaryFile instanceof MzMLFile) {
                    File file = new File(primaryFile.getAbsoluteFileName().
                            replaceAll("\\.mzML$", ".mgf").replace("\\.mzml$",
                            ".mgf"));
                    SpectraConversionResult conversionResult = FileFormatUtils.
                            mzMLToMGF(((MzMLFile) primaryFile).getUnmarshaller(),
                                    file.getAbsolutePath());
                    if (conversionResult.isSuccessfulConversion() && file.
                            exists()) {
                        commandList.add("-spectrum_files");
                        commandList.add(file.getAbsolutePath());
                    }
                } else {
                    commandList.add("-spectrum_files");
                    commandList.add(primaryFile.getAbsoluteFileName());
                }

                commandList.add("-outputFolder");
                commandList.add(primaryFile.getFile().getParentFile().
                        getAbsolutePath() + File.separator + "annotation_output");
                System.out.println("Execution String: " + StringUtils.join(" ",
                        commandList));

                try {
                    Process proc = getProcess();
                    TaskStreams streams = getStreams(proc);
                    task.setStreams(streams);
                    return new ProteoSuiteActionResult(awaitProcessTermination(
                            proc));
                } catch (InterruptedException | IOException ex) {
                    Logger.getLogger(SearchGuiViaMzidLibWrapper.class.getName()).
                            log(Level.SEVERE, null, ex);
                    return new ProteoSuiteActionResult(new ProteoSuiteException(
                            "Error running mzidLib/searchGUI for genome annotation.",
                            ex, true));
                }
            }
        });

        task.addCompletionAction(new ProteoSuiteAction<ProteoSuiteActionResult, ProteoSuiteActionSubject>() {
            @Override
            public final ProteoSuiteActionResult act(
                    final ProteoSuiteActionSubject argument) {
                AnalyseDynamicTab.getInstance().getAnalyseStatusPanel()
                        .setIdentificationsDone();

                AnalyseDynamicTab.getInstance().getAnalyseStatusPanel()
                        .setResultsDone();

                String outputMzid = primaryFile.getFile().getParentFile().
                        getAbsolutePath()
                        + File.separator + "annotation_output" + File.separator
                        + prefix
                        + "combined_fdr_peptide_threshold_mappedGff2_proteoGrouper_fdr_Threshold.mzid";

                File mzidFile = new File(outputMzid);
                if (mzidFile.exists()) {
                    DATA.getInspectModel().addIdentDataFile(new MzIdentMLFile(
                            mzidFile, null));
                }

                masterTaskLatch.countDown();

                JOptionPane
                        .showConfirmDialog(
                                AnalyseDynamicTab.getInstance(),
                                "Your genome annotation run has finished and your result files are now available.\n"
                                + "Please check the \"annotation_output\" folder where your raw data was situated.\n"
                                + "The mzID output file from the pipeline is currently loading in the background and should be available for viewing in the Inspect tab soon.\n"
                                + "Check the output folder for various CSV files and multiple annotated GFF files.\n"
                                + "Also check the \"ProteoAnnotator.txt\" file for any error messages and a log of the run.",
                                "Genome Annotation Completed",
                                JOptionPane.PLAIN_MESSAGE,
                                JOptionPane.INFORMATION_MESSAGE);

                return ProteoSuiteActionResult.emptyResult();
            }
        });

        task.addCompletionAction(
                (ProteoSuiteAction<ProteoSuiteActionResult, ProteoSuiteActionSubject>) (
                        final ProteoSuiteActionSubject argument) -> {
                    System.out.println("Genome annotation done.");
                    return ProteoSuiteActionResult.emptyResult();
                });

        while (iterator.hasNext()) {
            RawDataFile dataFile = iterator.next();
            BackgroundTask slaveTask = new BackgroundTask(dataFile,
                    "Run Genome Annotation");
            slaveTask.setSlaveStatus(true);
            slaveTask.addAsynchronousProcessingAction(Actions.latchedAction(
                    masterTaskLatch));
            TASK_MANAGER.submit(slaveTask);
        }

        TASK_MANAGER.submit(task);
    }

    public final void printDebugInfo() {
        System.out.println("This JAR found is: \"" + getMzIdLibJar() + "\"");
    }

    private Process getProcess() throws IOException {
        ProcessBuilder builder = new ProcessBuilder(commandList);
        Process proc = builder.start();
        return proc;
    }

    private TaskStreams getStreams(final Process proc) {
        TaskStreams streams = new TaskStreams();
        streams.setErrorStream(proc.getErrorStream());
        streams.setOutputStream(proc.getInputStream());
        return streams;
    }

    private int awaitProcessTermination(final Process proc) throws IOException,
            InterruptedException {
        return proc.waitFor();
    }

    public static final File getMzIdLibJar() {
        try {
            File thisClassFile = new File(URLDecoder.decode(
                    SearchGuiViaMzidLibWrapper.class.getProtectionDomain().
                    getCodeSource().getLocation().getPath(), "UTF-8"));
            if (thisClassFile.getAbsolutePath().endsWith("classes")) {
                File target = thisClassFile.getParentFile();
                Optional<File> proteosuiteBuildFolderOptional = Arrays.stream(
                        target.listFiles()).filter(p -> p.isDirectory() && p.
                                getName().startsWith("proteosuite")).findFirst();
                if (!proteosuiteBuildFolderOptional.isPresent()) {
                    throw new RuntimeException(
                            "Proteosuite build folder is not present: have you built the project?");
                }

                Optional<File> mzidlibFolderOptional = Arrays.stream(
                        proteosuiteBuildFolderOptional.get().listFiles()).
                        filter(p -> p.isDirectory() && p.getName().startsWith(
                                "mzidlib")).findFirst();
                if (!mzidlibFolderOptional.isPresent()) {
                    throw new RuntimeException(
                            "Proteosuite build mzidlib folder is not present: have you built the project? If so check in the POM file for the correct mzidlib link.");
                }

                File mzidlibFolder = mzidlibFolderOptional.get();
                Optional<File> mzidlibJarOptional = Arrays.stream(mzidlibFolder.
                        listFiles()).filter(p -> p.isFile() && p.getName().
                                equalsIgnoreCase(mzidlibFolder.getName()
                                        + ".jar")).findFirst();
                if (!mzidlibJarOptional.isPresent()) {
                    throw new RuntimeException(
                            "Proteosuite build mzidlib JAR is not present: have you built the project? If so check in the POM file for the correct mzidlib link.");
                }

                return mzidlibJarOptional.get();
            }

            Optional<File> mzidlibFolderOptional = Arrays.stream(thisClassFile.
                    getParentFile().listFiles()).filter(p -> p.isDirectory()
                            && p.getName().startsWith("mzidlib")).findFirst();
            if (!mzidlibFolderOptional.isPresent()) {
                throw new RuntimeException(
                        "Proteosuite mzidlib folder is not present.");
            }

            File mzidlibFolder = mzidlibFolderOptional.get();
            Optional<File> mzidlibJarOptional = Arrays.stream(mzidlibFolder.
                    listFiles()).filter(p -> p.isFile() && p.getName().
                            equalsIgnoreCase(mzidlibFolder.getName() + ".jar")).
                    findFirst();
            if (!mzidlibJarOptional.isPresent()) {
                throw new RuntimeException(
                        "Proteosuite build mzidlib JAR is not present: have you built the project? If so check in the POM file for the correct mzidlib link.");
            }

            return mzidlibJarOptional.get();
        } catch (UnsupportedEncodingException ex) {
            throw new RuntimeException("UTF-8 not supported?!");
        }
    }
}
