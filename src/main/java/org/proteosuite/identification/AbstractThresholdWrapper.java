package org.proteosuite.identification;

/**
 *
 * @author SPerkins
 */
public interface AbstractThresholdWrapper {
    void doThresholding();
}
