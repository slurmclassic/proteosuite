/*
 * ProteoSuite is releases under the Apache 2 license.
 * This means that you are free to modify, use and distribute the software in all legislations provided you give credit to our project.
 */
package org.proteosuite;

/**
 *
 * @author SPerkins
 */
public class ProteoSuiteException extends Exception {

    private final String localMessage;
    private final boolean isTerminal;

    public ProteoSuiteException(final String localMessage, final Exception e, final boolean isTerminal) {
        super(e);
        this.localMessage = localMessage;
        this.isTerminal = isTerminal;
        System.out.println("ProteoSuiteException created: " + localMessage);
    }

    public ProteoSuiteException(final String localMessage, final boolean isTerminal) {
        super();
        this.localMessage = localMessage;
        this.isTerminal = isTerminal;
        System.out.println("ProteoSuiteException created: " + localMessage);
    }

    @Override
    public final String getLocalizedMessage() {
        return localMessage != null ? localMessage : "";
    }

    public final boolean isTerminal() {
        return this.isTerminal;
    }
}
