--------------------------------------------------------------------
P R O T E O S U I T E   Ver 0.3.5                                                       
--------------------------------------------------------------------
 Software for Analysis of Quantitative Proteomics Data (Ver 0.3.5)
--------------------------------------------------------------------

---------------------
1. About ProteoSuite
---------------------

ProteoSuite is an open source framework for the analysis of quantitative proteomics data. 
The aim of the software is to provide bench scientists with an application to analyse their 
data using the most common techniques with the support of HUPO-PSI standards.

More information about ProteoSuite can be found at:
http://www.proteosuite.org

---------------------
2. License
---------------------

This software is released under the Apache 2.0 license which means that it can 
be used for commercial and non-commercial purposes.
See Apache 2.0 license for further details. 
http://www.apache.org/licenses/LICENSE-2.0.html

---------------------
3. Installation
---------------------

Please read the complete installation instructions at https://bitbucket.org/slurmclassic/proteosuite/wiki/ProteosuiteUsersGuide
---------------------
4. Source code 
---------------------

This project has been developed using NetBeans 8.0.2 and Apache Maven to manage dependencies. 
Please read the programmers guide at https://bitbucket.org/slurmclassic/proteosuite/wiki/ProteosuiteProgrammersGuide

---------------------
5. SVN content
---------------------

Directories available on the SVN public repository 
(https://bitbucket.org/slurmclassic/proteosuite/src):

Folder/File							Description								Comments
-------------------------------		-------------------------------			-------------------------------
* Plugins							xTracker XSD files						Needed for running ProteoSuite/x-Tracker
* src								ProteoSuite source code					Needed for compiling ProteoSuite
* mzQuantML_1_0_0.xsd				mzQuantML schema definition				Needed for running ProteoSuite
* pom.xml							ProteoSuite maven Project Object Model	Needed for compiling ProteoSuite
* README.txt						This file								Not needed for compiling

Maven dependencies:
Please read the programmers guide at https://bitbucket.org/slurmclassic/proteosuite/wiki/ProteosuiteProgrammersGuide

---------------------
6. Technical support
----------------------

In case of technical problems please consult the documentation available at:
     http://www.proteosuite.org/
or send an e-mail to:
     Andrew.Jones@liv.ac.uk